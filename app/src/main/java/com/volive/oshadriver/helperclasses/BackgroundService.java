package com.volive.oshadriver.helperclasses;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.JsonElement;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BackgroundService extends Service {
    public static final int notify = 10000;  //interval between two services(Here Service run every 1 Minute)
    Context context;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    String language;
    GPSTracker gpsTracker;
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling

//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        this.context = this;
//        preferenceUtils = new PreferenceUtils(this);
//        gpsTracker = new GPSTracker(this);
//        strLatitude = String.valueOf(gpsTracker.getLatitude());
//        strLongitude = String.valueOf(gpsTracker.getLongitude());
//        helperClass=new HelperClass(this);
////        return super.onStartCommand(intent, flags, startId);
////        Handler handler = new Handler();
////        handler.postDelayed(new Runnable() {
////
////            @Override
////            public void run() {
////                //performe the deskred task
////                locUpdateServiceRequest();
////
////            }
////        }, 60000);
//
//        // If we get killed, after returning from here, restart
//        return START_STICKY;
//    }

    public void locUpdateServiceRequest() {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        callRetrofit = service.update_driver_location(Constant_keys.API_KEY,
                preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, ""),
                preferenceUtils.getStringFromPreference(PreferenceUtils.request_type, ""), preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""),
                preferenceUtils.getStringFromPreference(PreferenceUtils.Address, ""),
                String.valueOf(gpsTracker.getLatitude()), String.valueOf(gpsTracker.getLongitude()));
        Log.e("locUpdateServ: ", gpsTracker.getLatitude()+"------"+gpsTracker.getLongitude());

        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                System.out.println("----------------------------------------------------");
                Log.d("Call request", call.request().toString());
                Log.d("Call request header", call.request().headers().toString());
                Log.d("Response raw header", response.headers().toString());
                Log.d("Response raw", String.valueOf(response.raw().body()));
                Log.d("Response code", String.valueOf(response.code()));
                System.out.println("----------------------------------------------------");
                Log.e("onResponse: 1111", response.body().toString());
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    Log.d("data_update", "response  >>" + searchResponse);
                    try {
                        JSONObject loginObject = new JSONObject(searchResponse);
                        int status = loginObject.getInt("status");
                        //   String message = loginObject.getString("message");
                        if (status == 1) {
                            Log.e("onResponse: ", searchResponse);
//                            helperClass.displaySnackbar(spReqDetailsLayout, message);
                        } else {
//                            helperClass.displaySnackbar(SpMAinLAyout, message);
                            Log.e("onResponse: ", searchResponse);
                        }

                    } catch (JSONException e) {
                        Log.e("error", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });
    }


    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        preferenceUtils = new PreferenceUtils(this);
        gpsTracker = new GPSTracker(this);

        helperClass = new HelperClass(this);
//        if (mTimer != null) // Cancel if already existed
//            mTimer.cancel();
//        else
        mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);   //Schedule task

    }

    @Override
    public void onDestroy() {
//        super.onDestroy();
//        mTimer.cancel();    //For Cancel Timer
//        Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    // display toast
                    locUpdateServiceRequest();
                }
            });
        }
    }


}
