package com.volive.oshadriver.helperclasses;

import android.app.Application;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/reguler.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

        TypefaceUtil.overrideFont(this, "DEFAULT", "fonts/reguler.ttf");
        TypefaceUtil.overrideFont(this, "MONOSPACE", "fonts/reguler.ttf");
        TypefaceUtil.overrideFont(this, "SERIF", "fonts/reguler.ttf");
        TypefaceUtil.overrideFont(this, "SANS_SERIF", "fonts/reguler.ttf");

    }
}
