package com.volive.oshadriver.models;

public class Notification_model {


    String notification_id,notification_receiver_id,notification_sender_id,
            notification_request_id,notification_notification_title,notfication_created_at,time,description,seen_status;

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getNotification_receiver_id() {
        return notification_receiver_id;
    }

    public void setNotification_receiver_id(String notification_receiver_id) {
        this.notification_receiver_id = notification_receiver_id;
    }

    public String getNotification_sender_id() {
        return notification_sender_id;
    }

    public void setNotification_sender_id(String notification_sender_id) {
        this.notification_sender_id = notification_sender_id;
    }

    public String getNotification_request_id() {
        return notification_request_id;
    }

    public void setNotification_request_id(String notification_request_id) {
        this.notification_request_id = notification_request_id;
    }

    public String getNotification_notification_title() {
        return notification_notification_title;
    }

    public void setNotification_notification_title(String notification_notification_title) {
        this.notification_notification_title = notification_notification_title;
    }

    public String getNotfication_created_at() {
        return notfication_created_at;
    }

    public void setNotfication_created_at(String notfication_created_at) {
        this.notfication_created_at = notfication_created_at;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeen_status() {
        return seen_status;
    }

    public void setSeen_status(String seen_status) {
        this.seen_status = seen_status;
    }
}
