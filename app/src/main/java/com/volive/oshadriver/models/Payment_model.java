package com.volive.oshadriver.models;

public class Payment_model {

    String pay_request_id, pay_order_id, pay_pickup_from, pay_drop_to, pay_date, pay_profile_pic, pay_discount_amount,
    pay_payment_method, pay_payment_status, pay_provider_amount, pay_coupon_code, pay_offer_price, pay_additional_amount,
    pay_user_id, pay_company_id,pay_provider_id,pay_assign_status,pay_request_type,pay_total_amount,pay_invoice_number,pay_final_amount;


    public String getPay_request_id() {
        return pay_request_id;
    }

    public void setPay_request_id(String pay_request_id) {
        this.pay_request_id = pay_request_id;
    }

    public String getPay_user_id() {
        return pay_user_id;
    }

    public void setPay_user_id(String pay_user_id) {
        this.pay_user_id = pay_user_id;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public void setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
    }

    public String getPay_pickup_from() {
        return pay_pickup_from;
    }

    public void setPay_pickup_from(String pay_pickup_from) {
        this.pay_pickup_from = pay_pickup_from;
    }

    public String getPay_drop_to() {
        return pay_drop_to;
    }

    public void setPay_drop_to(String pay_drop_to) {
        this.pay_drop_to = pay_drop_to;
    }

    public String getPay_date() {
        return pay_date;
    }

    public void setPay_date(String pay_date) {
        this.pay_date = pay_date;
    }

    public String getPay_profile_pic() {
        return pay_profile_pic;
    }

    public void setPay_profile_pic(String pay_profile_pic) {
        this.pay_profile_pic = pay_profile_pic;
    }

    public String getPay_discount_amount() {
        return pay_discount_amount;
    }

    public void setPay_discount_amount(String pay_discount_amount) {
        this.pay_discount_amount = pay_discount_amount;
    }

    public String getPay_payment_method() {
        return pay_payment_method;
    }

    public void setPay_payment_method(String pay_payment_method) {
        this.pay_payment_method = pay_payment_method;
    }

    public String getPay_payment_status() {
        return pay_payment_status;
    }

    public void setPay_payment_status(String pay_payment_status) {
        this.pay_payment_status = pay_payment_status;
    }

    public String getPay_provider_amount() {
        return pay_provider_amount;
    }

    public void setPay_provider_amount(String pay_provider_amount) {
        this.pay_provider_amount = pay_provider_amount;
    }

    public String getPay_coupon_code() {
        return pay_coupon_code;
    }

    public void setPay_coupon_code(String pay_coupon_code) {
        this.pay_coupon_code = pay_coupon_code;
    }

    public String getPay_offer_price() {
        return pay_offer_price;
    }

    public void setPay_offer_price(String pay_offer_price) {
        this.pay_offer_price = pay_offer_price;
    }

    public String getPay_additional_amount() {
        return pay_additional_amount;
    }

    public void setPay_additional_amount(String pay_additional_amount) {
        this.pay_additional_amount = pay_additional_amount;
    }

    public String getPay_company_id() {
        return pay_company_id;
    }

    public void setPay_company_id(String pay_company_id) {
        this.pay_company_id = pay_company_id;
    }

    public String getPay_provider_id() {
        return pay_provider_id;
    }

    public void setPay_provider_id(String pay_provider_id) {
        this.pay_provider_id = pay_provider_id;
    }

    public String getPay_assign_status() {
        return pay_assign_status;
    }

    public void setPay_assign_status(String pay_assign_status) {
        this.pay_assign_status = pay_assign_status;
    }

    public String getPay_request_type() {
        return pay_request_type;
    }

    public void setPay_request_type(String pay_request_type) {
        this.pay_request_type = pay_request_type;
    }

    public String getPay_total_amount() {
        return pay_total_amount;
    }

    public void setPay_total_amount(String pay_total_amount) {
        this.pay_total_amount = pay_total_amount;
    }

    public String getPay_invoice_number() {
        return pay_invoice_number;
    }

    public void setPay_invoice_number(String pay_invoice_number) {
        this.pay_invoice_number = pay_invoice_number;
    }

    public String getPay_final_amount() {
        return pay_final_amount;
    }

    public void setPay_final_amount(String pay_final_amount) {
        this.pay_final_amount = pay_final_amount;
    }
}









           /*  "request_rating": "5",
                     "rating_comments": "",
                     "request_rating_status": "1",
                     "sp_type": "2",
                     "cancel_by": "0",
                     "profile_pic": "uploads/3bf2e00c6b56d0270d08fd660972ab4b.jpg",
                     "date": "Sep 16,2019 05:48",
                     "final_amount": "18.9"

*/