package com.volive.oshadriver.models;

public class SpOrderListModel {
    String txt_order_id;
    String txt_loaction;
    String txt_date;
    String txt_issue;
    String txt_loc_det;
   String UserID,User_photo,order_ID,Pick_loc,Drop_loc,Req_id,User_name,work_status;

    public String getUser_name() {
        return User_name;
    }

    public void setUser_name(String user_name) {
        User_name = user_name;
    }

    public String getReq_id() {
        return Req_id;
    }

    public void setReq_id(String req_id) {
        Req_id = req_id;
    }

    public String getTxt_loc_det() {
        return txt_loc_det;
    }

    public void setTxt_loc_det(String txt_loc_det) {
        this.txt_loc_det = txt_loc_det;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    int image;

    public String getTxt_order_id() {
        return txt_order_id;
    }

    public void setTxt_order_id(String txt_order_id) {
        this.txt_order_id = txt_order_id;
    }

    public String getTxt_loaction() {
        return txt_loaction;
    }

    public void setTxt_loaction(String txt_loaction) {
        this.txt_loaction = txt_loaction;
    }

    public String getTxt_date() {
        return txt_date;
    }

    public void setTxt_date(String txt_date) {
        this.txt_date = txt_date;
    }

    public String getTxt_issue() {
        return txt_issue;
    }

    public void setTxt_issue(String txt_issue) {
        this.txt_issue = txt_issue;
    }


    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUser_photo() {
        return User_photo;
    }

    public void setUser_photo(String user_photo) {
        User_photo = user_photo;
    }

    public String getOrder_ID() {
        return order_ID;
    }

    public void setOrder_ID(String order_ID) {
        this.order_ID = order_ID;
    }

    public String getPick_loc() {
        return Pick_loc;
    }

    public void setPick_loc(String pick_loc) {
        Pick_loc = pick_loc;
    }

    public String getDrop_loc() {
        return Drop_loc;
    }

    public void setDrop_loc(String drop_loc) {
        Drop_loc = drop_loc;
    }

    public String getWork_status() {
        return work_status;
    }

    public void setWork_status(String work_status) {
        this.work_status = work_status;
    }
}
