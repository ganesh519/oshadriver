package com.volive.oshadriver.models;

public class Vechicle_images_model {
    private String image_id;
    private String image_pic;
    private String pic_type;
    private boolean isVisible;

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getImage_pic() {
        return image_pic;
    }

    public void setImage_pic(String image_pic) {
        this.image_pic = image_pic;
    }

    public String getPic_type() {
        return pic_type;
    }

    public void setPic_type(String pic_type) {
        this.pic_type = pic_type;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isVisible() {
        return isVisible;
    }


}