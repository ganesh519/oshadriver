package com.volive.oshadriver.util;

import com.google.gson.JsonElement;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface API_Services {


    @FormUrlEncoded
    @POST("login")
    Call<JsonElement> LOGIN_SERVICE(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                    @Field("user_type") String user_type, @Field("email") String email,
                                    @Field("password") String pass_word, @Field("device_name") String device_type,
                                    @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST("user_registration")
    Call<JsonElement> sign_up(@Field("API-KEY") String api_key, @Field("lang") String lang,
                              @Field("name") String user_type, @Field("email") String email, @Field("phone") String phone, @Field("password") String pass_word, @Field("confirm_password") String confirm_password, @Field("agree_tc") String agree_tc, @Field("device_name") String device_type, @Field("device_token") String device_token);



    @FormUrlEncoded
    @POST("verify_otp")
    Call<JsonElement> otp_service(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                  @Field("user_id") String user_id, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("resend_otp")
    Call<JsonElement> resend_otp(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                 @Field("user_id") String user_id);

    @GET("logout?")
    Call<JsonElement> logout(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("user_id") String user_id);


    @FormUrlEncoded
    @POST("forgot_password")
    Call<JsonElement> forgot_password(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                      @Field("email") String user_id, @Field("user_type") String user_type);


    @FormUrlEncoded
    @POST("user_password_change")
    Call<JsonElement> user_password_change(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                           @Field("user_id") String user_id, @Field("password") String password, @Field("confirm_password") String confirm_password);



    @FormUrlEncoded
    @POST("send_request")
    Call<JsonElement> send_request(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                   @Field("user_id") String user_id, @Field("pickup_from") String pickup_from,
                                   @Field("drop_to") String drop_to,
                                   @Field("description") String description, @Field("delivery_items") String delivery_items,
                                   @Field("request_type") String request_type, @Field("passengers_num") String passengers_num,
                                   @Field("car_type") String car_type, @Field("latitude_pick") String latitude_pick,
                                   @Field("longitude_pick") String longitude_pick, @Field("latitude_drop") String latitude_drop,
                                   @Field("longitude_drop") String longitude_drop, @Field("comment") String comment);


    @GET("service_request_details_offer?")
    Call<JsonElement> get_req_details(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("request_id") String request_id);


    @GET("provider_service_request_details?")
    Call<JsonElement> provider_service_request_details(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("request_id") String request_id, @Query("provider_id") String provider_id);



    @FormUrlEncoded
    @POST("accept_offer")
    Call<JsonElement> accept_offer(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                   @Field("request_id") String request_id, @Field("provider_id") String provider_id, @Field("offer_price") String offer_price);





    @GET("user_requests_list?")
    Call<JsonElement> my_request_list(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("user_id") String user_id);



    @GET("user_requests_filter?")
    Call<JsonElement> user_requests_filter(@Query("API-KEY") String api_key, @Query("lang") String lang,
                                           @Query("user_id") String user_id, @Query("total_amount") String total_amount,
                                           @Query("from_date") String from_date, @Query("to_date") String to_date,
                                           @Query("request_status") String request_status);






    @GET("view_order_details?")
    Call<JsonElement> view_order_details(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("order_id") String user_id);



    @Multipart
    @POST("provider_registration?")
    Call<JsonElement> provider_registration(@Part("API-KEY") RequestBody api_key, @Part("lang") RequestBody lang, @Part("user_type") RequestBody user_type,
                                            @Part("request_type") RequestBody request_type, @Part("name") RequestBody name, @Part("email") RequestBody email,
                                            @Part("phone") RequestBody phone, @Part("password") RequestBody password, @Part("confirm_password") RequestBody confirm_password,
                                            @Part("agree_tc") RequestBody agree_tc, @Part("dob") RequestBody dob, @Part MultipartBody.Part profile_pic,
                                            @Part MultipartBody.Part id_proof, @Part MultipartBody.Part license, @Part MultipartBody.Part insurance, @Part ArrayList<MultipartBody.Part> document, @Part("latitude") RequestBody lattitude,
                                            @Part("longitude") RequestBody longitude, @Part("device_name") RequestBody device_type, @Part("device_token") RequestBody device_token);







    @GET("orders?")
    Call<JsonElement> userRequestList(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("provider_id") String provider_id,@Query("request_type") String request_type,@Query("request_status") String request_status);




    @GET("view_orders_details?")
    Call<JsonElement> userRequestListDetails(@Query("API-KEY") String api_key, @Query("lang") String lang,@Query("provider_id") String provider_id, @Query("user_id") String user_id,@Query("order_id") String order_id,@Query("request_status") String request_status);



    @GET("home_dashboard?")
    Call<JsonElement> driversRidesStatus(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("provider_id") String provider_id);





    @FormUrlEncoded
    @POST("send_offer")
    Call<JsonElement> send_offer(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                   @Field("request_id") String request_id, @Field("provider_id") String provider_id, @Field("offer_price") String offer_price);




    @GET("reject_reasons_list?")
    Call<JsonElement> reject_reasons_list(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("request_type") String request_type ,@Query("request_status") String request_status);



    @FormUrlEncoded
    @POST("reject_service_request")
    Call<JsonElement> reject_service_request(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                 @Field("request_id") String request_id, @Field("provider_id") String provider_id, @Field("reason_id") String reason_id);



    @FormUrlEncoded
    @POST("start_work")
    Call<JsonElement> start_work(@Field("API-KEY") String api_key, @Field("lang") String lang, @Field("request_id") String request_id);



    @FormUrlEncoded
    @POST("complete_service_request")
    Call<JsonElement> complete_service_request(@Field("API-KEY") String api_key, @Field("lang") String lang, @Field("request_id") String request_id);





    @GET("notifications?")
    Call<JsonElement> notifications(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("user_id") String user_id );




    @GET("unread_notifications?")
    Call<JsonElement> unread_notifications(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("user_id") String user_id );

    @GET("profile?")
    Call<JsonElement> get_profile(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("user_id") String user_id );



    @GET("payments_history?")
    Call<JsonElement> payments_history(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("user_id") String user_id ,@Query("from_date") String from_date,@Query("to_date")  String to_date,@Query("total_amount") String total_amount);


    @FormUrlEncoded
    @POST("apply_referal_code")
    Call<JsonElement> apply_referal_code(@Field("API-KEY") String api_key, @Field("lang") String lang, @Field("user_id") String user_id,@Field("referal_code_by") String referal_code_by);




    @FormUrlEncoded
    @POST("delete_images")
    Call<JsonElement> delete_img_service(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                          @Field("image_id[]") ArrayList<String> image_id);




    //  @Part MultipartBody.Part id_proof, @Part MultipartBody.Part license, @Part MultipartBody.Part insurance,
    @Multipart
    @POST("edit_profile?")
    Call<JsonElement> edit_profile(@Part("API-KEY") RequestBody api_key, @Part("lang") RequestBody lang, @Part("user_id") RequestBody user_id,
                                            @Part("username") RequestBody username, @Part("name") RequestBody name, @Part("email") RequestBody email,
                                            @Part("phone") RequestBody phone, @Part("password") RequestBody password, @Part("confirm_password") RequestBody confirm_password,
                                            @Part("dob") RequestBody dob, @Part MultipartBody.Part profile_pic,
                                            @Part("id_proof") RequestBody id_proof,@Part("insurance") RequestBody insurance,@Part("license") RequestBody license, @Part ArrayList<MultipartBody.Part> images,
                                            @Part("latitude") RequestBody lattitude,
                                            @Part("longitude") RequestBody longitude);



    @GET("terms_conditions?")
    Call<JsonElement> terms_conditions(@Query("API-KEY") String api_key, @Query("lang") String lang);


    @FormUrlEncoded
    @POST("contact_us")
    Call<JsonElement> contact_us(@Field("API-KEY") String api_key, @Field("lang") String lang, @Field("user_id")
            String user_id, @Field("name") String name,@Field("email") String email, @Field("phone_number") String phone_number,@Field("message")  String message);



    @GET("chat_history?")
    Call<JsonElement> view_chat(@Query("API-KEY") String api_key, @Query("lang") String lang,@Query("sender_id") String sender, @Query("receiver_id") String reciever,@Query("request_id") String order_id);


    @FormUrlEncoded
    @POST("service_request_chat")
    Call<JsonElement> send_messge(@Field("API-KEY") String api_key, @Field("lang") String lang,@Field("sender_id") String sender,@Field("receiver_id") String reciever,@Field("message") String messge,@Field("request_id") String order_id);



    @GET("wallet_summary?")
    Call<JsonElement> wallet_summary(@Query("API-KEY") String api_key, @Query("lang") String lang,@Query("user_id") String user_id, @Query("type") String type);


    @FormUrlEncoded
    @POST("update_driver_location")
    Call<JsonElement> update_driver_location(@Field("API-KEY") String api_key, @Field("lang") String lang,@Field("request_id") String request_id,@Field("provider_id") String provider_id,@Field("address") String address,@Field("latitude") String latitude,@Field("longitude") String longitude);



    @FormUrlEncoded
    @POST("generate_invoice")
    Call<JsonElement> generate_invoice(@Field("API-KEY") String api_key, @Field("lang") String lang,@Field("request_id") String request_id,@Field("invoice_description") String invoice_description);


   /* @FormUrlEncoded
    @POST("change_userlang")
    Call<JsonElement> change_lang(@Field("API-KEY") String api_key, @Field("lang") String lang,@Field("user_id") String user);

*/
    @GET("update_language?")
    Call<JsonElement> change_lang(@Query("API-KEY") String api_key, @Query("lang") String lang, @Query("user_id") String user_id );


    @FormUrlEncoded
    @POST("cancel_request")
    Call<JsonElement> cancel_request(@Field("API-KEY") String api_key, @Field("lang") String lang,
                                     @Field("request_id") String request_id ,@Field("user_id") String user_id,@Field("cancel_reason") String cancel_reason);








    @Multipart
    @POST("service_request_chat")
    Call<JsonElement> send_message(@Part("API-KEY") RequestBody api_key, @Part("lang") RequestBody lang,
                                                @Part("request_id") RequestBody request_id, @Part("sender_id") RequestBody sender_id,
                                                @Part("receiver_id") RequestBody receiver_id,
                                                @Part("message") RequestBody message, @Part MultipartBody.Part chat_file);




    @FormUrlEncoded
    @POST("add_money")
    Call<JsonElement> add_money(@Field("API-KEY") String api_key, @Field("lang") String lang,@Field("user_id") String user_id,@Field("amount") String amount,@Field("transaction_id") String transaction_id);




    @GET("tracking?")
    Call<JsonElement> tracking(@Query("API-KEY") String api_key, @Query("lang") String lang,@Query("request_id") String request_id);



}









