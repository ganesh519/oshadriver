package com.volive.oshadriver.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManagement
{
    private SharedPreferences pref;
        private SharedPreferences.Editor editor;
        Context _context;
        int PRIVATE_MODE = 0;
        private static final String PREF_NAME = "freelancer";

        public static final String APP_RUN_FIRST_TIME ="KEY_SET_APP_RUN_FIRST_TIME";





        public SessionManagement(Context context) {
            this._context = context;
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();
        }

        /**
         * Create login session
         */


        /**
         * Get stored session data
         */


        public void setApp_runFirst(String App_runFirst)
        {
            editor.remove(APP_RUN_FIRST_TIME);
            editor.putString(APP_RUN_FIRST_TIME, App_runFirst);
            editor.commit();
        }
        public String getApp_runFirst()
        {
            String App_runFirst= pref.getString(APP_RUN_FIRST_TIME, "first");

            return  App_runFirst;
        }
}
