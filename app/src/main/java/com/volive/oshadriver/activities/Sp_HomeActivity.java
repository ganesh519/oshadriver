package com.volive.oshadriver.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.volive.oshadriver.fragments.ContactUsFragment;
import com.volive.oshadriver.fragments.SP_Homefragment;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.BackgroundService;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.GPSTracker;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONObject;

import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sp_HomeActivity extends BaseActivity implements View.OnClickListener {
    public static FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    CircleImageView SpMenuProfileImage;

    public static String activityName = "";

   public static DrawerLayout sp_drawerLayout;
    ImageView sp_img_menu,sp_track_image,notify_img;
    RelativeLayout Sp_rlHOMEMenu,Sp_rl_lang, Sp_rlRequest, Sp_rlReject, Sp_rlPending, Sp_rlCompleted, Sp_rlwallet, Sp_rl_TermsCondition,Sp_rl_MyProfileMenu,Sp_rl_MyPayment,Sp_rl_MyNotification,Sp_rl_Contact,Sp_rl_LogoutMenu;

    TextView Sp_txt_homeMenuH,sp_txt_userName,
            Sp_txt_language,sp_txt_userMail,Sp_txt_request,Sp_txt_reject,Sp_txt_pending,Sp_txt_completed, Sp_txt_wallet, Sp_txt_MyPayment,Sp_txt_MyProfile,Sp_txt_MyNotification,Sp_count_notification,Sp_txt_Terms,Sp_txt_contact, Sp_txt_logoutt;
    Intent intent;
    public  static TextView toolbar_title,txt_not_header_count;
    public  static  RelativeLayout rel_notification;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    String language,stReqType;
    public static String strLatitude = "", strLongitude = "",stUserId = " ";
    GPSTracker gpsTracker;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp__mainactivity1);
        preferenceUtils = new PreferenceUtils(Sp_HomeActivity.this);
        helperClass = new HelperClass(Sp_HomeActivity.this);
        stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        language  = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        stReqType = preferenceUtils.getStringFromPreference(PreferenceUtils.request_type,"");


        gpsTracker = new GPSTracker(this);
        strLatitude = String.valueOf(gpsTracker.getLatitude());
        strLongitude = String.valueOf(gpsTracker.getLongitude());

        startService(new Intent(this, BackgroundService.class));

        Log.e("wsd';",stUserId);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new SP_Homefragment()).commit();

        initilizeUI();
        InitilizeViews();
        setValues();
        notification_service();

    }

    private void initilizeUI() {

        SpMenuProfileImage = findViewById(R.id.SpMenuProfileImage);
        sp_drawerLayout=findViewById(R.id.sp_drawerLayout);
        sp_img_menu=findViewById(R.id.title_bar_left_menu);

//        sp_track_image=findViewById(R.id.track_image);
        notify_img=findViewById(R.id.notify_img);

        toolbar_title =findViewById(R.id.toolbar_title);
        rel_notification = findViewById(R.id.rel_notification);
        txt_not_header_count = findViewById(R.id.txt_not_header_count);

        sp_txt_userName = findViewById(R.id.sp_txt_userName);
        Sp_txt_language = findViewById(R.id.Sp_txt_language);
        sp_txt_userMail = findViewById(R.id.sp_txt_userMail);
        //RelativeLayout id's
        Sp_rlHOMEMenu=findViewById(R.id.Sp_rl_HOMEMenu);
        Sp_rl_lang = findViewById(R.id.Sp_rl_lang);
        Sp_rlReject = findViewById(R.id.Sp_rl_reject);
        Sp_rlRequest = findViewById(R.id.Sp_rl_Request);
        Sp_rlPending = findViewById(R.id.Sp_rl_pending);
        Sp_rlCompleted = findViewById(R.id.Sp_rl_completed);
        Sp_rlwallet = findViewById(R.id.Sp_rl_wallet);
        Sp_rl_MyPayment=findViewById(R.id.Sp_rl_MyPayment);
        Sp_rl_MyProfileMenu=findViewById(R.id.Sp_rl_MyProfileMenu);
        Sp_rl_MyNotification=findViewById(R.id.Sp_rl_MyNotification);
        Sp_rl_TermsCondition=findViewById(R.id.Sp_rl_TermsCondition);
        Sp_rl_Contact = findViewById(R.id.Sp_rl_contactUS);
        Sp_rl_LogoutMenu=findViewById(R.id.Sp_rl_LogoutMenu);
        //TextViews of navigation
        Sp_txt_homeMenuH=findViewById(R.id.Sp_txt_homeMenuH);
        Sp_txt_reject=findViewById(R.id.Sp_txt_reject);
        Sp_txt_request=findViewById(R.id.Sp_txt_Request);
        Sp_txt_pending=findViewById(R.id.Sp_txt_pending);
        Sp_txt_completed=findViewById(R.id.Sp_txt_completed);
        Sp_txt_wallet=findViewById(R.id.Sp_txt_wallet);
        Sp_txt_MyPayment=findViewById(R.id.Sp_txt_MyPayment);
        Sp_txt_MyProfile=findViewById(R.id.Sp_txt_MyProfile);
        Sp_txt_MyNotification=findViewById(R.id.Sp_txt_MyNotifiction);
//        Sp_count_notification=findViewById(R.id.Sp_count_notification);
        Sp_txt_contact = findViewById(R.id.Sp_txt_contactUS);
        Sp_txt_Terms=findViewById(R.id.Sp_txt_TermCondition);
        Sp_txt_logoutt=findViewById(R.id.Sp_txt_logoutt);
    }

    private void InitilizeViews() {
        rel_notification.setOnClickListener(this);
//        sp_track_image.setVisibility(View.INVISIBLE);
        notify_img.setOnClickListener(this);
        toolbar_title.setOnClickListener(this);
        SpMenuProfileImage.setOnClickListener(this);
        sp_img_menu.setOnClickListener(this);
        txt_not_header_count.setOnClickListener(this);
        Sp_txt_language.setOnClickListener(this);
//        sp_track_image.setOnClickListener(this);

        Sp_rlHOMEMenu.setOnClickListener(this);
        Sp_rl_lang.setOnClickListener(this);

        Sp_rlReject.setOnClickListener(this);
        Sp_rlRequest.setOnClickListener(this);
        Sp_rlPending.setOnClickListener(this);
        Sp_rlCompleted.setOnClickListener(this);
        Sp_rlwallet.setOnClickListener(this);
        Sp_rl_MyPayment.setOnClickListener(this);
        Sp_rl_MyProfileMenu.setOnClickListener(this);
        Sp_rl_MyNotification.setOnClickListener(this);
        Sp_rl_TermsCondition.setOnClickListener(this);
        Sp_rl_Contact.setOnClickListener(this);
        Sp_rl_LogoutMenu.setOnClickListener(this);
        Sp_rl_lang.setOnClickListener(this);
    }


    private void setValues() {
        rel_notification.setVisibility(View.VISIBLE);
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "").isEmpty())
        {
            SpMenuProfileImage.setImageResource(R.drawable.guest_user);

        } else {
            // validate.loadImage(true, ProfileActivity.this, ProfileActivity.image_url, profile_image_select);
            Glide.with(getApplicationContext()).load(preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "")).into(SpMenuProfileImage);
        }

        sp_txt_userName.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.UserName,""));
        sp_txt_userMail.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Email,""));


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.notify_img:

                Intent i = new Intent(Sp_HomeActivity.this, NotificationsActivity.class);
                startActivity(i);

                break;

            case R.id.title_bar_left_menu:

                sp_drawerLayout.openDrawer(GravityCompat.START);

                break;

            case R.id.Sp_txt_language:
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));

                sp_drawerLayout.closeDrawers();
                changelanguageDialog();

                break;


            case R.id.Sp_rl_lang:

                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));

                sp_drawerLayout.closeDrawers();
                changelanguageDialog();

                break;

            case R.id.Sp_rl_HOMEMenu:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorPrimary));

                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));

                mFragmentManager = getSupportFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.containerView, new SP_Homefragment()).commit();
                toolbar_title.setText("Osha");

                sp_drawerLayout.closeDrawers();

                break;


            case R.id.Sp_rl_Request:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));

                intent = new Intent(Sp_HomeActivity.this, Sp_request.class);
                startActivity(intent);
                finish();

                sp_drawerLayout.closeDrawers();

                break;

            case R.id.Sp_rl_reject:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlReject.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();

                intent = new Intent(Sp_HomeActivity.this, Sp_reject.class);
                startActivity(intent);
                finish();

                break;

            case R.id.Sp_rl_completed:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();

                intent = new Intent(Sp_HomeActivity.this, SP_completed.class);
                startActivity(intent);
                finish();

                break;

            case R.id.Sp_rl_pending:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));


                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();

                intent = new Intent(Sp_HomeActivity.this, Sp_pending.class);
                startActivity(intent);
                finish();

                break;

            case R.id.Sp_rl_wallet:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                intent = new Intent(Sp_HomeActivity.this, WalletActivity.class);
                startActivity(intent);
                finish();

                sp_drawerLayout.closeDrawers();

                break;

            case R.id.Sp_rl_MyProfileMenu:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();

                intent = new Intent(Sp_HomeActivity.this, ProfileActivity.class);
                startActivity(intent);
                finish();

                break;

            case R.id.Sp_rl_MyPayment:

            Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorPrimary));
            Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
            Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

            Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
            Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
            Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


            sp_drawerLayout.closeDrawers();

            intent = new Intent(Sp_HomeActivity.this, Sp_Payment_activity.class);
            startActivity(intent);
            finish();

            break;
            case R.id.Sp_rl_MyNotification:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));


                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();

                intent = new Intent(Sp_HomeActivity.this, NotificationsActivity.class);
                startActivity(intent);
                finish();

                break;


            case R.id.Sp_rl_TermsCondition:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();

                intent = new Intent(Sp_HomeActivity.this, TermsAndConditionsActivity.class);
                startActivity(intent);
                finish();

                break;

            case R.id.Sp_rl_contactUS:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();

                mFragmentManager = getSupportFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.containerView, new ContactUsFragment()).commit();

                break;

            case R.id.Sp_rl_LogoutMenu:

                Sp_txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_reject.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_request.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_completed.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_pending.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_wallet.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyPayment.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_contact.setTextColor(getResources().getColor(R.color.colorWhite));
                Sp_txt_logoutt.setTextColor(getResources().getColor(R.color.colorPrimary));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));

                Sp_rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlCompleted.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlRequest.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlPending.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rlwallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyPayment.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                Sp_rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                Sp_rl_Contact.setBackground(getResources().getDrawable(R.drawable.button_background));


                sp_drawerLayout.closeDrawers();


                preferenceUtils.logOut();
                preferenceUtils.saveBoolean(PreferenceUtils.LOGIN_TYPE, false);
                preferenceUtils.saveString(PreferenceUtils.USER_ID, "");

                sp_drawerLayout.closeDrawers();
                logout_service();


                break;

        }
    }

    private void logout_service() {


        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_logout = null;
        call_logout = service.logout(Constant_keys.API_KEY, language, stUserId);
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(Sp_HomeActivity.this);
        progressdialog.setCancelable(false);
        progressdialog.setMessage(getString(R.string.loading));
        progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressdialog.show();
        call_logout.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressdialog.dismiss();
                Log.e("ajkldhn", response.body().toString());
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {

                            showToast(message);
                            Intent intent = new Intent(Sp_HomeActivity.this, LoginActivty.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            sp_drawerLayout.closeDrawers();
                        }
                        else {
                            showToast(message);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.e("Eror:" ,call.toString());
                Log.e("Eror:",t.toString());

            }
        });


    }


    private void notification_service()
    {

        final API_Services service  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_service;
        call_service = service.unread_notifications(Constant_keys.API_KEY,language,stUserId);
        call_service.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                if (response.isSuccessful())
                {
                    Log.e("count_not",response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");

                        String user_details = jsonObject.getString("user_details");

                        if (status == 1)
                        {
                            String count = jsonObject.getString("count");

                            int i_count = Integer.parseInt(count);

                            if (i_count > 0)
                            {
                                txt_not_header_count.setVisibility(View.VISIBLE);
                                txt_not_header_count.setText(count);
                            }
                            else {
                                txt_not_header_count.setVisibility(View.GONE);
                            }

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }

    private void changelanguageDialog() {
        final Dialog dialog = new Dialog(Sp_HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.change_langauge_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
        Button btnok = (Button) dialog.findViewById(R.id.btnapply);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.language_group);
        final RadioButton engbtn = (RadioButton) dialog.findViewById(R.id.englishButton);
        final RadioButton arabicbtn = (RadioButton) dialog.findViewById(R.id.arabicButton);
        String getLang = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        if (getLang.equalsIgnoreCase("en"))
        {
            engbtn.setChecked(true);
            arabicbtn.setChecked(false);
        }
        else {
            engbtn.setChecked(false);
            arabicbtn.setChecked(true);
        }
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) dialog.findViewById(selectedId);
                if (radioButton != null) {
                    if (engbtn.isChecked()) {
//                        Resources res = getApplicationContext().getResources();
//                        DisplayMetrics dm = res.getDisplayMetrics();
//                        android.content.res.Configuration conf = res.getConfiguration();
                       /* Locale locale = new Locale("en");
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config,
                                getApplicationContext().getResources().getDisplayMetrics());*/
                        String languageToLoad = "en"; // your language
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getBaseContext().getResources().updateConfiguration(config,
                                getBaseContext().getResources().getDisplayMetrics());
                        preferenceUtils.setLanguage("en");
                        /*preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "en");
                        String lang = "en";
                        Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                        startActivity(intent);
                        dialog.dismiss();*/
                        preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "en");
                        String lang = "en";
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "").isEmpty()) {
                            stUserId = "";
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        } else {
                            stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
                            Log.e("kfds", stUserId);
                            languge_change(lang);
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    } else if (arabicbtn.isChecked()) {
//                        Resources res = getApplicationContext().getResources();
//                        DisplayMetrics dm = res.getDisplayMetrics();
//                        android.content.res.Configuration conf = res.getConfiguration();
                        String languageToLoad = "ar"; // your language
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getBaseContext().getResources().updateConfiguration(config,
                                getBaseContext().getResources().getDisplayMetrics());
                        preferenceUtils.setLanguage("ar");
                      /*  Locale locale = new Locale("ar");
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config,
                                getApplicationContext().getResources().getDisplayMetrics());*/

                        /*preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "ar");
                        String lang = "ar";
                        Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                        startActivity(intent);
                        dialog.dismiss();*/
                        preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "ar");
                        String lang = "ar";

                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "").isEmpty()) {
                            stUserId = "";
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        } else {
                            stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
                            Log.e("kfds", stUserId);
                            languge_change(lang);
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    }
                }
                dialog.dismiss();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void languge_change(String lang) {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_logout = null;
        call_logout = service.change_lang(Constant_keys.API_KEY, lang, stUserId);
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(Sp_HomeActivity.this);
        progressdialog.setCancelable(false);
        progressdialog.setMessage(getString(R.string.loading));
        progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressdialog.show();
        call_logout.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressdialog.dismiss();

                if (response.isSuccessful()) {
                    Log.e("ajkldhn", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");
                        if (status == 1) {

                            //showToast(jsonObject.getString("message"));
                              /*  Intent intent = new Intent(context, HomeActivity.class);
                                context.startActivity(intent);
*/
                              /*  Intent intent = new Intent(HomeActivity.this, Splash_screen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                drawer.closeDrawers();*/
                        }
                        else {
                            showToast(jsonObject.getString("message"));

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });


    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finishAffinity();
       /* Intent i=new Intent(Intent.ACTION_MAIN);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();*/
    }


}
