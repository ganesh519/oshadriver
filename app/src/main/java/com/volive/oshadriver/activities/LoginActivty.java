package com.volive.oshadriver.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivty extends BaseActivity implements View.OnClickListener {
    public static String fromscreen = "";
    PreferenceUtils preferenceUtils;
    TextView txt_signup, txt_forgot;
    Button btn_signin;
    String activityName = "", strUserID, language;
    EditText email_edit_text, pswrd_edit_text;
    String st_email, st_password;
    HelperClass validation_key;
    RelativeLayout login_layout;
    Context context;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activty);

        FirebaseApp.initializeApp(this);
        preferenceUtils = new PreferenceUtils(LoginActivty.this);
        validation_key = new HelperClass(LoginActivty.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        strUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        intializeUI();
        intilaizeValues();

    }

    private void intializeUI() {
        txt_signup = findViewById(R.id.txt_signup);
        txt_forgot = findViewById(R.id.txt_forgot);
        pswrd_edit_text = findViewById(R.id.pswrd_edit_text);
        email_edit_text = findViewById(R.id.email_edit_text);
        btn_signin = findViewById(R.id.btn_signin);
        login_layout = findViewById(R.id.login_layout);
    }

    private void intilaizeValues() {
        txt_forgot.setOnClickListener(this);
        txt_signup.setOnClickListener(this);
        pswrd_edit_text.setOnClickListener(this);
        email_edit_text.setOnClickListener(this);
        btn_signin.setOnClickListener(this);
        login_layout.setOnClickListener(this);

    }

    private void login_account() {

        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);

        Call<JsonElement> call_login = null;

        call_login = services.LOGIN_SERVICE(Constant_keys.API_KEY, language, "3", st_email, st_password, Constant_keys.device_type, FirebaseInstanceId.getInstance().getToken());

       Log.e("asdlknh",FirebaseInstanceId.getInstance().getToken());
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_login.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                Log.e("login", response.body().toString());
                if (response.isSuccessful())

                {

                    try {


                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        String base_url = jsonObject.getString("base_url");

                        if (status == 1) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONObject user_json = jsonObject.getJSONObject("user_details");
                            String user_id = user_json.getString("user_id");
                            String name = user_json.getString("name");
                            String username = user_json.getString("username");
                            String email = user_json.getString("email");
                            String phone = user_json.getString("phone");
                            String dob = user_json.getString("dob");
                            String gender = user_json.getString("gender");
                            String password = user_json.getString("password");
                            String auth_level = user_json.getString("auth_level");
                            String profile_pic = base_url + user_json.getString("profile_pic");
                            String user_status = user_json.getString("user_status");
                            String otp = user_json.getString("otp");
                            String otp_status = user_json.getString("otp_status");
                            String request_type = user_json.getString("request_type");

                            preferenceUtils.saveString(PreferenceUtils.USER_ID, user_id);
                            preferenceUtils.saveString(PreferenceUtils.User, name);
                            preferenceUtils.saveString(PreferenceUtils.UserName, username);
                            preferenceUtils.saveString(PreferenceUtils.Email, email);
                            preferenceUtils.saveString(PreferenceUtils.Mobile, phone);
                            preferenceUtils.saveString(PreferenceUtils.password_new, password);
                            preferenceUtils.saveString(PreferenceUtils.authlevel, auth_level);
                            preferenceUtils.saveString(PreferenceUtils.OTP_STATUS, otp_status);
                            preferenceUtils.saveString(PreferenceUtils.IMAGE,profile_pic);
                            preferenceUtils.saveString(PreferenceUtils.DOB,dob);
                            preferenceUtils.saveString(PreferenceUtils.request_type,request_type);
                            preferenceUtils.saveString(PreferenceUtils.Address,user_json.getString("address"));

                            preferenceUtils.saveBoolean(PreferenceUtils.LOGIN_TYPE, true);

                            Intent a = new Intent(LoginActivty.this, Sp_HomeActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("Activity", "login");
                            a.putExtras(bundle);
                            //  a.putExtra("Activity","login");
                            startActivity(a);
                            //  finish();
                        }
                        else
                            {
                             Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }



            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("error", call.toString());
                Log.e("error", t.toString());
            }
        });


    }


    private void alerteDialog() {
        final Dialog dialog = new Dialog(LoginActivty.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.suceess_alert_dialog);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView text = dialog.findViewById(R.id.text);
        text.setText(getString(R.string.pwd_reset_success));
        TextView ok_txt = dialog.findViewById(R.id.ok_txt);
        ok_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.txt_forgot:
                final Dialog dialogOffer = new Dialog(LoginActivty.this, R.style.MyAlertDialogTheme);
                dialogOffer.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogOffer.setContentView(R.layout.forgot_pwd_dialog);
                dialogOffer.getWindow().setBackgroundDrawableResource(
                        R.drawable.bg_dialog);
                dialogOffer.setCanceledOnTouchOutside(true);
                dialogOffer.setCancelable(true);
                dialogOffer.show();
//                dialogOffer.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                ImageView back_img_details = dialogOffer.findViewById(R.id.back_img_details);
                final EditText email_edit_text_dialog = dialogOffer.findViewById(R.id.email_edit_text);

                Button submit_btn = dialogOffer.findViewById(R.id.submit_btn);
                back_img_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogOffer.dismiss();
                    }
                });

                submit_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String email_frg = email_edit_text_dialog.getText().toString();

                        if (email_frg.isEmpty()) {
                            email_edit_text_dialog.setError(getString(R.string.enter_email));
                        }
                        if (!HelperClass.validateEmail(email_edit_text_dialog.getText().toString().trim())) {
                            validation_key.displaySnackbar(login_layout, getString(R.string.please_enter_your_valid_email));
                        } else {
                            dialogOffer.dismiss();
                            forgot_service(email_frg);

                        }
                    }
                });

                break;
            case R.id.txt_signup:
                Intent i = new Intent(LoginActivty.this, SPSignUp.class);
                startActivity(i);

                break;
            case R.id.btn_signin:

                st_email = email_edit_text.getText().toString();
                st_password = pswrd_edit_text.getText().toString();

                if (st_email.isEmpty())
                {
                    email_edit_text.setError(getString(R.string.valid_username_email));

                } /*else if (!HelperClass.validateEmail(email_edit_text.getText().toString().trim())) {
                    validation_key.displaySnackbar(login_layout, getString(R.string.please_enter_your_valid_email));
                } */
                else if (st_password.isEmpty()) {
                    pswrd_edit_text.setError(getString(R.string.enter_password));
                } else if (!isNetworkAvailable()) {
                    showToast(getString(R.string.please_check_your_network_connection));
                    return;
                } else {

                    login_account();


                }
                break;


        }

    }

    private void forgot_service(String email_frg) {

        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);

        Call<JsonElement> call_login = null;
        call_login = services.forgot_password(Constant_keys.API_KEY, language, email_frg, "3");
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_login.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                Log.e("kjsfdn", response.body().toString());
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");

                        if (status == 1) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            alerteDialog();
                            /*    Intent a = new Intent(LoginActivty.this, UserMainActivity.class);
                                startActivity(a);
                                //  finish();*/
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                /*  */


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("error", call.toString());
                Log.e("error", t.toString());
            }
        });


    }


}