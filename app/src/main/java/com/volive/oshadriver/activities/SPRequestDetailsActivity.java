package com.volive.oshadriver.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.DataParser;
import com.volive.oshadriver.helperclasses.GPSTracker;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SPRequestDetailsActivity extends BaseActivity implements View.OnClickListener {
    public static String sender_id, reciever_id, recievr_image, request_id, order_id;
    ImageView imageView, profile_sp_img, back_img_details, img_pin_location;
    TextView toolbar_title, order_number, location_txt, txt_pick_up, txt_reject_reason, txt_user_comments,
            txt_trip_amount, txt_comp_cust_name, txt_del_items, txt_drop, txt_type_car, txt_trip_date, txt_pass_num,
            txt_date, txt_issue_desc, txt_total_price, txt_view_issues;
    LinearLayout req_ll_layout, ll_del_car, cancel_track_layout, pending_map_layout, ll_trip_amount, amount_layout,
            ll_invoice, reject_layout, completed_layout, del_items, des_items, amount_estimate_layout;
    Button btn_reject, btn_send_offer, btn_track_loc, btn_complete, btn_start_work, btn_cancel_order, btn_send_invoice;
    String Req_status = "";
    GoogleMap gMap;
    GPSTracker gpsTracker;
    FragmentManager fragmentManager;
    SupportMapFragment mapFragment;
    LocationManager mLocationManager;
    GoogleApiClient client;
    ScrollView scrollview;
    PreferenceUtils preferenceUtils;
    String language;
    HelperClass helperClass;
    String ReqUser_id, User_ID, Req_type, st_orderID, stReq_status, stReq_id, st_pick, st_drop, str_work_status = "",st_price, st_invoice_number;
    RelativeLayout rel_map;
    String latitude_pick, longitude_pick, longitude_drop, latitude_drop;
    double double_lat, double_lang;
    RatingBar user_rate_bar;
    Float rating;
    ArrayList<String> array_reasons_id = new ArrayList<>();
    ArrayList<String> array_reasons_list = new ArrayList<>();
    String st_reason_name, st_reason_id, username;
    ImageView img_reason;
    AutoCompleteTextView auto_select_reasons;
    JSONObject user_details;
    Handler handler = new Handler();
    int delay = 10000; //milliseconds

    double User_Latitude, User_Longitude, User_drop_lat, User_drop_lang;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sprequest_details);
        preferenceUtils = new PreferenceUtils(SPRequestDetailsActivity.this);
        helperClass = new HelperClass(SPRequestDetailsActivity.this);
        gpsTracker = new GPSTracker(SPRequestDetailsActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        Req_type = preferenceUtils.getStringFromPreference(PreferenceUtils.request_type, "");


        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        client = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .build();
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            Req_status = extras.getString("status_req");
            //Position = extras.getInt("Position");
            stReq_id = extras.getString("request");
            ReqUser_id = extras.getString("req_user");
            st_orderID = extras.getString("order_id");
            stReq_status = extras.getString("main_sts");
            Log.e("skl", ReqUser_id + Req_status);

        }


        //checking status value also

        if (Req_status.equals("1")) {
            User_ID = "";
        } else {
            User_ID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
            Log.e("fda", User_ID);

        }
        initializeUI();
        initializeValues();

        if (Req_type.equals("1")) {
            des_items.setVisibility(View.GONE);
            del_items.setVisibility(View.GONE);
        } else {
            del_items.setVisibility(View.VISIBLE);
            des_items.setVisibility(View.VISIBLE);
        }

        reqUserDetails_service();

        handler.postDelayed(new Runnable() {
            public void run() {
                //do something
                // get_details();
                //reqUserDetails_service_canel();
                handler.postDelayed(this, delay);
            }
        }, delay);


    }


    private void initializeUI() {
        ll_del_car = findViewById(R.id.ll_del_car);
        txt_type_car = findViewById(R.id.txt_type_car);
        txt_pass_num = findViewById(R.id.txt_pass_num);

        txt_user_comments = findViewById(R.id.txt_user_comments);
        txt_trip_date = findViewById(R.id.txt_trip_date);
        txt_trip_amount = findViewById(R.id.txt_trip_amount);
        txt_comp_cust_name = findViewById(R.id.txt_comp_cust_name);
        ll_trip_amount = findViewById(R.id.ll_trip_amount);
        txt_reject_reason = findViewById(R.id.txt_reject_reason);
        fragmentManager = getSupportFragmentManager();
        //GoogleMap
        mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
//        mapFragment.getMapAsync(SPRequestDetailsActivity.this);

        //TextView
        user_rate_bar = findViewById(R.id.user_rate_bar);

        rel_map = findViewById(R.id.rel_map);
        toolbar_title = findViewById(R.id.toolbar_title);
        order_number = findViewById(R.id.order_number);
        location_txt = findViewById(R.id.location_txt);
        txt_drop = findViewById(R.id.txt_drop);
        txt_del_items = findViewById(R.id.txt_del_items);
        //   txt_customer_name = findViewById(R.id.txt_customer_name);
        txt_pick_up = findViewById(R.id.txt_pick_up);
        //  txt_problem_type = findViewById(R.id.txt_problem_type);
        txt_issue_desc = findViewById(R.id.txt_issue_desc);
        txt_total_price = findViewById(R.id.txt_total_price);
        //ImageView
        back_img_details = findViewById(R.id.back_img_details);
        profile_sp_img = findViewById(R.id.profile_sp_img);
        img_pin_location = findViewById(R.id.img_pin_location);
        //Buttonbtn_reject
        btn_reject = findViewById(R.id.btn_reject);
        btn_send_offer = findViewById(R.id.btn_send_offer);
        btn_complete = findViewById(R.id.btn_complete);
        btn_track_loc = findViewById(R.id.btn_track_loc);
        btn_start_work = findViewById(R.id.btn_start_work);
        btn_cancel_order = findViewById(R.id.btn_cancel_order);
        //LinearLayout
        req_ll_layout = findViewById(R.id.req_ll_layout);
        cancel_track_layout = findViewById(R.id.cancel_track_layout);
        pending_map_layout = findViewById(R.id.pending_map_layout);
        amount_layout = findViewById(R.id.amount_layout);
        completed_layout = findViewById(R.id.completed_layout);
        btn_send_invoice = findViewById(R.id.btn_send_invoice);
        ll_invoice = findViewById(R.id.ll_invoice);
        del_items = findViewById(R.id.deliver_items);
        des_items = findViewById(R.id.des_items);
        reject_layout = findViewById(R.id.reject_layout);
        amount_estimate_layout = findViewById(R.id.amount_estimate);
        //ScrollView
        scrollview = findViewById(R.id.scrollview);
    }

    private void initializeValues() {
        ll_del_car.setOnClickListener(this);
        user_rate_bar.setOnClickListener(this);
        rel_map.setOnClickListener(this);
        txt_del_items.setOnClickListener(this);
        txt_issue_desc.setOnClickListener(this);
        btn_send_invoice.setOnClickListener(this);
        img_pin_location.setOnClickListener(this);

        if (Req_status.equalsIgnoreCase("2")) {
            req_ll_layout.setVisibility(View.GONE);
            amount_layout.setVisibility(View.GONE);
            pending_map_layout.setVisibility(View.VISIBLE);
            // img_pin_location.setVisibility(View.GONE);
            reject_layout.setVisibility(View.GONE);
            completed_layout.setVisibility(View.VISIBLE);
            ll_trip_amount.setVisibility(View.GONE);
            ll_invoice.setVisibility(View.GONE);
            cancel_track_layout.setVisibility(View.GONE);
            toolbar_title.setText(getResources().getString(R.string.details));

        } else if (Req_status.equalsIgnoreCase("1")) {
            req_ll_layout.setVisibility(View.VISIBLE);
            //   img_pin_location.setVisibility(View.VISIBLE);
            pending_map_layout.setVisibility(View.GONE);
            // pending_map_layout.setVisibility(View.VISIBLE);

            amount_layout.setVisibility(View.GONE);
            reject_layout.setVisibility(View.GONE);
            completed_layout.setVisibility(View.VISIBLE);
            ll_trip_amount.setVisibility(View.GONE);
            ll_invoice.setVisibility(View.GONE);

            rel_map = findViewById(R.id.rel_map);
            toolbar_title.setText(getResources().getString(R.string.request_details));
        } else if (Req_status.equalsIgnoreCase("3")) {
            req_ll_layout.setVisibility(View.GONE);
            pending_map_layout.setVisibility(View.GONE);
            //pending_map_layout.setVisibility(View.VISIBLE);
            amount_layout.setVisibility(View.GONE);
            //img_pin_location.setVisibility(View.VISIBLE);
            toolbar_title.setText(getResources().getString(R.string.details));
            completed_layout.setVisibility(View.VISIBLE);
            ll_trip_amount.setVisibility(View.VISIBLE);
            ll_invoice.setVisibility(View.VISIBLE);
            reject_layout.setVisibility(View.GONE);
            del_items.setVisibility(View.GONE);
            des_items.setVisibility(View.GONE);
            rel_map = findViewById(R.id.rel_map);
            amount_estimate_layout.setVisibility(View.GONE);

        } else if (Req_status.equalsIgnoreCase("4")) {
            req_ll_layout.setVisibility(View.GONE);
            pending_map_layout.setVisibility(View.GONE);
            //img_pin_location.setVisibility(View.VISIBLE);
            // pending_map_layout.setVisibility(View.VISIBLE);
            completed_layout.setVisibility(View.VISIBLE);
            ll_trip_amount.setVisibility(View.GONE);
            ll_invoice.setVisibility(View.GONE);
            del_items.setVisibility(View.GONE);
            des_items.setVisibility(View.GONE);
            toolbar_title.setText(getResources().getString(R.string.details));
            reject_layout.setVisibility(View.VISIBLE);
            imageView = findViewById(R.id.amount);


        }

        back_img_details.setOnClickListener(this);
        btn_reject.setOnClickListener(this);
        btn_send_offer.setOnClickListener(this);
        btn_track_loc.setOnClickListener(this);
        btn_complete.setOnClickListener(this);
        btn_start_work.setOnClickListener(this);
        btn_cancel_order.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.btn_send_invoice:
                Intent a = new Intent(SPRequestDetailsActivity.this, CreateInvoiceActivity.class);
                a.putExtra("order", st_orderID);
                a.putExtra("pick_up", st_pick);
                a.putExtra("drop", st_drop);
                a.putExtra("price", st_price);
                a.putExtra("req_id", request_id);
                a.putExtra("name_c", username);
                startActivity(a);
                // finish();
                break;
            case R.id.back_img_details:
                finish();
                break;
            case R.id.btn_reject:

                final Dialog dialog = new Dialog(SPRequestDetailsActivity.this, R.style.MyAlertDialogTheme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.reject_reason_list);
                dialog.getWindow().setBackgroundDrawableResource(
                        R.drawable.bg_dialog);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();
                auto_select_reasons = dialog.findViewById(R.id.auto_select_reasons);
                ImageView dialog_cancel_img = dialog.findViewById(R.id.cancel_image);
                Button send_btn = dialog.findViewById(R.id.send_btn);
                img_reason = dialog.findViewById(R.id.img_reasons);

                reject_reason_list();

                dialog_cancel_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                send_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        submit_reject_reason();
                        dialog.dismiss();

                    }
                });

                break;
            case R.id.btn_send_offer:

                final Dialog dialogOffer = new Dialog(SPRequestDetailsActivity.this, R.style.MyAlertDialogTheme);
                dialogOffer.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogOffer.setContentView(R.layout.send_offer_dialog);
                dialogOffer.getWindow().setBackgroundDrawableResource(
                        R.drawable.bg_dialog);
                dialogOffer.setCanceledOnTouchOutside(true);
                dialogOffer.setCancelable(true);
                dialogOffer.show();
//                dialogOffer.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                ImageView cancel_image = dialogOffer.findViewById(R.id.cancel_image);
                Button submit_btn = dialogOffer.findViewById(R.id.submit_btn);
                final EditText price_et = dialogOffer.findViewById(R.id.price_et);

                cancel_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogOffer.dismiss();
                    }
                });
                submit_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String stPrice = price_et.getText().toString();
                        if (stPrice.isEmpty()) {
                            price_et.setError(getString(R.string.enter_price));
                        } else {
                            send_offer(stPrice);

                            dialogOffer.dismiss();
                        }
                    }
                });
                break;
            case R.id.btn_track_loc:
                intent = new Intent(SPRequestDetailsActivity.this, SPTrackActivity.class);
                intent.putExtra("cust_name", username);
                intent.putExtra("order_num", order_id);
                intent.putExtra("rcv_img", recievr_image);
                intent.putExtra("req_id", request_id);
                startActivity(intent);
                break;
            case R.id.btn_complete:

                complete_work();

                break;
            case R.id.btn_start_work:

                work_start();
                break;
            case R.id.btn_cancel_order:
                alerteDialog();
                break;
            case R.id.img_pin_location:
                if (!latitude_drop.isEmpty() || !longitude_drop.isEmpty()) {
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr=" + Double.parseDouble(latitude_drop) + "," + Double.parseDouble(longitude_drop)));
                    startActivity(intent);
                }
                break;
        }
    }

    private void work_start() {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.start_work(Constant_keys.API_KEY, language, request_id);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(SPRequestDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();

                String search = response.body().toString();
                Log.e("jbsdkiof", search);
                if (response.isSuccessful()) {

                    try {
                        JSONObject req_json = new JSONObject(response.body().toString());
                        int status = req_json.getInt("status");
                        if (status == 1) {
                            showToast(req_json.getString("message"));

                            Intent intent = new Intent(SPRequestDetailsActivity.this, SPTrackActivity.class);
                            intent.putExtra("cust_name", username);
                            intent.putExtra("order_num", order_id);
                            intent.putExtra("rcv_img", recievr_image);
                            intent.putExtra("req_id", request_id);
                            startActivity(intent);

                        }
                        else
                            {
                            showToast(req_json.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });


    }


    private void complete_work() {

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;

        call_retrofit = service.complete_service_request(Constant_keys.API_KEY, language, request_id);

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(SPRequestDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();

                String search = response.body().toString();
                Log.e("jbsdkiof", search);
                if (response.isSuccessful()) {

                    try {
                        JSONObject req_json = new JSONObject(response.body().toString());
                        // JSONObject user_details = req_json.getJSONObject("details");
                        int status = req_json.getInt("status");
                        if (status == 1) {
                            showToast(req_json.getString("message"));
                            Intent intent = new Intent(SPRequestDetailsActivity.this, CreateInvoiceActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("order", st_orderID);
                            intent.putExtra("pick_up", st_pick);
                            intent.putExtra("drop", st_drop);
                            intent.putExtra("price", st_price);
                            intent.putExtra("req_id", request_id);
                            intent.putExtra("name_c", username);
                            startActivity(intent);
                            // finish();


                        } else {
                            showToast(req_json.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });


    }

    private void submit_reject_reason() {

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        callRetrofit = service.reject_service_request(Constant_keys.API_KEY, language, request_id, preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""), st_reason_id);

        final ProgressDialog progressDoalog = new ProgressDialog(getApplicationContext(), R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.dismiss();
        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                Log.e("dfh", response.body().toString());
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    try {
                        JSONObject jsonObject = new JSONObject(searchResponse);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SPRequestDetailsActivity.this, Sp_HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        progressDoalog.dismiss();
                        Log.e("error", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });
    }

    private void reject_reason_list() {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        callRetrofit = service.reject_reasons_list(Constant_keys.API_KEY, language, Req_type, stReq_status);

        final ProgressDialog progressDoalog = new ProgressDialog(getApplicationContext(), R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.dismiss();
        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                Log.e("ksdmfj;pk", response.body().toString());
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    try {
                        JSONObject jsonObject = new JSONObject(searchResponse);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray array_reasons = jsonObject.getJSONArray("reasons_list");
                            array_reasons_id = new ArrayList<>();
                            array_reasons_list = new ArrayList<>();
                            for (int i = 0; i < array_reasons.length(); i++) {
                                JSONObject jsonob = array_reasons.getJSONObject(i);
                                array_reasons_id.add(jsonob.getString("reason_id"));
                                array_reasons_list.add(jsonob.getString("reason_name"));
                                // auto_select_reasons.setText(jsonob.getString("reason_name"));
                            }

                            ArrayAdapter reason_list = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_list_item_1
                                    , array_reasons_list);
                            reason_list.setDropDownViewResource(android.R.layout.simple_list_item_1);
                            auto_select_reasons.setAdapter(reason_list);
                            auto_select_reasons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    auto_select_reasons.showDropDown();
                                    st_reason_id = array_reasons_id.get(position);
                                    Log.e("adfkhb", st_reason_id);
                                    // st_reason_id = array_reasons_id.get(position);
                                    st_reason_name = auto_select_reasons.getText().toString();
                                    Log.e("wdf;nk", st_reason_name);
                                }
                            });

                            auto_select_reasons.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View arg0) {
                                    auto_select_reasons.showDropDown();
                                }
                            });

                            img_reason.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View arg0) {
                                    auto_select_reasons.showDropDown();
                                }
                            });
                        }
                    } catch (JSONException e) {
                        progressDoalog.dismiss();
                        Log.e("error", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });
    }


    private void send_offer(String stPrice) {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.send_offer(Constant_keys.API_KEY, language, stReq_id, preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""), stPrice);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(SPRequestDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                String search = response.body().toString();
                Log.e("djb", search);
                if (response.isSuccessful()) {
                    try {
                        JSONObject req_json = new JSONObject(response.body().toString());
                        // JSONObject user_details = req_json.getJSONObject("details");
                        int status = req_json.getInt("status");
                        if (status == 1) {
                            showToast(req_json.getString("message"));
                            Intent intent = new Intent(SPRequestDetailsActivity.this, Sp_HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else if (status == 0) {
                            showToast(req_json.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }


    private void reqUserDetails_service() {
        Log.e("smd", language + "user" + User_ID + "req" + ReqUser_id + st_orderID + stReq_status);
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.userRequestListDetails(Constant_keys.API_KEY, language, User_ID, ReqUser_id, st_orderID, stReq_status);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(SPRequestDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    String search = response.body().toString();
                    Log.e("dfd", search);
                    try {
                        JSONObject req_json = new JSONObject(response.body().toString());
                        int status = req_json.getInt("status");
                        if (status == 1) {
                            rating = Float.parseFloat(req_json.getString("rating"));
                            user_rate_bar.setRating(rating);

                            if (Req_status.equals("1")) {
                                //request
                                user_details = req_json.getJSONObject("requests");
                                values_assign(Req_status);

                            } else if (Req_status.equals("2")) {
                                //pending
                                user_details = req_json.getJSONObject("pending");
                                values_assign(Req_status);
                                sender_id = user_details.getString("provider_id");
                                reciever_id = user_details.getString("user_id");
                                String profile_pic = user_details.getString("profile_pic");
                                recievr_image = user_details.getString(profile_pic);
                                request_id = user_details.getString("request_id");

                            } else if (Req_status.equals("3")) {
                                //completed
                                user_details = req_json.getJSONObject("completed");
                                txt_trip_amount.setText(user_details.getString("total_amount"));
                                txt_trip_date.setText(user_details.getString("completed_on"));
                                values_assign(Req_status);
                                String invoice_status = user_details.getString("invoice_status");
                                if (invoice_status.equals("1")) {
                                    ll_invoice.setVisibility(View.GONE);
                                } else {
                                    ll_invoice.setVisibility(View.VISIBLE);
                                }
                                sender_id = user_details.getString("provider_id");
                                reciever_id = user_details.getString("user_id");
                                String profile_pic = user_details.getString("profile_pic");
                                recievr_image = user_details.getString(profile_pic);
                                request_id = user_details.getString("request_id");
                            } else if (Req_status.equals("4")) {
                                //rejected
                                user_details = req_json.getJSONObject("rejected");
                                String reject_reason = user_details.getString("reject_reason");
                                txt_reject_reason.setText(reject_reason);
                                values_assign(Req_status);
                            }

                        } else if (status == 0) {
                            Toast.makeText(SPRequestDetailsActivity.this, req_json.getString("message"), Toast.LENGTH_SHORT);
                        } else if (status == 2) {
                            alert_cancel(req_json.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }


    private void alert_cancel(String message) {

        final Dialog dialog = new Dialog(SPRequestDetailsActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_req);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        TextView txt_cancel_order = dialog.findViewById(R.id.txt_message);
        txt_cancel_order.setText(message);

        Button btn_ok = dialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent a = new Intent(SPRequestDetailsActivity.this, Sp_HomeActivity.class);
                a.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(a);
                finish();
            }
        });

    }


    private void values_assign(final String req_status) {
        try {

            String profile_pic = user_details.getString("profile_pic");
            Glide.with(getApplicationContext()).
                    load(Constant_keys.imagebaseurl + profile_pic)
                    .error(R.drawable.guest_user)
                    .into(profile_sp_img);

            order_id = user_details.getString("order_id");
            request_id = user_details.getString("request_id");
            order_number.setText(order_id);
            txt_comp_cust_name.setText(user_details.getString("username"));
            username = user_details.getString("username");
            txt_trip_date.setText(user_details.getString("created_on"));
            st_drop = user_details.getString("drop_to");
            txt_drop.setText(st_drop);
            st_pick = user_details.getString("pickup_from");
            txt_pick_up.setText(st_pick);
            txt_user_comments.setText(user_details.getString("comment"));
            location_txt.setText(user_details.getString("address"));

            String delivery_items = user_details.getString("delivery_items");
            txt_del_items.setText(delivery_items);

            String description = user_details.getString("description");
            txt_issue_desc.setText(description);


            if (delivery_items.equals("") || delivery_items.isEmpty()) {
                des_items.setVisibility(View.GONE);
                del_items.setVisibility(View.GONE);
            } else {
                des_items.setVisibility(View.VISIBLE);
                del_items.setVisibility(View.VISIBLE);
            }


            String passengers_num = user_details.getString("passengers_num");
            String car_type = user_details.getString("car_type");

            //car type cheking
            if (passengers_num.equals("") || passengers_num.isEmpty() || passengers_num.equals("0")) {
                ll_del_car.setVisibility(View.GONE);
                ll_del_car.setVisibility(View.GONE);
            } else {
                ll_del_car.setVisibility(View.VISIBLE);
                ll_del_car.setVisibility(View.VISIBLE);
                txt_pass_num.setText(user_details.getString("passengers_num"));
                txt_type_car.setText(user_details.getString("car_type"));
            }


            latitude_pick = user_details.getString("latitude_pick");
            longitude_pick = user_details.getString("longitude_pick");

            latitude_drop = user_details.getString("latitude_drop");
            longitude_drop = user_details.getString("longitude_drop");

//            mapingpoint();


            if (user_details.has("tracking_status"))
            {
                str_work_status = user_details.getString("tracking_status");
            }


            User_Latitude = Double.parseDouble(latitude_pick);
            User_Longitude = Double.parseDouble(longitude_pick);


            User_drop_lat = Double.parseDouble(latitude_drop);
            User_drop_lang = Double.parseDouble(longitude_drop);


            st_price = user_details.getString("total_amount");
            st_invoice_number = user_details.getString("invoice_number");

            Log.e("wedgrs", st_pick + latitude_pick + longitude_pick + sender_id + reciever_id + recievr_image + request_id);

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    gMap = googleMap;
                    if (ActivityCompat.checkSelfPermission(SPRequestDetailsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SPRequestDetailsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    gMap.setMyLocationEnabled(false);
                    gMap.getUiSettings().setZoomControlsEnabled(false);
                    gMap.getUiSettings().setZoomGesturesEnabled(false);
                    gMap.getUiSettings().setCompassEnabled(false);
                    gMap.getUiSettings().setRotateGesturesEnabled(false);
                    gMap.getUiSettings().setScrollGesturesEnabled(false);

                    if (req_status.equals("2"))
                    {

                        if (str_work_status.equals("3"))
                        {
                            gMap.getUiSettings().setZoomControlsEnabled(true);
                            gMap.getUiSettings().setRotateGesturesEnabled(true);
                            gMap.getUiSettings().setScrollGesturesEnabled(true);

                            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.current_location_pic);
                          //  BitmapDescriptor icon1 = BitmapDescriptorFactory.fromResource(R.drawable.pick_up_loc_pic);
                            BitmapDescriptor icon_drop = BitmapDescriptorFactory.fromResource(R.drawable.drop_loc_pic);


                            LatLng MylocationLatlong = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                          //  LatLng Userlatlong = new LatLng(User_Latitude, User_Longitude);

                            LatLng User_drop = new LatLng(User_drop_lat, User_drop_lang);

                            Log.e("sckn", String.valueOf(User_Latitude));


                            LatLng start = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                           // LatLng middle = new LatLng(User_Latitude, User_Longitude);

                            LatLng dest = new LatLng(User_drop_lat, User_drop_lang);

                            // Getting URL to the Google Directions API
                            String url = getDirections_Url(start, dest);

                            FetchUrl downloadTask = new FetchUrl();

                            // Start downloading json data from Google Directions API
                            downloadTask.execute(url);
                            gMap.addMarker(new MarkerOptions().position(MylocationLatlong).icon(icon));
                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MylocationLatlong, 12));
//                            gMap.addMarker(new MarkerOptions().position(Userlatlong).icon(icon1));
//                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Userlatlong, 12));
                            gMap.addMarker(new MarkerOptions().position(User_drop).icon(icon_drop));
                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(User_drop, 12));
                        }

                        else {
                            gMap.getUiSettings().setZoomControlsEnabled(true);
                            gMap.getUiSettings().setRotateGesturesEnabled(true);
                            gMap.getUiSettings().setScrollGesturesEnabled(true);

                            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.current_location_pic);
                            BitmapDescriptor icon1 = BitmapDescriptorFactory.fromResource(R.drawable.pick_up_loc_pic);

                            BitmapDescriptor icon_drop = BitmapDescriptorFactory.fromResource(R.drawable.drop_loc_pic);


                            LatLng MylocationLatlong = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                            LatLng Userlatlong = new LatLng(User_Latitude, User_Longitude);

                            LatLng User_drop = new LatLng(User_drop_lat, User_drop_lang);

                            Log.e("sckn", String.valueOf(User_Latitude));


                            LatLng start = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                            LatLng middle = new LatLng(User_Latitude, User_Longitude);

                            LatLng dest = new LatLng(User_drop_lat, User_drop_lang);

                            // Getting URL to the Google Directions API
                            String url = getDirectionsUrl(start, dest, middle);

                            FetchUrl downloadTask = new FetchUrl();

                            // Start downloading json data from Google Directions API
                            downloadTask.execute(url);
                            gMap.addMarker(new MarkerOptions().position(MylocationLatlong).icon(icon));
                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MylocationLatlong, 12));
                            gMap.addMarker(new MarkerOptions().position(Userlatlong).icon(icon1));
                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Userlatlong, 12));
                            gMap.addMarker(new MarkerOptions().position(User_drop).icon(icon_drop));
                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(User_drop, 12));
                        }

                    } else if (req_status.equals("1")) {
                        gMap.getUiSettings().setZoomControlsEnabled(true);
                        gMap.getUiSettings().setRotateGesturesEnabled(true);
                        gMap.getUiSettings().setScrollGesturesEnabled(true);

                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.current_location_pic);
                        BitmapDescriptor icon1 = BitmapDescriptorFactory.fromResource(R.drawable.pick_up_loc_pic);
                        BitmapDescriptor icon_drop = BitmapDescriptorFactory.fromResource(R.drawable.drop_loc_pic);

                        LatLng MylocationLatlong = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                        LatLng Userlatlong = new LatLng(User_Latitude, User_Longitude);

                        LatLng User_drop = new LatLng(User_drop_lat, User_drop_lang);

                        Log.e("sckn", String.valueOf(User_Latitude));


                        LatLng start = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

                        LatLng middle = new LatLng(User_Latitude, User_Longitude);

                        LatLng dest = new LatLng(User_drop_lat, User_drop_lang);

                        // Getting URL to the Google Directions API
                        String url = getDirectionsUrl(start, dest, middle);

                        FetchUrl downloadTask = new FetchUrl();

                        // Start downloading json data from Google Directions API
                        downloadTask.execute(url);
                        gMap.addMarker(new MarkerOptions().position(MylocationLatlong).icon(icon));
                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MylocationLatlong, 12));
                        gMap.addMarker(new MarkerOptions().position(Userlatlong).icon(icon1));
                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Userlatlong, 12));
                        gMap.addMarker(new MarkerOptions().position(User_drop).icon(icon_drop));
                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(User_drop, 12));

                    } else {
                        double latitude = Double.parseDouble(latitude_pick);
                        double longitude = Double.parseDouble(longitude_pick);
                        LatLng position = new LatLng(latitude, longitude);
                        if (gMap != null)
                            gMap.clear();

                        gMap.addMarker(new MarkerOptions().position(position).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.pin_location)));
                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void alerteDialog() {
        final Dialog dialog = new Dialog(SPRequestDetailsActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cancel_reason_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();


        ImageView img_cancel = dialog.findViewById(R.id.img_cancel);
        final EditText edit_reason = dialog.findViewById(R.id.edit_reason);
        Button btn_submit = dialog.findViewById(R.id.btn_submit);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s_reason = edit_reason.getText().toString();
                if (s_reason.isEmpty()) {
                    Toast.makeText(SPRequestDetailsActivity.this, getString(R.string.enter_reason_cancel), Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    cancel_ride(s_reason);
                }


//                Toast.makeText(OrderDetailsActivity.this, "Your Ride has been cancelled", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(OrderDetailsActivity.this, MyRequestActivity.class));
            }
        });


    }

    private void cancel_ride(String s_reason) {

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.cancel_request(Constant_keys.API_KEY, language, request_id, preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""), s_reason);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(SPRequestDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    String serach = response.body().toString();
                    Log.e("cancel", serach);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            String message = jsonObject.getString("message");
                            showToast(message);
                            Intent intent = new Intent(SPRequestDetailsActivity.this, Sp_HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("Activity", Sp_HomeActivity.activityName);
                            startActivity(intent);
                            finish();

                        } else {
                            showToast(jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private String getDirectionsUrl(LatLng start, LatLng dest, LatLng middle) {

        // Origin of route
        String str_start = "origin=" + start.latitude + "," + start.longitude;

        //wayPoints
        String str_middle = "waypoints=" + middle.latitude + "," + middle.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
//        String str_wayPoint = "waypoints="+;


        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "travelmode=driving&alternatives=true&key=AIzaSyCWNkrF-2U0Jc48DvvtydZ5Kv-gLTDIaOk";
      //  travelmode=driving
        // Building the parameters to the web service
        String parameters = str_start + "&" + str_dest + "&" + str_middle + "&" + sensor + "&" + mode;

       // String link="https://www.google.com/maps/dir/?api=1&travelmode=driving"+srcAdd+desAdd+wayPoints;
        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }



    private String getDirections_Url(LatLng start, LatLng dest) {

        // Origin of route
        String str_start = "origin=" + start.latitude + "," + start.longitude;

        //wayPoints
      //  String str_middle = "waypoints=" + middle.latitude + "," + middle.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
//        String str_wayPoint = "waypoints="+;


        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "travelmode=driving&alternatives=true&key=AIzaSyCWNkrF-2U0Jc48DvvtydZ5Kv-gLTDIaOk";
        //  travelmode=driving
        // Building the parameters to the web service
        String parameters = str_start + "&" + str_dest + "&" + sensor + "&" + mode;

        // String link="https://www.google.com/maps/dir/?api=1&travelmode=driving"+srcAdd+desAdd+wayPoints;
        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0]);
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(getResources().getColor(R.color.colorPrimary));

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                gMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }


}















































