package com.volive.oshadriver.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.adapters.NotificationAdapter;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.models.Notification_model;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends BaseActivity implements View.OnClickListener {
    ImageView back_img_details;
    TextView toolbar_title;
    RecyclerView rec_view_notification;
    LinearLayoutManager layoutManager;
    ArrayList<Notification_model> notification_array = new ArrayList<>();
    NotificationAdapter notificationAdapter;
    ArrayList notify_list_array;
    String activityName = "",stUserID,language;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        preferenceUtils =  new PreferenceUtils(NotificationsActivity.this);
        helperClass = new HelperClass(NotificationsActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        initializeUI();
        initializeValues();
        adapter_part();
        load_notifications();


    }

    private void adapter_part() {
        layoutManager = new LinearLayoutManager(NotificationsActivity.this, LinearLayoutManager.VERTICAL, false);
        rec_view_notification.setLayoutManager(layoutManager);
        notify_list_array = new ArrayList();
        //notificationAdapter = new NotificationAdapter(this, notify_list_array);
       // rview_notification.setAdapter(notificationAdapter);

    }

    private void load_notifications() {

        final API_Services services  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification= null;
        call_notification = services.notifications(Constant_keys.API_KEY,language,stUserID);
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(NotificationsActivity.this);
        progressdialog.setCancelable(false);
        progressdialog.setMessage(getString(R.string.loading));
        progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressdialog.show();
        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
              progressdialog.dismiss();
                if (response.isSuccessful())
                {
                    Log.e("sjwlb",response.body().toString());

                    try {
                        JSONObject json_object = new JSONObject(response.body().toString());
                        int status  = json_object.getInt("status");
                        String message = json_object.getString("message");



                        if (status ==1)
                        {
                           // showToast(message);
                            String new_notifications = json_object.getString("new_notifications");
                            JSONArray notfi_array = json_object.getJSONArray("notifications");
                            notification_array = new ArrayList<>();

                            for (int i =0 ;i < notfi_array.length();i++)
                            {
                                JSONObject json_arry = notfi_array.getJSONObject(i);
                                Notification_model notification_model = new Notification_model();

                                notification_model.setNotification_id(json_arry.getString("notification_id"));
                                notification_model.setNotification_notification_title(json_arry.getString("notification_title"));
                                notification_model.setNotfication_created_at(json_arry.getString("created_at"));
                                notification_model.setTime(json_arry.getString("time"));
                                notification_array.add(notification_model);
                            }
                        }
                        else {
                            showToast(message);
                        }

                        notificationAdapter = new NotificationAdapter(getApplicationContext(),notification_array);
                        rec_view_notification.setAdapter(notificationAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }



            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });



    }

    private void initializeUI() {
        //ImageView
        back_img_details = findViewById(R.id.back_img_details);
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);

        //RecyclerView
        rec_view_notification = findViewById(R.id.rec_view_notification);


    }

    private void initializeValues()
    {

        back_img_details.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
            Intent i = new Intent(NotificationsActivity.this, Sp_HomeActivity.class);
            startActivity(i);
            finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back_img_details:
                Intent i = new Intent(NotificationsActivity.this, Sp_HomeActivity.class);
                startActivity(i);
                finish();

                break;

        }

    }
}