package com.volive.oshadriver.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.adapters.Sp_RequestAdapter;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.models.SpOrderListModel;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SP_completed extends BaseActivity implements View.OnClickListener{

    RecyclerView recyclerView;
    ImageView image_back;
    String activityName = "",stUserID,language,stReqType;
    ArrayList<SpOrderListModel> spOrderListModels = new ArrayList<>();
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp_completed);
         preferenceUtils = new PreferenceUtils(SP_completed.this);
         helperClass = new HelperClass(SP_completed.this);
         stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
         language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
         stReqType =preferenceUtils.getStringFromPreference(PreferenceUtils.request_type,"");

         intializeID();
         intializeValues();
         adapter_part();
       //  loadData();
         service_completed();


    }

    private void service_completed()
    {

            final API_Services services = Retrofit_fun.getClient().create(API_Services.class);

            Call<JsonElement> call_login = null;
            call_login = services.userRequestList(Constant_keys.API_KEY, language,stUserID, stReqType, "1");
            final ProgressDialog progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            call_login.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    progressDialog.dismiss();
                    Log.e("sidfhi", response.body().toString());
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());
                            int status = jsonObject.getInt("status");
                            spOrderListModels = new ArrayList<>();
                            if (status == 1) {
                            //    showToast(jsonObject.getString("message"));
                                // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                JSONArray user_json = jsonObject.getJSONArray("completed");

                                for (int i = 0;i<user_json.length();i++)
                                {
                                    JSONObject user_req = user_json.getJSONObject(i);
                                    SpOrderListModel spOrderListModel = new SpOrderListModel();
                                    spOrderListModel.setOrder_ID(user_req.getString("order_id"));
                                    spOrderListModel.setPick_loc(user_req.getString("pickup_from"));
                                    spOrderListModel.setDrop_loc(user_req.getString("drop_to"));
                                    spOrderListModel.setUser_photo(user_req.getString("profile_pic"));
                                    spOrderListModel.setUserID(user_req.getString("user_id"));
                                    spOrderListModel.setReq_id(user_req.getString("request_id"));
                                   // spOrderListModel.setReq_id(user_req.getString("ID"));
                                    spOrderListModels.add(spOrderListModel);
                                }


                            } else {
                                showToast(jsonObject.getString("message"));
                            }
                            Sp_RequestAdapter requestAdapter = new Sp_RequestAdapter(SP_completed.this, spOrderListModels, "3");
                            recyclerView.setAdapter(requestAdapter);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("error", call.toString());
                    Log.e("error", t.toString());
                }
            });


        }


    private void intializeID() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        image_back = (ImageView) findViewById(R.id.image_back);
    }

    private void intializeValues() {
        image_back.setOnClickListener(this);

    }
    private void adapter_part() {

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
            Intent i = new Intent(SP_completed.this, Sp_HomeActivity.class);
            startActivity(i);
            finish();

    }




    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {

            case  R.id.image_back:
                Intent i = new Intent(SP_completed.this, Sp_HomeActivity.class);
                startActivity(i);
                finish();
                break;
        }


    }
}