package com.volive.oshadriver.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.erikagtierrez.multiple_media_picker.Gallery;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.adapters.GalleryAdapter;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.GPSTracker;
import com.volive.oshadriver.helperclasses.GalleryUriToPath;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.helperclasses.WorkaroundMapFragment;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import net.alhazmy13.hijridatepicker.date.gregorian.GregorianDatePickerDialog;
import net.alhazmy13.hijridatepicker.date.hijri.HijriDatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;


public class SPSignUp extends BaseActivity implements View.OnClickListener,OnMapReadyCallback, HijriDatePickerDialog.OnDateSetListener /*GregorianDatePickerDialog.OnDateSetListener */{

   // public static boolean isSignUp = false;
    TextInputLayout   emailWrapper, input_id /*birth_input*/;
    RadioButton radio_passenger, radio_items;
    //RadioGroup radioGroup;
    RecyclerView rec_car_images;
    Button btn_signup,bt_upload_carInsurance,bt_upload_profile_pic,bt_upload_photoId,
            bt_upload_drivng_licence,bt_upload_carImages;
    String activityName = "", actionName = "";
    TextView txt_signup, txt_changename, birth_edit,txt_driving_license,txt_his_id,txt_his_car_insurance,txt_profile_photo;
    ImageView expand_map_img, current_loc;
    TextView termsandcond;
    CheckBox cb_signup,check_passenger,check_items;
    View gender_view, date_view;
    RelativeLayout birth_input;
    LinearLayout calender_layout;
    HelperClass helperClass;
    SupportMapFragment mapFragment;
    GoogleMap gMap;
    EditText ed_name,email_edit_text,ed_mobile,ed_create_pass,ed_confirm_pass;
    ScrollView scrollview;
    GPSTracker gpsTracker;
    PreferenceUtils preferenceUtils;
    ImageView pick_img_part;
    FragmentManager fragmentManager;

    String st_req_type = "",st_name,st_email,st_mobile,st_password,st_cnf_pwd,language, user_id, agree_tc = "",
            st_agreeTC,stDob,str_latitude,str_longitude,st_pick_img_type = "empty",st_user = "3",st_referelCode;

    double double_lat,double_lang;


    int CAMERA_CAPTURE = 1;
    int OPEN_MEDIA_PICKER =2;
    int PICK_IMAGE = 3 ;
    int PICK_FILE_REQUEST = 7;
    String PickedImgPath = "empty";


    String Picked_profilePath = "empty",Picked_licencePath ="empty",Picked_IDPath ="empty",Picked_insurance ="empty";


    public static List<String> selectedImgList = new ArrayList<>();
    public static List<String> selectedvideoImgList = new ArrayList<>();
    String pickedDocPath = null;

    String multi_image_path = "empty";

    ArrayList<File> document_file = new ArrayList<>();
    ArrayList<MultipartBody.Part> document_list;
    ImageView img_file_selct, img_pic_video, img_select;
    ArrayList<MultipartBody.Part> image_video_array = new ArrayList<>();
    ArrayList<MultipartBody.Part> image_array = new ArrayList<>();


    private GalleryAdapter galleryAdapter;

    LocationManager mLocationManager;
    GoogleApiClient client;

    ImageView img_driving_licence,img_driver_id,img_driver_profile,img_car_insurance;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spsign_up);
        FirebaseApp.initializeApp(this);
        preferenceUtils = new PreferenceUtils(SPSignUp.this);
        helperClass = new HelperClass(SPSignUp.this);
        gpsTracker = new GPSTracker(SPSignUp.this);
        //Geocoder geocoder = new Geocoder(DistaceCalculating.this, Locale.getDefault());
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        client = new GoogleApiClient.Builder(getApplicationContext())
//                .addApi(AppIndex.API)
                .addApi(LocationServices.API)
                .build();

        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        intializeID();
        initializeValues();

//        cb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (cb.isChecked()) {
//                    cb.setButtonDrawable(R.drawable.checked_cb);
//                } else {
//                    cb.setButtonDrawable(R.drawable.small_check);
//                }
//            }
//        });



    }

    private void intializeID()
    {

        fragmentManager = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        txt_profile_photo = findViewById(R.id.txt_profile_photo);
        txt_his_car_insurance = findViewById(R.id.txt_his_car_insurance);
        txt_his_id = findViewById(R.id.txt_his_id);
        txt_driving_license = findViewById(R.id.txt_driving_license);

        scrollview=findViewById(R.id.scrollview);
        cb_signup = findViewById(R.id.cb_signup);
        date_view = findViewById(R.id.date_view);

        img_driving_licence = findViewById(R.id.img_driving_licence);

        img_driver_id = findViewById(R.id.img_driver_id);
        img_driver_profile = findViewById(R.id.img_driver_profile);
        img_car_insurance = findViewById(R.id.img_car_insurance);






        check_passenger = findViewById(R.id.check_passenger);
        check_items = findViewById(R.id.check_items);
        termsandcond = (TextView) findViewById(R.id.termsandcond);
        emailWrapper = (TextInputLayout) findViewById(R.id.emailWrapper);
        radio_passenger = (RadioButton) findViewById(R.id.radio_passenger);
        radio_items = (RadioButton) findViewById(R.id.radio_items);
        //radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        calender_layout = findViewById(R.id.calender_layout);
        ed_name = findViewById(R.id.ed_name);
        email_edit_text = findViewById(R.id.email_edit_text);
        pick_img_part = findViewById(R.id.pick_img_part);

        btn_signup = (Button) findViewById(R.id.btn_signup);
        bt_upload_carInsurance = findViewById(R.id.bt_upload_carInsurance);

        bt_upload_profile_pic = findViewById(R.id.bt_upload_profile_pic);
        bt_upload_photoId = findViewById(R.id.bt_upload_photoId);
        bt_upload_drivng_licence = findViewById(R.id.bt_upload_drivng_licence);
        bt_upload_carImages = findViewById(R.id.bt_upload_carImages);
        ed_mobile = findViewById(R.id.ed_mobile);
        ed_create_pass = findViewById(R.id.ed_create_pass);
        ed_confirm_pass = findViewById(R.id.ed_confirm_pass);
        birth_input = findViewById(R.id.birth_input);
        birth_edit = findViewById(R.id.birth_edit);
        txt_signup = findViewById(R.id.txt_signup);
        expand_map_img = findViewById(R.id.expand_map_img);
        date_view = findViewById(R.id.date_view);
        rec_car_images = findViewById(R.id.rec_car_images);




    }

    /*private void checkbox()
    {
        if (cb_signup.isChecked()){
            st_agreeTC = "1";
        }
        else {
            st_agreeTC = "0";
        }
    }*/

    private void initializeValues()
    {
        check_passenger.setOnClickListener(this);
        check_items.setOnClickListener(this);

        bt_upload_carInsurance.setOnClickListener(this);
        bt_upload_profile_pic.setOnClickListener(this);
        bt_upload_photoId.setOnClickListener(this);
        bt_upload_drivng_licence.setOnClickListener(this);
        bt_upload_carImages.setOnClickListener(this);
        rec_car_images.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        cb_signup.setOnClickListener(this);
        img_driving_licence.setOnClickListener(this);
        img_driver_id.setOnClickListener(this);
        img_driver_profile.setOnClickListener(this);
        img_car_insurance.setOnClickListener(this);

        termsandcond.setText(Html.fromHtml(getString(R.string.terms_and_condition)));
        termsandcond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SPSignUp.this, TermsAndConditionsActivity.class);
                startActivity(i);
            }
        });


        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(SPSignUp.this, LoginActivty.class);
                startActivity(i);

            }
        });
        if (cb_signup.isChecked()) {
            agree_tc = "1";
        } else {
            agree_tc = "0";
        }

        cb_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cb_signup.isChecked()) {
                    agree_tc = "1";
                } else {
                    agree_tc = "0";
                }
            }
        });


       /* radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedRadioButtonID = radioGroup.getCheckedRadioButtonId();
                // If nothing is selected from Radio Group, then it return -1
                if (selectedRadioButtonID != -1) {
                    RadioButton selectedRadioButton = (RadioButton) findViewById(selectedRadioButtonID);
                    String selectedRadioButtonText = selectedRadioButton.getText().toString();
                    // st_req_type = selectedRadioButton.getText().toString();

                    if (selectedRadioButtonText.equals(getString(R.string.passengers_delivery)))
                    {
                        st_req_type = "1";

                    }
                    else if (selectedRadioButtonText.equals(getString(R.string.items_delivery)))
                    {
                        st_req_type = "0";
                    }
                    Log.e("sdk",st_req_type);
                }
            }
        });*/


        birth_input.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                arbic_calender();
                // TODO Auto-generated method stub
                //To show current date in the datepicker
               /* Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(SPSignUp.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        *//*      Your code   to get date and time    *//*
                        selectedmonth = selectedmonth + 1;
//                        et_date.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
                        birth_edit.setText("" + selectedday + "-" + selectedmonth + "-" + selectedyear);
                        stDob = birth_edit.getText().toString();
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle(getString(R.string.select_date));
                mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDatePicker.show();*/
            }
        });


      /*  final ArrayAdapter<String> genderArray = new ArrayAdapter<String>(SPSignUp.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.gender_array));
        genderArray.setDropDownViewResource(android.R.layout.simple_list_item_1);*/
        calender_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*UmmalquraCalendar now = new UmmalquraCalendar();
                HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                        SPSignUp.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                //Change the language to any of supported language
                // dpd.setLocale(new Locale("ar"));
                dpd.show(getFragmentManager(), "Datepickerdialog");*/

                arbic_calender();
                /*Calendar prsnt = Calendar.getInstance();
                GregorianDatePickerDialog date = GregorianDatePickerDialog.newInstance(
                        SPSignUp.this,
                        prsnt.get(Calendar.YEAR),
                        prsnt.get(Calendar.MONTH),
                        prsnt.get(Calendar.DAY_OF_MONTH));
                date.show(getFragmentManager(), "GregorianDatePickerDialog");*/

            }
        });


    }

    private void arbic_calender() {
        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);
        UmmalquraCalendar now = new UmmalquraCalendar();

        HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                SPSignUp.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );



        //dpd.setLocale(new Locale("ar"));
        //Change the language to any of supported language
        //dpd.setLocale(new Locale("ar"));
        dpd.show(getFragmentManager(), "Datepickerdialog");

       /* Calendar prsnt = Calendar.getInstance();
        GregorianDatePickerDialog date = GregorianDatePickerDialog.newInstance(
                SPSignUp.this,
                prsnt.get(Calendar.YEAR),
                prsnt.get(Calendar.MONTH),
                prsnt.get(Calendar.DAY_OF_MONTH));
        date.show(getFragmentManager(), "GregorianDatePickerDialog");*/


    }


    private void alertDialog(){
        final Dialog dialog = new Dialog(SPSignUp.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.referal_alert_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        Button send_btn = dialog.findViewById(R.id.submit_button);
        ImageView imageView = dialog.findViewById(R.id.cancel_img);
        final EditText  referral_code = dialog.findViewById(R.id.referral_code);

        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String  referral_code_st = referral_code.getText().toString();
                if (referral_code_st.isEmpty())
                   {
                    referral_code.setError(getString(R.string.referl_code));
                   }
                else {
                    send_referal(referral_code_st);
                    dialog.dismiss();
                    }






            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });
    }

    private void send_referal(String referral_code_st) {

        final API_Services service  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_service  = null;
        call_service = service.apply_referal_code(Constant_keys.API_KEY,language,user_id,referral_code_st);


        call_service.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                if (response.isSuccessful())
                {
                    Log.e("skn",response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");
                        String message  = jsonObject.getString("message");
                        if (status == 1)
                        {
                            showToast(message);
                            Intent i = new Intent(SPSignUp.this, OTPScreen.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                         startActivity(i);
                          finish();
                        }else {
                            showToast(message);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });




    }


    @Override
    public void onClick(View v) {

        switch(v.getId())
        {

            case R.id.check_passenger:
                if (check_passenger.isChecked())
                {
                    st_req_type = "1";
                }
                else {
                    st_req_type = "";
                }

                break;
            case R.id.check_items:

                if (check_items.isChecked())
                {
                    st_req_type = "2";
                }
                else {
                    st_req_type = "";
                }

                break;

            case R.id.bt_upload_profile_pic:

                st_pick_img_type = "0";
                selectImage();

                break;

            case R.id.bt_upload_photoId:
                st_pick_img_type = "1";
                selectImage();
                break;

            case R.id.bt_upload_carInsurance:
                st_pick_img_type = "2";
                selectImage();
                break;

            case  R.id.bt_upload_drivng_licence:
                st_pick_img_type = "3";
                selectImage();
                break;

            case R.id.bt_upload_carImages:
                browse();
                break;

            case  R.id.btn_signup:
                check_box_value();
                st_name = ed_name.getText().toString();
                st_email = email_edit_text.getText().toString();
                st_mobile = ed_mobile.getText().toString();
                st_password =  ed_create_pass.getText().toString();
                st_cnf_pwd = ed_confirm_pass.getText().toString();
                  stDob = birth_edit.getText().toString();

                    if (st_name.isEmpty())
                {
                    ed_name.setError(getString(R.string.enter_name));
                }
                else if (st_email.isEmpty())
                {
                    email_edit_text.setError(getString(R.string.enter_email));
                }
                else if (!HelperClass.validateEmail(email_edit_text.getText().toString().trim()))
                {
                    showToast(getString(R.string.valid_email));
                }
                else if(st_mobile.isEmpty())
                {
                    ed_mobile.setError(getString(R.string.enter_mobile));
                }
                else if (st_mobile.length() != 10)
                {
                    ed_mobile.setError(getString(R.string.valid_mobile));
                }
                else if (st_password.isEmpty())
                {
                    ed_create_pass.setError(getString(R.string.enter_pwd));
                }
                else if (st_cnf_pwd.isEmpty())
                {
                    ed_confirm_pass.setError(getString(R.string.enter_confim));
                }
                else if (!st_password.equals(st_cnf_pwd))
                {
                    Toast.makeText(getApplicationContext(),getString(R.string.pwd_match),Toast.LENGTH_SHORT).show();
                }
                 if (agree_tc.isEmpty() || agree_tc.equalsIgnoreCase("0"))
                {
                    Toast.makeText(getApplicationContext(),getString(R.string.agree_terms_cond),Toast.LENGTH_SHORT).show();
                }
                else if (str_latitude.isEmpty()  || str_longitude.isEmpty())
                {
                    Toast.makeText(getApplicationContext(),getString(R.string.check_location),Toast.LENGTH_SHORT).show();
                }
                else if (st_pick_img_type.isEmpty())
                {
                    Toast.makeText(getApplicationContext(),getString(R.string.upload_all),Toast.LENGTH_SHORT).show();
                }
                else
                 if (helperClass.checkInternetConnection(SPSignUp.this))
                 {
                    registrationService();

                 }
                else
                 {
                    Toast.makeText(getApplicationContext(),getString(R.string.check_internet),Toast.LENGTH_SHORT).show();
                 }
                break;







        }

    }

    private void check_box_value() {

        if (check_passenger.isChecked() && check_items.isChecked())
        {
            st_req_type = "3";
        }
        else if (check_passenger.isChecked())
        {
            st_req_type = "1";
        }
        else if (check_items.isChecked())
        {
            st_req_type = "2";
        }
        else if (!check_items.isChecked() && !check_passenger.isChecked())
        {
            // Toast.makeText(getApplicationContext(),"please choose at least  one type of category  ",Toast.LENGTH_SHORT).show();
            showToast(getResources().getString(R.string.choose_one));
        }
          Log.e("sdj",st_req_type);


    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        LatLng sydney;
        if (ActivityCompat.checkSelfPermission(SPSignUp.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SPSignUp.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        gMap.setMyLocationEnabled(false);
        gMap.getUiSettings().setZoomControlsEnabled(false);
        gMap.getUiSettings().setZoomGesturesEnabled(true);
        gMap.getUiSettings().setCompassEnabled(false);
        gMap.getUiSettings().setRotateGesturesEnabled(true);
        double no = 12.786;
        DecimalFormat dec = new DecimalFormat("#0.0000");
        //    System.out.println(dec.format(no));

        str_latitude = String.valueOf(gpsTracker.getLatitude());
        str_longitude = String.valueOf(gpsTracker.getLongitude());

         // Toast.makeText(getApplicationContext(),"lat,lang" + str_latitude + str_longitude,Toast.LENGTH_SHORT).show();
          Log.e("hvkjb",str_latitude + str_longitude);
        //(String.format("%.4f", s_lat));
        //str_longitude =  String.format("%.4f", s_lang);

        try {
            LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

                getCompleteAddressString(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }
        gMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.e("onCameraChange: ", "" + cameraPosition.target.latitude + " : " + cameraPosition.target.longitude);
                DecimalFormat dec = new DecimalFormat("#0.0000");
                str_latitude = String.valueOf(cameraPosition.target.latitude);
                str_longitude = String.valueOf(cameraPosition.target.longitude);

//                str_latitude = dec.format(cameraPosition.target.latitude);
//                str_longitude = dec.format(cameraPosition.target.longitude);
                double_lat = cameraPosition.target.latitude;
                double_lang = cameraPosition.target.longitude;
                getCompleteAddressString(cameraPosition.target.latitude, cameraPosition.target.longitude);
            }
        });

    }

    public String getCompleteAddressString(Double LATITUDE, Double LONGITUDE) {
        String strAdd = "";
        //txt_location.setText("");
        Geocoder geocoder = new Geocoder(SPSignUp.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
               // txt_location.setText(strAdd);
                if (strAdd.trim().isEmpty()) {
                    String address = addresses.get(0).getAddressLine(0);
                    //   Toast.makeText(getApplicationContext(), "lat gfhgfhgfh " + addresses.get(0).getLatitude() +"Lang" + addresses.get(0).getLongitude(),Toast.LENGTH_SHORT).show();
                    strAdd = address;
                  //  txt_location.setText(strAdd);
                   // strAdress = txt_location.getText().toString();
                }
                Log.d("Current location add", "" + strReturnedAddress.toString());
            } else {
                Log.d("My Current location add", "No Address returned!");
            }
        } catch (Exception e) {
            Log.e("getCompleteAddressSt: ", e.getMessage());
        }
        return strAdd;
    }



   /* @Override
    public void onDateSet(GregorianDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        birth_edit.setText(date);
        stDob = birth_edit.getText().toString();
    }*/

    @Override
    public void onDateSet(HijriDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        birth_edit.setText(date);
        stDob = birth_edit.getText().toString();
    }
    private void filechooser() {
        Intent intent;
        try {
            intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FILE_REQUEST);

//                    showFileChooser();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    private void selectImage() {

        final CharSequence[] options = {getResources().getString(R.string.takeaphoto), getResources().getString(R.string.choosefrmgallery)};

        AlertDialog.Builder builder = new AlertDialog.Builder(SPSignUp.this);
        builder.setTitle(getResources().getString(R.string.photos));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {
                if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.takeaphoto))) {
                    cameraIntent();
                } else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.choosefrmgallery))) {
                    choosefromgallery();
                }
                else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }

    private void cameraIntent()

    {
        Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE);
    }

    private void choosefromgallery(){
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
    public void browse() {
        Intent intent = new Intent(this, Gallery.class);
        // Set the title
        intent.putExtra("title", "Select media");
        // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
        intent.putExtra("mode", 2);
        //intent.putExtra("maxSelection", 3); // Optional
        startActivityForResult(intent, OPEN_MEDIA_PICKER);
    }







    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
            if (selectionResult.size() > 0) {
                MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[selectionResult.size()];
                for (int index = 0; index < selectionResult.size(); index++)
                {

                    selectedvideoImgList.add(selectionResult.get(index));

                    multi_image_path = selectionResult.get(index);
                    String PickedImgPath = selectionResult.get(index);
                    System.out.println("path" + PickedImgPath);


                    File file = new File(PickedImgPath);
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    surveyImagesParts[index] = MultipartBody.Part.createFormData("images[]", file.getName(), surveyBody);
                    image_video_array.add(MultipartBody.Part.createFormData("images[]", file.getName(), surveyBody));
                    //txt_photos.setText("sample file's are selected");
                    String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
                    //txt_photos.setText(filename);
                }

                GridLayoutManager mLayoutManager = new GridLayoutManager(SPSignUp.this, 3);
                rec_car_images.setLayoutManager(mLayoutManager);
                galleryAdapter = new GalleryAdapter(SPSignUp.this, selectedvideoImgList);
                rec_car_images.setAdapter(galleryAdapter);
            }
        } else if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }


        if (requestCode == PICK_IMAGE)
        {
            if (resultCode == RESULT_OK)
            {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

              //  PickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                if (st_pick_img_type.equals("0"))
                {
                    Picked_profilePath = GalleryUriToPath.getPath(getApplicationContext(), picUri);
                    String file_profile = Picked_profilePath.substring(Picked_profilePath.lastIndexOf("/") + 1);
                //txt_image_only.setText(filename);
               // txt_profile_photo.setText(file_profile);
                    try {
                        Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                        img_driver_profile.setImageBitmap(bm);

                    }
                    catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
                else if(st_pick_img_type.equals("1"))
                {
                    Picked_IDPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);
                String file_id =  Picked_IDPath.substring(Picked_IDPath.lastIndexOf("/") + 1);
               // txt_his_id.setText(file_id);
                    try {
                        Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                        img_driver_id.setImageBitmap(bm);

                    }
                    catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else if (st_pick_img_type.equals("2"))
                {
                    Picked_insurance = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                String file_inurance =  Picked_insurance.substring(Picked_insurance.lastIndexOf("/") + 1);
               // txt_his_car_insurance.setText(file_inurance);
                    try {
                        Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                        img_car_insurance.setImageBitmap(bm);

                    }
                    catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                else if (st_pick_img_type.equals("3"))
                {
                    Picked_licencePath = GalleryUriToPath.getPath(getApplicationContext(), picUri);
                    String file_licence =  Picked_licencePath.substring(Picked_licencePath.lastIndexOf("/") + 1);
                   // txt_driving_license.setText(file_licence);

                    try {
                        Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                        img_driving_licence.setImageBitmap(bm);

                    }
                    catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }





              //  String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);



                // txt_image_only.setText("photo Id has selected");
                c.close();
            }
        }
        else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK)
        {
            Uri filePath = data.getData();
//                pickedDocPath = String.valueOf(filePath);
            pickedDocPath = GalleryUriToPath.getPath(this, filePath);
            document_file.add(new File(pickedDocPath));
            Log.e("path ", pickedDocPath);
            //   Toast.makeText(this, "document has  selected", Toast.LENGTH_SHORT).show();
            String filename = pickedDocPath.substring(pickedDocPath.lastIndexOf("/") + 1);
          //  text_doc_name.setText(filename);
            // text_doc_name.setText("document has  selected");

        }

    }

    private void onCaptureImageResult(Intent data)
    {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
         //   PickedImgPath = destination.getAbsolutePath();

            if (st_pick_img_type.equals("0"))
            {
                Picked_profilePath = destination.getAbsolutePath();
                String file_profile = Picked_profilePath.substring(Picked_profilePath.lastIndexOf("/") + 1);
                //txt_profile_photo.setText(file_profile);
                img_driver_profile.setImageBitmap(thumbnail);
            }

            else if(st_pick_img_type.equals("1"))
            {
                Picked_IDPath = destination.getAbsolutePath();
                String file_id =  Picked_IDPath.substring(Picked_IDPath.lastIndexOf("/") + 1);
               // txt_his_id.setText(file_id);
                img_driver_id.setImageBitmap(thumbnail);


            }else if (st_pick_img_type.equals("2"))
            {
                Picked_insurance = destination.getAbsolutePath();
                String file_inurance =  Picked_insurance.substring(Picked_insurance.lastIndexOf("/") + 1);
                txt_his_car_insurance.setText(file_inurance);
                img_car_insurance.setImageBitmap(thumbnail);


            }else if (st_pick_img_type.equals("3"))
            {
                Picked_licencePath = destination.getAbsolutePath();
                String file_licence =  Picked_licencePath.substring(Picked_licencePath.lastIndexOf("/") + 1);;
              //  txt_driving_license.setText(file_licence);
                img_driving_licence.setImageBitmap(thumbnail);
            }


            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
       // txt_image_only.setText(filename);
        // txt_image_only.setText("photo Id has selected");

    }



    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        Log.d("MIME_TYPE_EXT", extension);
        if (extension != null && extension != "") {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            //  Log.d("MIME_TYPE", type);
        } else {
            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            type = fileNameMap.getContentTypeFor(url);
        }
        return type;
    }

    public void registrationService() {

        Log.e("data_lat",str_latitude + str_longitude);

        File file = null;
        document_list = new ArrayList<>();
        MultipartBody.Part image_profile = null;

            if (!Picked_profilePath.equals("empty")) {
                file = new File(Picked_profilePath);
                RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(Picked_profilePath)), file);
                image_profile = MultipartBody.Part.createFormData("profile_pic", file.getName(), requestBody);
                Log.d("Image", ">>>>>>>>>>" + image_profile);
            }



        MultipartBody.Part image_idProof = null;

            if (!Picked_IDPath.equals("empty")) {
                file = new File(Picked_IDPath);
                RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(Picked_IDPath)), file);
                image_idProof = MultipartBody.Part.createFormData("id_proof", file.getName(), requestBody);
                Log.d("Image", ">>>>>>>>>>" + image_idProof);
            }


        MultipartBody.Part image_licence = null;

            if (!Picked_licencePath.equals("empty")) {
                file = new File(Picked_licencePath);
                RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(Picked_licencePath)), file);
                image_licence = MultipartBody.Part.createFormData("license", file.getName(), requestBody);
                Log.d("Image", ">>>>>>>>>>" + image_licence);
            }


        MultipartBody.Part image_insurence = null;
            if (!Picked_insurance.equals("empty")) {
                file = new File(Picked_insurance);
                RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(Picked_insurance)), file);
                image_insurence = MultipartBody.Part.createFormData("insurance", file.getName(), requestBody);
                Log.d("Image", ">>>>>>>>>>" + image_insurence);
            }
        RequestBody r_api_key = RequestBody.create(MediaType.parse("multipart/form-data"), Constant_keys.API_KEY);
        RequestBody r_lang = RequestBody.create(MediaType.parse("multipart/form-data"), language);
        RequestBody r_user_type = RequestBody.create(MediaType.parse("multipart/form-data"), st_user);
        RequestBody r_request_type = RequestBody.create(MediaType.parse("multipart/form-data"), st_req_type);

        RequestBody r_Mail = RequestBody.create(MediaType.parse("multipart/form-data"), st_email);
        RequestBody r_phone = RequestBody.create(MediaType.parse("multipart/form-data"), st_mobile);
        RequestBody r_name = RequestBody.create(MediaType.parse("multipart/form-data"), st_name);
        RequestBody r_password = RequestBody.create(MediaType.parse("multipart/form-data"), st_password);
        RequestBody r_confirm_password = RequestBody.create(MediaType.parse("multipart/form-data"), st_cnf_pwd);
        RequestBody r_agree_tc = RequestBody.create(MediaType.parse("multipart/form-data"), agree_tc);
        RequestBody r_dob = RequestBody.create(MediaType.parse("multipart/form-data"), stDob);
        RequestBody r_Latitude = RequestBody.create(MediaType.parse("multipart/form-data"), str_latitude);
        RequestBody r_Longitude = RequestBody.create(MediaType.parse("multipart/form-data"), str_longitude);
        RequestBody r_device_name = RequestBody.create(MediaType.parse("multipart/form-data"), Constant_keys.device_type);
        RequestBody r_device_token = RequestBody.create(MediaType.parse("multipart/form-data"), FirebaseInstanceId.getInstance().getToken());
      //  RequestBody r_referalcode = RequestBody.create(MediaType.parse("multipart/form-data"), str_longitude);
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        callRetrofit = service.provider_registration(r_api_key, r_lang, r_user_type,
                r_request_type,r_name, r_Mail, r_phone, r_password, r_confirm_password, r_agree_tc,
                r_dob,image_profile,image_idProof,image_licence,image_insurence, image_video_array,
                r_Latitude, r_Longitude, r_device_name,r_device_token);


        Log.e("data", callRetrofit.toString());
        final ProgressDialog progressDoalog = new ProgressDialog(SPSignUp.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    Log.d("Registration", "response  >>" + searchResponse.toString());
                    try {
                        JSONObject regObject = new JSONObject(searchResponse);
                        int status = regObject.getInt("status");
                        String message = regObject.getString("message");
                        String base_url = regObject.getString("base_url");

                        if (status ==1 ) {

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                            JSONObject user_json = regObject.getJSONObject("provider_details");

                            user_id = user_json.getString("user_id");
                            String userID = user_json.getString("user_id");
                            String name = user_json.getString("name");
                            String username = user_json.getString("username");
                            String email = user_json.getString("email");
                            String phone = user_json.getString("phone");
                            String dob = user_json.getString("dob");
                            String gender = user_json.getString("gender");
                            String password = user_json.getString("password");
                            String auth_level = user_json.getString("auth_level");
                            String profile_pic = base_url+user_json.getString("profile_pic");
                            String user_status = user_json.getString("user_status");
                            String otp = user_json.getString("otp");
                            String otp_status = user_json.getString("otp_status");
                            String email_status = user_json.getString("email_status");
                            String email_token = user_json.getString("email_token");
                            String request_type = user_json.getString("request_type");
                         //   String reeferal_code = user_json.getString("")
                            preferenceUtils.saveString(PreferenceUtils.USER_ID, userID);
                            preferenceUtils.saveString(PreferenceUtils.User, name);
                            preferenceUtils.saveString(PreferenceUtils.UserName, username);
                            preferenceUtils.saveString(PreferenceUtils.Email, email);
                            preferenceUtils.saveString(PreferenceUtils.Mobile, phone);
                            preferenceUtils.saveString(PreferenceUtils.password_new, password);
                            preferenceUtils.saveString(PreferenceUtils.authlevel, auth_level);
                            preferenceUtils.saveString(PreferenceUtils.OTP_STATUS, otp_status);
                            preferenceUtils.saveString(PreferenceUtils.IMAGE, profile_pic);
                            preferenceUtils.saveString(PreferenceUtils.request_type,request_type);

                            preferenceUtils.saveString(PreferenceUtils.DOB,dob);
                            preferenceUtils.saveBoolean(PreferenceUtils.LOGIN_TYPE, true);

                            alertDialog();

                        }
                        else {
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        Log.e("error", e.getMessage());


                    }
                }
            }


            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });
    }



}