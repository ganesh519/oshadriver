package com.volive.oshadriver.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.adapters.Sp_RequestAdapter;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.models.SpOrderListModel;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sp_reject extends BaseActivity implements View.OnClickListener {
    RecyclerView recyclerView;
    ImageView image_back;
    ArrayList<SpOrderListModel> spOrderListModels = new ArrayList<>();
    Sp_RequestAdapter sp_requestAdapter;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    String language;
    String reqType,stUserId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp_reject);
        preferenceUtils = new PreferenceUtils(Sp_reject.this);
        helperClass = new HelperClass(Sp_reject.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        reqType = preferenceUtils.getStringFromPreference(PreferenceUtils.request_type,"");
        stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        intializeUI();
        intializeValues();
        adapter_part();
        load_reject_list();
        //loadData();

       /* Sp_RequestAdapter requestAdapter = new Sp_RequestAdapter(Sp_reject.this, arrayList, "4");
        recyclerView.setAdapter(requestAdapter);*/
    }

    private void load_reject_list()
    {

        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);

        Call<JsonElement> call_login = null;
        call_login = services.userRequestList(Constant_keys.API_KEY, language,stUserId, reqType,"");
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_login.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();

                if (response.isSuccessful())
                {

                    try {
                        Log.e("sidfhi", response.body().toString());
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");
                        // String base_url = jsonObject.getString("base_url");

                        spOrderListModels = new ArrayList<>();
                        if (status == 1) {
                          //  showToast(jsonObject.getString("message"));
                            // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONArray user_json = jsonObject.getJSONArray("rejected");
                            for (int i = 0;i<user_json.length();i++)
                            {
                                JSONObject user_req = user_json.getJSONObject(i);
                                SpOrderListModel spOrderListModel = new SpOrderListModel();
                                spOrderListModel.setOrder_ID(user_req.getString("order_id"));
                                spOrderListModel.setPick_loc(user_req.getString("pickup_from"));
                                spOrderListModel.setDrop_loc(user_req.getString("drop_to"));
                                spOrderListModel.setUser_photo(user_req.getString("profile_pic"));
                                spOrderListModel.setUserID(user_req.getString("user_id"));
                                spOrderListModel.setUser_name(user_req.getString("username"));
                                spOrderListModel.setReq_id(user_req.getString("request_id"));
                                spOrderListModels.add(spOrderListModel);
                            }
                        }
                        else
                         {
                            showToast(jsonObject.getString("message"));
                         }
                        sp_requestAdapter = new Sp_RequestAdapter(Sp_reject.this, spOrderListModels, "4");
                        recyclerView.setAdapter(sp_requestAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("error", call.toString());
                Log.e("error", t.toString());
            }
        });


    }

    private void adapter_part()
    {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);


    }

    private void intializeUI() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        image_back = (ImageView) findViewById(R.id.image_back);

    }

    private void intializeValues() {

        image_back.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

            Intent i = new Intent(Sp_reject.this, Sp_HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();

    }


  /*  public void loadData() {
        int[] ordertxt = {R.string.order_txt, R.string.order_txt, R.string.order_txt};
        int[] loc_txt = {R.string.pick_from, R.string.pick_from, R.string.pick_from};
        int[] imageId = {R.drawable.req1, R.drawable.provider_two, R.drawable.provider_one};
        int[] issue_txt = {R.string.issue_txt, R.string.issue_txt_1, R.string.issue_txt};

        for (int i = 0; i < imageId.length; i++) {

            SpOrderListModel spOrderListModel = new SpOrderListModel();
            spOrderListModel.setImage(imageId[i]);
            spOrderListModel.setTxt_order_id(getResources().getString(ordertxt[i]));
            spOrderListModel.setTxt_loaction(getResources().getString(loc_txt[i]));
            spOrderListModel.setTxt_issue(getResources().getString(issue_txt[i]));

            arrayList.add(spOrderListModel);

        }
    }*/

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case  R.id.image_back:
                Intent i = new Intent(Sp_reject.this, Sp_HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

                break;
        }


    }
}