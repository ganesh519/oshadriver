package com.volive.oshadriver.activities;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.volive.oshadriver.R;
import com.volive.oshadriver.util.Chating_screen_status;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.SessionManagement;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static android.os.Build.VERSION_CODES.M;

public class Splash_Screen extends AppCompatActivity {
    static final Integer GPS_SETTINGS = 0x7;

    LocationRequest mLocationRequest;
    GoogleApiClient client;
    PendingResult<LocationSettingsResult> result;
    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static int SPLASH_TIME_OUT = 3000;

    PreferenceUtils preferenceUtils;
    SessionManagement sessionManagement;
    String langauage;
    ImageView image_splash;
    //ViewPager img_splash;
    SharedPreferences prefs = null;
    private Boolean isFirstRun;
    Handler handler;
    String strSelectLanguage;

    JSONObject object = new JSONObject();
    String freelancer_id, reciever_image, order_id, request_id,order_date, user_id,provider_id, message, image, auth_level, sender_id, receiver_Id;
    String type = "", title = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);
        preferenceUtils = new PreferenceUtils(this);
        sessionManagement = new SessionManagement(this);
        strSelectLanguage = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        setLanguageConversion();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (M <= Build.VERSION.SDK_INT)
                {
                    if (checkPermissions()) {

                        if (!preferenceUtils.getbooleanFromPreference(PreferenceUtils.LOGIN_TYPE, false)) {
                            Intent intent = new Intent(getApplicationContext(), LoginActivty.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        // TODO Auto-generated method stub
                    }
                }

                else {

                    Toast.makeText(getApplicationContext(), getString(R.string.please_take_permission), Toast.LENGTH_SHORT).show();
                  /*  Intent intent = new Intent(getApplication(), Splash_screen.class);
                    //  intent.putExtra("array", itemList);
                    startActivity(intent);*/
                }


                //data
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    // Create channel to show notifications.
                    String channelId = getString(R.string.default_notification_channel_id);
                    String channelName = getString(R.string.default_notification_channel_name);
                    NotificationManager notificationManager =
                            getSystemService(NotificationManager.class);
                    notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                            channelName, NotificationManager.IMPORTANCE_LOW));
                }
                if (getIntent().getExtras() != null) {
                    for (String key : getIntent().getExtras().keySet()) {
                        Object value = getIntent().getExtras().get(key);
                        Log.e("notification dta", "Key: " + key + " Value: " + value);
                        try {
                            object.put(key, value);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        type = object.getString("notification_type");
                        if (type.equalsIgnoreCase("new_request")) {
                            //title = object.getString("notification_title_en");
                            Intent intent = new Intent(getApplicationContext(), SPRequestDetailsActivity.class);
                            intent.putExtra("status_req", "1");
                            intent.putExtra("main_sts","0");
                            intent.putExtra("request", object.getString("request_id"));
                            intent.putExtra("req_user",object.getString("sender_id"));
                            intent.putExtra("order_id",object.getString("order_id"));
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("offer_accepted")) {
                            title = object.getString("notification_title_en");
                            Intent intent = new Intent(getApplicationContext(), Sp_pending.class);
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("cancel_request")) {
                            title = object.getString("notification_title_en");
                            Intent intent = new Intent(getApplicationContext(), Sp_reject.class);
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("new_offer")) {

                            title = object.getString("notification_title_en");
                            user_id = object.getString("sender_id");
                            provider_id = object.getString("receiver_id");
                            order_id = object.getString("order_id");
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("work_started")) {
                            title = object.getString("notification_title_en");

                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("request_completed")) {
                            title = object.getString("notification_title_en");
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            intent.putExtra("sts_value", "0");
                            startActivity(intent);
                        }
                        if (object.getString("notification_type").equalsIgnoreCase("new_message")) {
                            title = object.getString("notification_title_en");
                            sender_id = object.getString("sender_id");
                            receiver_Id = object.getString("receiver_id");
                            request_id = object.getString("request_id");
                            if (Chating_screen_status.isActivityVisible()) {
                                Intent intent = new Intent();
                                intent = new Intent("FCM-MESSAGE");
                                intent.putExtra("sender", sender_id);
                                intent.putExtra("rec_id", receiver_Id);
                                intent.putExtra("rcv_image", preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, ""));
                                intent.putExtra("rcv_image",object.getString("image"));
                                intent.putExtra("request", request_id);
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(Splash_Screen.this);
                                localBroadcastManager.sendBroadcast(intent);

                            } else {
                                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                                intent.putExtra("sender", receiver_Id);
                                intent.putExtra("rec_id", sender_id);
                                intent.putExtra("rcv_image",object.getString("image"));
                                intent.putExtra("rcv_image", preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, ""));
                                intent.putExtra("request", request_id);
                                startActivity(intent);
                            }
                        }
                    }


                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                        return;
            }

        }, 3000);

    }


    private boolean checkPermissions() {
        final Context context = Splash_Screen.this;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Splash_Screen.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
                int i = 0;
                if (grantResults != null && grantResults.length > 0) {
                    for (i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            break;
                        }
                    }
                    if (i == grantResults.length) {
                        Intent intent = new Intent(getApplication(), LoginActivty.class);
                        //  intent.putExtra("array", itemList);
                        startActivity(intent);
                    } else {
                        checkPermissions();
                    }
                }
                break;
        }
    }

    public void setLanguageConversion() {
        if (strSelectLanguage == null || strSelectLanguage.isEmpty()) {
            String languageToLoad = "ar"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("ar");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"ar");
        } else if (strSelectLanguage.equalsIgnoreCase("en")) {
            String languageToLoad = "en"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("en");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"en");
        } else if (strSelectLanguage.equalsIgnoreCase("ar")) {
            String languageToLoad = "ar"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("ar");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"ar");
        } else {
            String languageToLoad = "en"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("en");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"en");
        }
    }

}
