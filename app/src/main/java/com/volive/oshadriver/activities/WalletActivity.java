package com.volive.oshadriver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.volive.oshadriver.fragments.AddedFragment;
import com.volive.oshadriver.fragments.AllFragment;
import com.volive.oshadriver.fragments.PaidFragment;
import com.volive.oshadriver.R;

import java.util.ArrayList;
import java.util.List;

public class WalletActivity extends BaseActivity implements View.OnClickListener {

    ImageView imageView;
    ViewPager wallet_viewpager;
    TabLayout tabLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        intializeUI();
        intializeValues();

    }

    private void intializeUI() {
        imageView = (ImageView) findViewById(R.id.back_wallet);
        wallet_viewpager=findViewById(R.id.wallet_viewpager);
        tabLayout=findViewById(R.id.tabLayout);

    }

    private void intializeValues()
    {
        setupViewPager(wallet_viewpager);
        tabLayout.setupWithViewPager(wallet_viewpager);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent a   = new Intent(WalletActivity.this,Sp_HomeActivity.class);
                a.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(a);
                finish();
            }
        });

    }


  /*  @Override
    public void onBackPressed() {
        finish();
    }*/

    private void setupViewPager(ViewPager viewPager) {
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllFragment(), getString(R.string.all));
        adapter.addFragment(new PaidFragment(), getString(R.string.paid));
        adapter.addFragment(new AddedFragment(), getString(R.string.added));

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                //fragment_name.setText(fragmentname_list[i]);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            if(mFragmentTitleList.size()!=0){
                return mFragmentTitleList.size();
            }else
                return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
