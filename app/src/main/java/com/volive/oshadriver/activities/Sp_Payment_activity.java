package com.volive.oshadriver.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.adapters.PaymentAdapter;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.models.Payment_model;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;


import net.alhazmy13.hijridatepicker.date.gregorian.GregorianDatePickerDialog;
import net.alhazmy13.hijridatepicker.date.hijri.HijriDatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sp_Payment_activity extends BaseActivity implements /*HijriDatePickerDialog.OnDateSetListener,*/View.OnClickListener, GregorianDatePickerDialog.OnDateSetListener {

    ImageView image_back, filter_image;
    RecyclerView recyclerView;
    String activityName = "",UserID,language;
    EditText et_from_date, et_to_date;
    boolean isfromDate, isTodate = false;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    ArrayList<Payment_model>  payment_array = new ArrayList<>();
    PaymentAdapter paymentAdapter;
    String  strPrice = "",strFromDate = "",strToDate = "";

    TextView txt_received_amount ,txt_admin_amount,txt_remaining_balnce;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp__payment_activity);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            activityName = extras.getString("Activity");
        }
        preferenceUtils = new PreferenceUtils(Sp_Payment_activity.this);
        helperClass = new HelperClass(Sp_Payment_activity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        UserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        intializeUI();
        intializeValues();
        adapter_part();
        if (helperClass.checkInternetConnection(Sp_Payment_activity.this)) {
            data_filter("", "", "");
        } else {
            showToast(getResources().getString(R.string.check_internet));
        }
    }
    private void adapter_part() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
    }


    private void intializeUI() {

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        image_back = (ImageView) findViewById(R.id.image_back);
        filter_image = findViewById(R.id.filter_image);
        txt_remaining_balnce = findViewById(R.id.txt_remaining_balnce);
        txt_admin_amount = findViewById(R.id.txt_admin_amount);
        txt_received_amount = findViewById(R.id.txt_received_amount);
    }

    private void intializeValues() {

          image_back.setOnClickListener(this);
          filter_image.setOnClickListener(this);
          recyclerView.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

            Intent i = new Intent(Sp_Payment_activity.this, Sp_HomeActivity.class);
            i.putExtra("Activity", activityName);
            startActivity(i);
            finish();


    }



    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.filter_image:

                filter_dialog();
                break;

            case R.id.image_back:

                Intent i = new Intent(Sp_Payment_activity.this, Sp_HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("Activity", activityName);
                startActivity(i);
                finish();
                break;
        }
    }

    private void filter_dialog() {
        final Dialog dialog = new Dialog(Sp_Payment_activity.this,R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.requestfilter_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
//                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ImageView back_img_details = dialog.findViewById(R.id.back_img_details);
        Button btn_apply_filter = dialog.findViewById(R.id.btn_apply_filter);
        final EditText filter_by_percentage = dialog.findViewById(R.id.filter_by_percentage);
//                final LinearLayout status_layout = dialog.findViewById(R.id.status_layout);
//                View status_view = dialog.findViewById(R.id.status_view);
        final TextView filter_by_price = dialog.findViewById(R.id.filter_by_price);

        TextView txt_reset = dialog.findViewById(R.id.txt_reset);
       // RelativeLayout percentage_layout=dialog.findViewById(R.id.percentage_layout);
      //  percentage_layout.setVisibility(View.VISIBLE);
        final SeekBar rangeSeekbar8 = dialog.findViewById(R.id.rangeSeekbar8);
//                status_view.setVisibility(View.GONE);
//                status_layout.setVisibility(View.GONE);
        et_from_date = dialog.findViewById(R.id.et_from_date);
        et_to_date = dialog.findViewById(R.id.et_to_date);

        et_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isfromDate = true;
                isTodate = false;
                Calendar now = Calendar.getInstance();
                GregorianDatePickerDialog dpd = GregorianDatePickerDialog.newInstance(
                        Sp_Payment_activity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
                dpd.show(getFragmentManager(), "GregorianDatePickerDialog");
            }
        });

        et_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTodate = true;
                isfromDate = false;

                Calendar now = Calendar.getInstance();
                GregorianDatePickerDialog dpd = GregorianDatePickerDialog.newInstance(
                        Sp_Payment_activity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
                dpd.show(getFragmentManager(), "GregorianDatePickerDialog");

            }
        });
        txt_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                        filter_by_price.setText("");
             //   filter_by_percentage.setText("");
                et_from_date.setText("");
                et_to_date.setText("");
                strPrice = "";
            }
        });
        rangeSeekbar8.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                filter_by_price.setText(i + " SAR");
                strPrice = String.valueOf(i);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        back_img_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_apply_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  payment_array.clear();
                dialog.dismiss();

                strFromDate = et_from_date.getText().toString();
                strToDate = et_to_date.getText().toString();
                if (strFromDate.isEmpty())
                {
                    showToast(getResources().getString(R.string.select_from_date));
                }
                else if (strToDate.isEmpty())
                {
                    showToast(getResources().getString(R.string.select_to_date));
                }
                else  if (helperClass.checkInternetConnection(Sp_Payment_activity.this)) {
                    data_filter(strFromDate, strToDate, strPrice);
                } else {
                    showToast(getResources().getString(R.string.check_internet));
                }
            }
        });
    }

    @Override
    public void onDateSet(GregorianDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (isfromDate) {
            String date = "" + year + "-" + (++monthOfYear) + "-" + dayOfMonth;
            et_from_date.setText(date);
        } else if (isTodate) {
            String date = "" + year + "-" + (++monthOfYear) + "-" + dayOfMonth;
            et_to_date.setText(date);
        }
    }

    private void data_filter(String strFromDate, String strToDate, String strPrice)
    {
        final API_Services services  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification= null;
        call_notification = services.payments_history(Constant_keys.API_KEY,language,preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""),strFromDate,strToDate,strPrice);

        final ProgressDialog progressDoalog = new ProgressDialog(Sp_Payment_activity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful())
                {
                    Log.e("sjwlb",response.body().toString());
                    try {
                        JSONObject json_obejct =  new JSONObject(response.body().toString());
                        int status = json_obejct.getInt("status");
                        String   message = json_obejct.getString("message");

                        payment_array = new ArrayList<>();
                        if (status == 1)
                        {
                           // showToast(message);
                            String total_amount = json_obejct.getString("total_amount");
                            String admin_amount  = json_obejct.getString("admin_amount");
                            String remaining_balance = json_obejct.getString("remaining_balance");

                            txt_received_amount.setText(total_amount);
                            txt_admin_amount.setText(admin_amount);
                            txt_remaining_balnce.setText(remaining_balance);

                            JSONArray json_array =json_obejct.getJSONArray("payment_history");

                            for (int i=0; i< json_array.length();i++)
                            {
                                JSONObject json_payment = json_array.getJSONObject(i);
                                Payment_model payment_model = new Payment_model();
                                payment_model.setPay_request_id(json_payment.getString("request_id"));
                                payment_model.setPay_order_id(json_payment.getString("order_id"));
                                payment_model.setPay_user_id(json_payment.getString("user_id"));
                                payment_model.setPay_date(json_payment.getString("date"));
                                payment_model.setPay_offer_price(json_payment.getString("offer_price"));
                                payment_model.setPay_additional_amount(json_payment.getString("additional_amount"));
                                payment_model.setPay_provider_amount(json_payment.getString("provider_amount"));
                                payment_model.setPay_payment_method(json_payment.getString("payment_method"));
                                payment_model.setPay_payment_status(json_payment.getString("payment_status"));
                                payment_model.setPay_final_amount(json_payment.getString("final_amount"));
                                payment_model.setPay_profile_pic(json_payment.getString("profile_pic"));
                                payment_array.add(payment_model);
                                   /* payment_model.setPay_offer_price(json_payment.getString("offer_price"));
                                    payment_model.setPay_offer_price(json_payment.getString("offer_price"));
                                    payment_model.setPay_offer_price(json_payment.getString("offer_price"));*/
                            }
                        }
                        else
                        {
                            showToast(message);
                        }
                        paymentAdapter = new PaymentAdapter(Sp_Payment_activity.this,payment_array);
                        recyclerView.setAdapter(paymentAdapter);
                        paymentAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        progressDoalog.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Erro:" ,call.toString());
                Log.e("Error:",t.toString());

            }
        });

    }

}






















 /*   private void payment_details() {

        final API_Services services  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification= null;
        call_notification = services.payments_history(Constant_keys.API_KEY,language,"49","","","");

        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                if (response.isSuccessful())
                {
                    Log.e("sjwlb",response.body().toString());

                    try {
                        JSONObject json_obejct =  new JSONObject(response.body().toString());

                        int status = json_obejct.getInt("status");
                        String   message = json_obejct.getString("message");

                        payment_array = new ArrayList<>();
                        if (status == 1)
                        {
                            showToast(message);
                            String total_amount = json_obejct.getString("total_amount");
                            String admin_amount  = json_obejct.getString("admin_amount");
                            String remaining_balance = json_obejct.getString("remaining_balance");

                            JSONArray json_array =json_obejct.getJSONArray("payment_history");

                            for (int i=0; i< json_array.length();i++)
                            {
                                JSONObject json_payment = json_array.getJSONObject(i);

                                Payment_model payment_model = new Payment_model();

                                payment_model.setPay_request_id(json_payment.getString("request_id"));
                                payment_model.setPay_order_id(json_payment.getString("order_id"));
                                payment_model.setPay_user_id(json_payment.getString("user_id"));
                                payment_model.setPay_date(json_payment.getString("date"));
                                payment_model.setPay_offer_price(json_payment.getString("offer_price"));
                                payment_model.setPay_additional_amount(json_payment.getString("additional_amount"));
                                payment_model.setPay_provider_amount(json_payment.getString("provider_amount"));
                                payment_model.setPay_payment_method(json_payment.getString("payment_method"));
                                payment_model.setPay_payment_status(json_payment.getString("payment_status"));
                                payment_model.setPay_final_amount(json_payment.getString("final_amount"));
                                payment_model.setPay_profile_pic(json_payment.getString("profile_pic"));
                                payment_array.add(payment_model);
                                   *//* payment_model.setPay_offer_price(json_payment.getString("offer_price"));
                                    payment_model.setPay_offer_price(json_payment.getString("offer_price"));
                                    payment_model.setPay_offer_price(json_payment.getString("offer_price"));*//*
                            }
                        }

                        else
                        {

                            showToast(message);

                        }
                        paymentAdapter = new PaymentAdapter(Sp_Payment_activity.this,payment_array);
                        recyclerView.setAdapter(paymentAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }



            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
        UmmalquraCalendar now = new UmmalquraCalendar();
                HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                        Sp_Payment_activity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                Change the language to any of supported language
                dpd.setLocale(new Locale("ar"));
                dpd.show(getFragmentManager(), "Datepickerdialog");
        /Change the language to any of supported language
//                dpd.setLocale(new Locale("ar"));
//  dpd.show(getFragmentManager(), "Datepickerdialog");
           UmmalquraCalendar now = new UmmalquraCalendar();
                HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                        Sp_Payment_activity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

          @Override
    public void onDateSet(HijriDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (isfromDate) {
            String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
            et_from_date.setText(date);
        } else if (isTodate) {
            String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
            et_to_date.setText(date);

 }*/










