package com.volive.oshadriver.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateInvoiceActivity extends BaseActivity implements View.OnClickListener {

    ImageView back_img_details;
    TextView toolbar_title, txt_orderId, txt_customerName, txt_invoiceNo, txt_invoice_date,
            txt_pick_up, txt_drop_up, txt_subTotal, txt_coupon_price, txt_total_price;
    EditText txt_service_name, txt_estimated_price, txt_input_num, txt_input_num1, et_desc;
    LinearLayout add_service_layout, adding_service_layout;
    Button btn_send_invoice;
    AutoCompleteTextView issue_actv;
    LinearLayout issue_layout;
    String st_userId, language;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    String request_id = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_invoice);
        helperClass = new HelperClass(CreateInvoiceActivity.this);
        preferenceUtils = new PreferenceUtils(CreateInvoiceActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        st_userId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        initializeUi();
        initializeValues();
        set_values();
        invoice_alret();
    }

    private void set_values() {

        Date today = Calendar.getInstance().getTime();//getting date
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");//formating according to my need
        String date = formatter.format(today);
        txt_invoice_date.setText(date);
        txt_orderId.setText(getIntent().getStringExtra("order"));
        // txt_invoice_date.setText(getIntent().getStringExtra("invoiv"));
        txt_pick_up.setText(getIntent().getStringExtra("pick_up"));
        txt_drop_up.setText(getIntent().getStringExtra("drop"));
        txt_total_price.setText(getIntent().getStringExtra("price") + " " + "SAR");
        txt_customerName.setText(getIntent().getStringExtra("name_c"));
        request_id = getIntent().getStringExtra("req_id");


    }

    private void initializeUi() {
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_orderId = findViewById(R.id.txt_orderId);
        txt_pick_up = findViewById(R.id.txt_pick_up);
        txt_drop_up = findViewById(R.id.txt_drop_up);
        txt_customerName = findViewById(R.id.txt_customerName);
        txt_invoiceNo = findViewById(R.id.txt_invoiceNo);
        txt_invoice_date = findViewById(R.id.txt_invoice_date);
        txt_subTotal = findViewById(R.id.txt_subTotal);
        txt_coupon_price = findViewById(R.id.txt_coupon_price);
        txt_total_price = findViewById(R.id.txt_total_price);

        //ImageView
        back_img_details = findViewById(R.id.back_img_details);

        //Button
        btn_send_invoice = findViewById(R.id.btn_send_invoice);
        //EditText
        txt_input_num1 = findViewById(R.id.txt_input_num1);
        et_desc = findViewById(R.id.et_desc);

        //LinearLayout
//        add_service_layout = findViewById(R.id.add_service_layout);
        adding_service_layout = findViewById(R.id.adding_service_layout);
        issue_layout = findViewById(R.id.issue_layout);
        //AutoCompleteTextView
        issue_actv = findViewById(R.id.issue_actv);

    }

    private void initializeValues() {
        back_img_details.setOnClickListener(this);
//        add_service_layout.setOnClickListener(this);
        btn_send_invoice.setOnClickListener(this);


        final ArrayAdapter<String> modelArray = new ArrayAdapter<String>(CreateInvoiceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.issues_array));
        modelArray.setDropDownViewResource(android.R.layout.simple_list_item_1);
        issue_actv.setAdapter(modelArray);
        issue_actv.setCursorVisible(false);
        issue_actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                issue_actv.showDropDown();
//                warrenty = String.valueOf(position);
//                System.out.println("djfdsgv " + warrenty);
            }
        });

        issue_actv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                issue_actv.showDropDown();
            }
        });

        issue_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                issue_actv.showDropDown();
            }
        });

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.back_img_details:
                finish();
                break;
            case R.id.btn_send_invoice:

                String st_desc = et_desc.getText().toString();
                if (st_desc.isEmpty()) {
                    et_desc.setError(getString(R.string.enter_desc));
                }

                else {
                    create_invoice(st_desc);
                }
                break;

        }
    }

    private void create_invoice(String st_desc) {

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;

        call_retrofit = service.generate_invoice(Constant_keys.API_KEY, language, request_id,st_desc);

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(CreateInvoiceActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();

                String search = response.body().toString();
                Log.e("jbsdkiof", search);
                if (response.isSuccessful()) {

                    try {
                        JSONObject req_json = new JSONObject(response.body().toString());
                        int status = req_json.getInt("status");
                        if (status == 1)
                        {
                            //showToast(req_json.getString("message"));
                            alerteDialog();
                            //finish();
                        } else {
                            showToast(req_json.getString("message"));
                            //finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });


    }


    private void alerteDialog() {
        final Dialog dialog = new Dialog(CreateInvoiceActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.suceess_alert_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView text = dialog.findViewById(R.id.text);
        text.setText(getString(R.string.succes_invoice));
        TextView ok_txt = dialog.findViewById(R.id.ok_txt);
        ok_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(CreateInvoiceActivity.this, Sp_HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

    }


    private void invoice_alret() {

        final Dialog dialog = new Dialog(CreateInvoiceActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.suceess_alert_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView text = dialog.findViewById(R.id.text);
        text.setText(getString(R.string.please_genrate_invoice));
        TextView ok_txt = dialog.findViewById(R.id.ok_txt);
        ok_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }



}