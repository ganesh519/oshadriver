package com.volive.oshadriver.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.DataParser;
import com.volive.oshadriver.helperclasses.GPSTracker;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SPTrackActivity extends BaseActivity implements View.OnClickListener {

    ImageView back_img_details, chat_img, call_img, img_pin_location;
    TextView toolbar_title, txt_order_number, txt_customer_name, view_details_txt;
    LinearLayout ll_chat;
    ImageView customer_image;
    String langauge, stUserID,Req_id;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    String latitude_drop, longitude_drop;
    GoogleMap gMap;
    GPSTracker gpsTracker;
    FragmentManager fragmentManager;
    SupportMapFragment mapFragment;
    double strlatitide, strlongitude, ProviderLatitude, ProviderLongitude;
    Handler handler = new Handler();
    int delay = 10000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sptrack);
        preferenceUtils = new PreferenceUtils(SPTrackActivity.this);
        gpsTracker = new GPSTracker(SPTrackActivity.this);
        helperClass = new HelperClass(SPTrackActivity.this);
        langauge = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");

        initializeUI();
        initializeValues();

        txt_customer_name.setText(getIntent().getStringExtra("cust_name"));
        txt_order_number.setText(getIntent().getStringExtra("order_num"));
        Req_id = getIntent().getStringExtra("req_id");

        tracking_service();
        handler.postDelayed(new Runnable() {
            public void run() {
                //do something
                // get_details();
                tracking_service();
                handler.postDelayed(this, delay);
            }
        }, delay);

    }


    private void initializeUI() {

        fragmentManager = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
       // mapFragment.getMapAsync(SPTrackActivity.this);
        //ImageView
        back_img_details = findViewById(R.id.back_img_details);
        chat_img = findViewById(R.id.chat_img);
        ll_chat = findViewById(R.id.ll_chat);
        txt_customer_name = findViewById(R.id.txt_cust_name);
        img_pin_location = findViewById(R.id.img_pin_location);
//        call_img = findViewById(R.id.call_img);
        view_details_txt = findViewById(R.id.view_details_txt);
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_order_number = findViewById(R.id.txt_order_number);

        //CircleImageView
        customer_image = findViewById(R.id.customer_image);

    }

    private void initializeValues() {
        ll_chat.setOnClickListener(this);
        back_img_details.setOnClickListener(this);
        chat_img.setOnClickListener(this);
        view_details_txt.setOnClickListener(this);
        img_pin_location.setOnClickListener(this);
        Picasso.get()
                .load(Constant_keys.imagebaseurl + SPRequestDetailsActivity.recievr_image)
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.guest_user)
                .into(customer_image);
//        call_img.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.img_pin_location:
                if (!latitude_drop.isEmpty() || !longitude_drop.isEmpty()) {
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr=" + Double.parseDouble(latitude_drop) + "," + Double.parseDouble(longitude_drop)));
                    startActivity(intent);
                }
                break;
            case R.id.view_details_txt:
                finish();
                break;
            case R.id.back_img_details:
                finish();
                break;
            case R.id.chat_img:
                intent = new Intent(SPTrackActivity.this, ChatActivity.class);
                intent.putExtra("sender", SPRequestDetailsActivity.sender_id);
                intent.putExtra("rec_id", SPRequestDetailsActivity.reciever_id);
                intent.putExtra("rcv_image", SPRequestDetailsActivity.recievr_image);
                intent.putExtra("request", SPRequestDetailsActivity.request_id);
                startActivity(intent);
//                finish();
                break;

            case R.id.ll_chat:

                intent = new Intent(SPTrackActivity.this, ChatActivity.class);
                intent.putExtra("sender", SPRequestDetailsActivity.sender_id);
                intent.putExtra("rec_id", SPRequestDetailsActivity.reciever_id);
                intent.putExtra("rcv_image", SPRequestDetailsActivity.recievr_image);
                intent.putExtra("request", SPRequestDetailsActivity.request_id);
                startActivity(intent);

                break;

            /*case R.id.call_img:

                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + "966[1]4882288"));
                if (ActivityCompat.checkSelfPermission(SPTrackActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);*/


        }
    }


    private void tracking_service()
    {
            final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> call_retrofit = null;
            call_retrofit = service.tracking(Constant_keys.API_KEY, langauge, Req_id);
           /* final ProgressDialog progressDialog;
            progressDialog = new ProgressDialog(SPTrackActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setTitle(R.string.loading);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();*/
            call_retrofit.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                   // progressDialog.dismiss();
                    if (response.isSuccessful()) {
                        String search = response.body().toString();
                        Log.e("tracking", search);
                        try {
                            JSONObject req_json = new JSONObject(response.body().toString());
                            int status = req_json.getInt("status");

                            if (status == 1) {
                                JSONObject json_detaisl = req_json.getJSONObject("details");

                                ProviderLatitude = Double.parseDouble(json_detaisl.getString("latitude_drop"));
                                ProviderLongitude = Double.parseDouble(json_detaisl.getString("longitude_drop"));

                                latitude_drop = json_detaisl.getString("latitude_drop");
                                longitude_drop = json_detaisl.getString("longitude_drop");



                                JSONObject provider_details = json_detaisl.getJSONObject("provider_details");

                                strlatitide = Double.parseDouble(provider_details.getString("latitude"));
                                strlongitude = Double.parseDouble(provider_details.getString("longitude"));




                                mapFragment.getMapAsync(new OnMapReadyCallback() {
                                    @Override
                                    public void onMapReady(GoogleMap googleMap)
                                    {
                                        gMap = googleMap;
                                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.current_location_pic);
                                        BitmapDescriptor icon1 = BitmapDescriptorFactory.fromResource(R.drawable.drop_loc_pic);

                                        LatLng MylocationLatlong = new LatLng(strlatitide, strlongitude);
                                        LatLng Userlatlong = new LatLng(ProviderLatitude, ProviderLongitude);


                                        LatLng origin = new LatLng(strlatitide, strlongitude);
                                        LatLng dest = new LatLng(ProviderLatitude, ProviderLongitude);

                                        // Getting URL to the Google Directions API
                                        String url = getDirectionsUrl(origin, dest);

                                        FetchUrl downloadTask = new FetchUrl();

                                        // Start downloading json data from Google Directions API
                                        downloadTask.execute(url);

                                        gMap.addMarker(new MarkerOptions().position(MylocationLatlong).icon(icon));
                                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MylocationLatlong, 15));
                                        gMap.addMarker(new MarkerOptions().position(Userlatlong).icon(icon1));
                                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Userlatlong, 15));

                                    }
                                });
                            }
                            else {
                                showToast(req_json.getString("message"));
                            }


                                //mapingpoint();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    //progressDialog.dismiss();
                    Log.d("Error Call", ">>>>" + call.toString());
                    Log.d("Error", ">>>>" + t.toString());
                }
            });

        }








    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving&alternatives=true&key=AIzaSyCWNkrF-2U0Jc48DvvtydZ5Kv-gLTDIaOk";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0]);
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(getResources().getColor(R.color.colorPrimary));

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                gMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }

}













































