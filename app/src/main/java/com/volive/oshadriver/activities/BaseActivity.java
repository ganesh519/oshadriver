package com.volive.oshadriver.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.volive.oshadriver.R;


/**
 * Created by Hari on 19/02/16.
 */
public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    private ProgressDialog dialog = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Don't auto-show the keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    /**
     * Take px as input and converts to  dp
     * Ref https://www.captechconsulting.com/blogs/understanding-density-independence-in-android
     *
     * @param px px
     * @return px in dp
     */
    public float dpFromPx(final float px) {
        return px / getResources().getDisplayMetrics().density;
    }

    public float pxFromDp(final float dp) {
        return dp * getResources().getDisplayMetrics().density;
    }


    /**
     * set custom font type
     *
     * @param font      font name
     * @param textViews all text views
     */
    public void setTypeFace(String font, TextView... textViews) {
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/" + font);
        for (int i = 0; i < textViews.length; i++)
            textViews[i].setTypeface(face);
    }

    /**
     * set custom font type
     *
     * @param font    font name
     * @param buttons all buttons
     */
    public void setTypeFace(String font, Button... buttons) {
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/" + font);
        for (int i = 0; i < buttons.length; i++)
            buttons[i].setTypeface(face);
    }

    /**
     * set custom font type
     *
     * @param font      font name
     * @param editTexts all edit texts
     */
    public void setTypeFace(String font, EditText... editTexts) {
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/" + font);
        for (int i = 0; i < editTexts.length; i++)
            editTexts[i].setTypeface(face);
    }

    /**
     * set custom font type
     *
     * @param font         font name
     * @param radioButtons all radio buttons
     */
    public void setTypeFace(String font, RadioButton... radioButtons) {
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/" + font);
        for (int i = 0; i < radioButtons.length; i++)
            radioButtons[i].setTypeface(face);
    }

    public void showProgressDialog(String message) {
        if (dialog == null) dialog = new ProgressDialog(this);
        dialog.setMessage(message);
       dialog.setCancelable(true);
        try {
            if (!dialog.isShowing()) dialog.show();
           } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    protected void showSimpleAlert(String title, String message) {
        if (!this.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle(title).setMessage(message).setPositiveButton("OK", null).show();
        }
    }

    public void showAlert(String title,
                           String alertMsg,
                          String positiveActionName,
                          DialogInterface.OnClickListener positiveAction,
                          String negativeActionName,
                          DialogInterface.OnClickListener negativeAction,
                          boolean dual,
                          boolean cancelable) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(alertMsg);
        builder.setCancelable(cancelable);
        builder.setInverseBackgroundForced(true);
        if (positiveActionName != null && positiveAction != null) {
            builder.setPositiveButton(positiveActionName, positiveAction);
        } else {

            if (positiveActionName != null) {
                builder.setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
            } else {
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
        }

        if (dual) {
            if (negativeActionName != null && negativeAction != null) {
                builder.setNegativeButton(negativeActionName, negativeAction);
            } else {
                if (negativeActionName != null) {
                    builder.setNegativeButton(negativeActionName, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                } else {
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                }
            }
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        try {
            if (dialog.isShowing()) dialog.dismiss();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public  boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

}