package com.volive.oshadriver.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.erikagtierrez.multiple_media_picker.Gallery;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.adapters.GalleryAdapter;
import com.volive.oshadriver.adapters.Vechicle_ImagesAdapter;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.GPSTracker;
import com.volive.oshadriver.helperclasses.GalleryUriToPath;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.models.Vechicle_images_model;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;


import net.alhazmy13.hijridatepicker.date.hijri.HijriDatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;


public class ProfileActivity extends BaseActivity implements View.OnClickListener, HijriDatePickerDialog.OnDateSetListener{

    ImageView back_img_details, profile_add, profile_add_img, expand_img, current_loc;
    TextView toolbar_title, txt_edit, provider_name_txt,txt_profile_birth_date, provider_name_email_txt,txt_change_password,
            txt_save, delete_document_layout,txt_no_images, txt_location_address,txt_select_image;
    CircleImageView profile_image;
    LinearLayout provider_layout;
    EditText txt_profile_name, txt_profile_id,
            txt_referral_Code, ed_profile_phn_number,
            ed_prf_password, ed_prf_cnf_password, txt__choose_location_address;
    EditText edit_profile_birth_date;
    View name_view, gender_view, birth_view, email_view, number_view, password_view, conf_pwd_view;
    String activityName = "",language,st_UserID,st_profile,st_username,strImageId;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    RecyclerView rec_car_profile,rec_car_images;
    public static ArrayList<String> selectable_id = new ArrayList<>();
    private GalleryAdapter galleryAdapter;
    static final int OPEN_MEDIA_PICKER = 1;
    int CAMERA_CAPTURE = 2;
    int PICK_IMAGE = 3 ;
    public static List<String> select_img_list = new ArrayList<>();
    String multi_image_path = "empty";
    ArrayList<MultipartBody.Part> image_array = new ArrayList<>();
    ArrayList<Vechicle_images_model> vechicle_images_models = new ArrayList<>();
    Vechicle_ImagesAdapter vechicle_imagesAdapter;
    Vechicle_images_model vechicle_images_model;
    LinearLayout upload_images_layout;
    String st_email,st_mobile,st_name,st_password,st_cnf_pwd,stDob,str_latitude = "",str_longitude = "",st_Profile_image = " ";
    String Picked_IDPath,Picked_licencePath,Picked_insurance,Picked_profile = "empty";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_provider);;
        preferenceUtils = new PreferenceUtils(ProfileActivity.this);
        helperClass = new HelperClass(ProfileActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        st_UserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        st_Profile_image = preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,"");

        initializeUi();
        initializeValues();


        if (preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "").isEmpty())
        {
            profile_image.setImageResource(R.drawable.guest_user);

        }
        else {
            // validate.loadImage(true, ProfileActivity.this, ProfileActivity.image_url, profile_image_select);
            Glide.with(getApplicationContext()).load(preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "")).into(profile_image);


        }
        setValues();
        if (!isNetworkAvailable()) {
            showToast(getResources().getString(R.string.please_check_your_network_connection));
            return;
        } else {
            get_profile();
        }

    }

    private void setValues() {

        txt_profile_birth_date.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.DOB,""));
        edit_profile_birth_date.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.DOB,""));

        ed_profile_phn_number.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Mobile,""));
        txt_referral_Code .setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Referal_code,""));
        ed_prf_password.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.password_new,""));
        ed_prf_cnf_password.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.password_new,""));
        provider_name_email_txt.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Email,""));
        provider_name_txt.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.UserName,""));

        rec_car_profile.setHasFixedSize(true);
        rec_car_profile.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayout.HORIZONTAL,false));




    }
    private void initializeUi() {
        //ImageView
        txt_select_image = findViewById(R.id.txt_select_image);
        upload_images_layout = findViewById(R.id.upload_images_layout);
        rec_car_images = findViewById(R.id.rec_car_images);
        rec_car_profile = findViewById(R.id.rec_car_profile);
        back_img_details = findViewById(R.id.back_img_details);
        profile_add = findViewById(R.id.profile_add);
        profile_add_img = findViewById(R.id.profile_add_img);
        expand_img = findViewById(R.id.expand_map_img);
        current_loc = findViewById(R.id.current_loc);
        txt_change_password = findViewById(R.id.txt_change_password);
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_edit = findViewById(R.id.txt_edit);
        provider_name_txt = findViewById(R.id.provider_name_txt);
        provider_name_email_txt = findViewById(R.id.provider_name_email_txt);
        txt_save = findViewById(R.id.txt_save);
        edit_profile_birth_date = findViewById(R.id.edit_profile_birth_date);
        //EditText
//        txt_profile_name = findViewById(R.id.txt_profile_name);
        txt_profile_birth_date = findViewById(R.id.txt_profile_birth_date);
        txt_referral_Code = findViewById(R.id.txt_referral_Code);
        ed_profile_phn_number = findViewById(R.id.ed_profile_phn_number);
        ed_prf_password = findViewById(R.id.ed_prf_password);
        ed_prf_cnf_password = findViewById(R.id.ed_prf_cnf_password);
        //CircleImageView
        profile_image = findViewById(R.id.profile_image);
        //LinearLayout
        provider_layout = findViewById(R.id.provider_layout);
        delete_document_layout = findViewById(R.id.delete_document_layout);

        birth_view = findViewById(R.id.birth_view);
        email_view = findViewById(R.id.email_view);
        number_view = findViewById(R.id.number_view);
        password_view = findViewById(R.id.password_view);
        conf_pwd_view = findViewById(R.id.conf_pwd_view);
        txt_no_images = findViewById(R.id.txt_no_images);

        ed_prf_cnf_password.setEnabled(false);
        ed_prf_password.setEnabled(false);
        ed_profile_phn_number.setEnabled(false);
        txt_referral_Code.setEnabled(false);


    }

    private void initializeValues() {
        profile_add_img.setOnClickListener(this);
        txt_select_image.setOnClickListener(this);
        upload_images_layout.setOnClickListener(this);
        back_img_details.setOnClickListener(this);
        txt_edit.setOnClickListener(this);
        txt_save.setOnClickListener(this);
        txt_profile_birth_date.setOnClickListener(this);
        ed_profile_phn_number.setOnClickListener(this);
        txt_referral_Code.setOnClickListener(this);
        edit_profile_birth_date.setOnClickListener(this);
        delete_document_layout.setOnClickListener(this);
        txt_change_password.setOnClickListener(this);
        txt_no_images.setOnClickListener(this);
        rec_car_images.setOnClickListener(this);
    }

    private void get_profile()
    {
        final API_Services services  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification= null;
        call_notification = services.get_profile(Constant_keys.API_KEY,language,st_UserID);
        final ProgressDialog progressDoalog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();
        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful())
                {
                    Log.e("sjwlb",response.body().toString());
                    try {
                        JSONObject json_object = new JSONObject(response.body().toString());
                        int status = json_object.getInt("status");
                        String base_url = json_object.getString("base_url");

                        if (status == 1)
                        {

                            JSONObject sp_profile = json_object.getJSONObject("profile");

                            String user_image = sp_profile.getString("profile_pic");
                            String dob = sp_profile.getString("dob");
                            String password = sp_profile.getString("password");
                            String request_type  = sp_profile.getString("request_type");

                            txt_referral_Code.setText(sp_profile.getString("referal_code"));

                            ed_prf_password.setText(password);
                            ed_prf_cnf_password.setText(password);

                            JSONArray car_images = sp_profile.getJSONArray("user_documents");
                            Log.e("waqkn",car_images.toString());
                            vechicle_images_models = new ArrayList<>();
                            for (int i = 0;i<car_images.length();i++)
                            {
                                JSONObject json_car = car_images.getJSONObject(i);

                                Vechicle_images_model vechicle_images_model = new Vechicle_images_model();

                                vechicle_images_model.setImage_id(json_car.getString("image_id"));
                                vechicle_images_model.setImage_pic(base_url + json_car.getString("image_name"));
                                vechicle_images_model.setPic_type(json_car.getString("image_type"));
                                vechicle_images_models.add(vechicle_images_model);
                            }
                        }
                        else {

                            showToast(json_object.getString("message"));
                        }
                        vechicle_imagesAdapter = new Vechicle_ImagesAdapter(getApplicationContext(),vechicle_images_models);
                        rec_car_profile.setAdapter(vechicle_imagesAdapter);

                        if (vechicle_images_models.size() > 0) {
                            vechicle_images_model = new Vechicle_images_model();
//                if checkbox is visible
                            if (vechicle_images_model.isVisible()) {
                                for (int i = 0; i < vechicle_images_models.size(); i++) {
                                    vechicle_images_models.get(i).setVisible(false);
                                }
                                vechicle_imagesAdapter.notifyDataSetChanged();
                            } else {
//                    if checkbox is invisible
                                for (int i = 0; i < vechicle_images_models.size(); i++) {
                                    vechicle_images_models.get(i).setVisible(true);
                                }
                                vechicle_imagesAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Error",call.toString());
            }
        });
    }
    private void arbic_calender() {
        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);
        UmmalquraCalendar now = new UmmalquraCalendar();
        HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                ProfileActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        //Change the language to any of supported language
        // dpd.setLocale(new Locale("ar"));
        dpd.show(getFragmentManager(), "Datepickerdialog");

       /* Calendar prsnt = Calendar.getInstance();
        GregorianDatePickerDialog date = GregorianDatePickerDialog.newInstance(
                SPSignUp.this,
                prsnt.get(Calendar.YEAR),
                prsnt.get(Calendar.MONTH),
                prsnt.get(Calendar.DAY_OF_MONTH));
        date.show(getFragmentManager(), "GregorianDatePickerDialog");*/


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.profile_add_img:

                selectImage();


                break;
            case R.id.txt_select_image:
                browse();
                //selectImage();
                break;

            case R.id.back_img_details:
                    Intent intent = new Intent(ProfileActivity.this, Sp_HomeActivity.class);
                    intent.putExtra("Activity", activityName);
                    startActivity(intent);
                    finish();
                break;
            case  R.id.txt_change_password:
                pass_word_change();
                break;
            case R.id.txt_edit:
                txt_save.setVisibility(View.VISIBLE);
                txt_edit.setVisibility(View.GONE);
                profile_add_img.setVisibility(View.VISIBLE);
                profile_add.setVisibility(View.GONE);
                upload_images_layout.setVisibility(View.VISIBLE);
                rec_car_images.setVisibility(View.VISIBLE);
             //actions
                txt_profile_birth_date.setVisibility(View.GONE);
                edit_profile_birth_date.setVisibility(View.VISIBLE);
                ed_prf_cnf_password.setEnabled(true);
                ed_prf_password.setEnabled(true);
                ed_profile_phn_number.setEnabled(true);
                txt_referral_Code.setEnabled(true);
                delete_document_layout.setVisibility(View.VISIBLE);

                break;
            case R.id.txt_save:


                if (vechicle_images_models.size() > 0) {
                    vechicle_images_model = new Vechicle_images_model();
//                if checkbox is visible
                    if (vechicle_images_model.isVisible()) {
                        for (int i = 0; i < vechicle_images_models.size(); i++) {
                            vechicle_images_models.get(i).setVisible(false);
                        }
                        vechicle_imagesAdapter.notifyDataSetChanged();
                    } else {
//                    if checkbox is invisible
                        for (int i = 0; i < vechicle_images_models.size(); i++) {
                            vechicle_images_models.get(i).setVisible(true);
                        }
                        vechicle_imagesAdapter.notifyDataSetChanged();
                    }
                }
                rec_car_images.setVisibility(View.GONE);
                upload_images_layout.setVisibility(View.GONE);
                txt_edit.setVisibility(View.VISIBLE);
                txt_save.setVisibility(View.GONE);
                profile_add_img.setVisibility(View.GONE);
                profile_add.setVisibility(View.VISIBLE);
                //actions
                txt_profile_birth_date.setVisibility(View.VISIBLE);
                edit_profile_birth_date.setVisibility(View.GONE);
                ed_prf_cnf_password.setEnabled(false);
                ed_prf_password.setEnabled(false);
                ed_profile_phn_number.setEnabled(false);
                txt_referral_Code.setEnabled(false);
                st_email = provider_name_email_txt.getText().toString();
                st_password = ed_prf_password.getText().toString();
                st_cnf_pwd = ed_prf_cnf_password.getText().toString();
                st_mobile = ed_profile_phn_number.getText().toString();
                st_username = provider_name_txt.getText().toString();
                st_name = provider_name_txt.getText().toString();
                stDob = edit_profile_birth_date.getText().toString();

                if (st_email.isEmpty())
                {
                    provider_name_email_txt.setError(getString(R.string.enter_your_email));
                }
                else if (st_mobile.isEmpty())
                {
                    ed_profile_phn_number.setError(getString(R.string.enter_number));
                }else if (!isNetworkAvailable()) {
                    showToast(getResources().getString(R.string.please_check_your_network_connection));
                    return;
                } else {
                    edit_profile_part();
                }

                break;
            case R.id.edit_profile_birth_date:
                //date_picker();
                arbic_calender();
                break;
            case R.id.delete_document_layout:
                getSeletedIssuesIDs();

                if (!strImageId.isEmpty())
                {
                    if (helperClass.checkInternetConnection(ProfileActivity.this))
                    {
                        delImgService();
                    } else
                        {
                        showToast(getResources().getString(R.string.please_check_your_network_connection));
                        //helperClass.displaySnackbar(provider_profileLayout, getResources().getString(R.string.plase_check_ur_internet_connection));
                    }
                }
                break;

        }
    }


    @Override
    public void onDateSet(HijriDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        edit_profile_birth_date.setText(date);
        stDob = edit_profile_birth_date.getText().toString();
    }



    private void edit_profile_part()
    {
        File file = null;
        //document_list = new ArrayList<>();
        MultipartBody.Part image_profile = null;
        if (!Picked_profile.equals("empty")) {
            file = new File(Picked_profile);
            RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(Picked_profile)), file);
            image_profile = MultipartBody.Part.createFormData("profile_pic", file.getName(), requestBody);
            Log.d(" profile_pic", ">>>>>>>>>>" + image_profile);
            Log.e("alm",Picked_profile);
            //preferenceUtils.saveString(PreferenceUtils.IMAGE,PickedImgPath);
        }

        RequestBody r_api_key = RequestBody.create(MediaType.parse("multipart/form-data"), Constant_keys.API_KEY);
        RequestBody r_lang = RequestBody.create(MediaType.parse("multipart/form-data"), language);
        RequestBody r_user_id = RequestBody.create(MediaType.parse("multipart/form-data"), st_UserID);
        RequestBody r_username= RequestBody.create(MediaType.parse("multipart/form-data"), st_username);
        RequestBody r_Mail = RequestBody.create(MediaType.parse("multipart/form-data"), st_email);
        RequestBody r_phone = RequestBody.create(MediaType.parse("multipart/form-data"), st_mobile);
        RequestBody r_name = RequestBody.create(MediaType.parse("multipart/form-data"), st_name);
        RequestBody r_password = RequestBody.create(MediaType.parse("multipart/form-data"), st_password);
        RequestBody r_confirm_password = RequestBody.create(MediaType.parse("multipart/form-data"), st_cnf_pwd);
        RequestBody r_dob = RequestBody.create(MediaType.parse("multipart/form-data"), stDob);
        RequestBody r_Latitude = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_Longitude = RequestBody.create(MediaType.parse("multipart/form-data"), "");

        RequestBody r_idProof = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_ins = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_licence = RequestBody.create(MediaType.parse("multipart/form-data"), "");

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        callRetrofit = service.edit_profile(r_api_key, r_lang, r_user_id,
                r_username,r_name, r_Mail, r_phone, r_password, r_confirm_password,
                r_dob,image_profile,r_idProof,r_ins,r_licence, image_array,
                r_Latitude, r_Longitude);


        Log.e("data", callRetrofit.toString());
        final ProgressDialog progressDoalog = new ProgressDialog(ProfileActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();
        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    Log.d("Registration", "response  >>" + searchResponse.toString());
                    try {
                        JSONObject regObject = new JSONObject(searchResponse);
                        int status = regObject.getInt("status");
                        String message = regObject.getString("message");
                        //    String base_url = regObject.getString("base_url");

                        if (status ==1 )
                        {
                            showToast(message);

                            //  Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONObject user_json = regObject.getJSONObject("details");
                                String name = user_json.getString("name");
                                String username = user_json.getString("username");
                                String email = user_json.getString("email");
                                String phone = user_json.getString("phone");
                                String profile_pic = user_json.getString("profile_pic");
                                //   String reeferal_code = user_json.getString("")
                                preferenceUtils.saveString(PreferenceUtils.User, name);
                                preferenceUtils.saveString(PreferenceUtils.UserName, username);
                                preferenceUtils.saveString(PreferenceUtils.Email, email);
                                preferenceUtils.saveString(PreferenceUtils.Mobile, phone);
                              //  preferenceUtils.saveString(PreferenceUtils.IMAGE, Constant_keys.imagebaseurl + profile_pic);
                                Glide.with(getApplicationContext()).load(Constant_keys.imagebaseurl + profile_pic).into(profile_image);


                            get_profile();


                        }
                        else {
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        Log.e("error", e.getMessage());


                    }
                }
            }


            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });



    }

    public void browse() {
        Intent intent = new Intent(this, Gallery.class);
        // Set the title
        intent.putExtra("title", "Select media");
        // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
        intent.putExtra("mode", 2);
        //intent.putExtra("maxSelection", 3); // Optional
        startActivityForResult(intent, OPEN_MEDIA_PICKER);
    }

    private void selectImage() {

        final CharSequence[] options = {getResources().getString(R.string.takeaphoto), getResources().getString(R.string.choosefrmgallery)};

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(getResources().getString(R.string.photos));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {
                if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.takeaphoto))) {
                    cameraIntent();
                } else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.choosefrmgallery))) {
                    choosefromgallery();

                }
                else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });

        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE);
    }

    private void choosefromgallery() {
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }



    private void delImgService()
    {
            final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> callRetrofit = null;
            callRetrofit = service.delete_img_service(Constant_keys.API_KEY, "en", selectable_id);
            final ProgressDialog progressDoalog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
            progressDoalog.setCancelable(false);
            progressDoalog.setMessage(getResources().getString(R.string.please_wait));
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDoalog.show();
            callRetrofit.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                    if (response.isSuccessful()) {
                        String searchResponse = response.body().toString();
                        Log.d("delete", "response  >>" + searchResponse.toString());
                        try {
                            JSONObject loginObject = new JSONObject(searchResponse);
                            int status = loginObject.getInt("status");
                            String message = loginObject.getString("message");
                            if (status == 1) {
                                showToast(message);
                                //helperClass.displaySnackbar(provider_profileLayout, message);
                               // imagelistSt.clear();
                              //  isProfile = true;
                                get_profile();

                            } else {
                                showToast(message);
                             //   helperClass.displaySnackbar(provider_profileLayout, message);
                            }

                        } catch (JSONException e) {
                            Log.e("error", e.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                    Log.d("Error Call", ">>>>" + call.toString());
                    Log.d("Error", ">>>>" + t.toString());
                }
            });
        }


    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        Log.d("MIME_TYPE_EXT", extension);
        if (extension != null && extension != "") {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            //  Log.d("MIME_TYPE", type);
        } else {
            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            type = fileNameMap.getContentTypeFor(url);
        }
        return type;
    }



    private void pass_word_change() {


        final Dialog dialogOffer = new Dialog(ProfileActivity.this, R.style.MyAlertDialogTheme);
        dialogOffer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogOffer.setContentView(R.layout.change_pwd_dialog);
        dialogOffer.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        dialogOffer.setCanceledOnTouchOutside(true);
        dialogOffer.setCancelable(true);
        dialogOffer.show();
        dialogOffer.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ImageView back_img_details = dialogOffer.findViewById(R.id.back_img_details);
        final EditText pswrd_edit_text = dialogOffer.findViewById(R.id.pswrd_edit_text);
        final EditText conform_pwd_edit_text = dialogOffer.findViewById(R.id.conform_pwd_edit_text);
        Button btn_save = dialogOffer.findViewById(R.id.btn_save);
        back_img_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOffer.dismiss();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String st_pwd,st_cnf_pwd;
                st_pwd = pswrd_edit_text.getText().toString();
                st_cnf_pwd = conform_pwd_edit_text.getText().toString();
                if (st_pwd.isEmpty())
                {
                    pswrd_edit_text.setError(getString(R.string.enter_password));
                }
                else if (st_cnf_pwd.isEmpty())
                {
                    conform_pwd_edit_text.setError(getString(R.string.conf_pwd_enter));
                }
                else {
                    dialogOffer.dismiss();
                    change_pwd(st_pwd,st_cnf_pwd);
                }
            }

    });
}

    private void change_pwd(final String st_pwd, String st_cnf_pwd)
    {
            final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> call_logout = null;
            call_logout = service.user_password_change(Constant_keys.API_KEY, language, st_UserID,st_pwd,st_cnf_pwd);
            final ProgressDialog progressdialog;
            progressdialog = new ProgressDialog(ProfileActivity.this);
            progressdialog.setCancelable(false);
            progressdialog.setMessage(getString(R.string.loading));
            progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressdialog.show();
            call_logout.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    progressdialog.dismiss();
                    Log.e("ajkldhn", response.body().toString());
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                showToast(jsonObject.getString("message"));
                                preferenceUtils.saveString(PreferenceUtils.password_new,st_pwd);
                                get_profile();
                                /*Intent intent = new Intent(ProfileActivity.this, LoginActivty.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                drawerLayout.closeDrawers();*/
                            }
                            else {
                                showToast(jsonObject.getString("message"));
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                }
            });

        }
    private void getSeletedIssuesIDs() {
        if (!selectable_id.isEmpty()) {
            String select_id_array = selectable_id.toString();
            strImageId = select_id_array.substring(1, select_id_array.length() - 1);
            Log.e("string_services_ids", "onClick: " + strImageId);
        } else {
            strImageId = "";
        }
    }

    private void date_picker() {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(ProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        /*      Your code   to get date and time    */
                        selectedmonth = selectedmonth + 1;
//                        et_date.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
                        edit_profile_birth_date.setText("" + selectedyear + "-" + selectedmonth + "-" + selectedday);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle(getString(R.string.select_date));
                mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDatePicker.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
            Intent i = new Intent(ProfileActivity.this, Sp_HomeActivity.class);
            i.putExtra("Activity", activityName);
            startActivity(i);
            finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
            if (selectionResult.size() > 0) {
                MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[selectionResult.size()];
                for (int index = 0; index < selectionResult.size(); index++) {

                    select_img_list.add(selectionResult.get(index));

                    multi_image_path = selectionResult.get(index);
                    String PickedImgPath = selectionResult.get(index);
                    System.out.println("path" + PickedImgPath);

                    File file = new File(PickedImgPath);
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    surveyImagesParts[index] = MultipartBody.Part.createFormData("images[]", file.getName(), surveyBody);
                    image_array.add(MultipartBody.Part.createFormData("images[]", file.getName(), surveyBody));
                    //txt_photos.setText("sample file's are selected");
                    Log.e("sa",image_array.toString());
                    String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
                    //txt_photos.setText(filename);
                }

                GridLayoutManager mLayoutManager = new GridLayoutManager(ProfileActivity.this, 3);
                rec_car_images.setLayoutManager(mLayoutManager);
                galleryAdapter = new GalleryAdapter(ProfileActivity.this, select_img_list);
                rec_car_images.setAdapter(galleryAdapter);
            }
        }
        else if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }

        else if (requestCode == PICK_IMAGE)
        {

            if (resultCode == RESULT_OK)
            {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                 Picked_profile = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                    Picked_profile = GalleryUriToPath.getPath(getApplicationContext(), picUri);
                    String file_profile = Picked_profile.substring(Picked_profile.lastIndexOf("/") + 1);
                    //txt_image_only.setText(filename);
                  //  txt_profile_photo.setText(file_profile);

                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                    profile_image.setImageBitmap(bm);
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                //  String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
                // txt_image_only.setText("photo Id has selected");
                c.close();
            }
        }



    }


    private void onCaptureImageResult(Intent data)
    {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            Picked_profile = destination.getAbsolutePath();
            String file_profile = Picked_profile.substring(Picked_profile.lastIndexOf("/") + 1);
            //txt_profile_photo.setText(file_profile);
            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        profile_image.setImageBitmap(thumbnail);
        //String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
        // txt_image_only.setText(filename);
        // txt_image_only.setText("photo Id has selected");

    }



}
