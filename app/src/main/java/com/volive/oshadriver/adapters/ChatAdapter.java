package com.volive.oshadriver.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.models.Chat_Model;
import com.volive.oshadriver.util.PreferenceUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;



/**
 * Created by swetha on 11/28/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    PreferenceUtils preferenceUtils;
    Context context;
    ArrayList<Chat_Model> chatModels;
    String userimage = "", receiverimage = "";
    String userid ;
    int count;
    String  reciever_image, base_path, sender_image,st_msg_type;


    public ChatAdapter(Activity chating_activity, ArrayList<Chat_Model> chat_model, String user_id, String reciever_image) {

        this.context = chating_activity;
        this.chatModels = chat_model;
        this.userid = user_id;
        count = this.chatModels.size();
        this.reciever_image = reciever_image;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        preferenceUtils = new PreferenceUtils(context);
        userid = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        userimage = preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "");
        st_msg_type = chatModels.get(position).getMessage_type();

        if (chatModels.get(position).getSender_id().equalsIgnoreCase(userid)){
            holder.relative_sender.setVisibility(View.VISIBLE);
            holder.linear_receiver.setVisibility(View.GONE);
            //Glide.with(context).load(Constants.imagebaseurl + userimage).into(holder.image_sender);

            holder.text_sendertime.setText(chatModels.get(position).getDate());


            if (st_msg_type.equalsIgnoreCase("text")) {
                holder.sender_img_msg.setVisibility(View.GONE);
                holder.text_sendermessage.setVisibility(View.VISIBLE);
                holder.text_sendermessage.setText(chatModels.get(position).getMessage());
            }
            else if (st_msg_type.equalsIgnoreCase("image") || st_msg_type.equalsIgnoreCase("0")) {
                holder.text_sendermessage.setVisibility(View.GONE);
                holder.sender_img_msg.setVisibility(View.VISIBLE);
                Picasso.get()

                        .load(Constant_keys.imagebaseurl + chatModels.get(position).getImage())
                        .error(R.drawable.loading_new)
                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                        .into(holder.sender_img_msg);
               // helperClass.loadImage(true, context, arrayList.get(position).getMessage(), holder.receiver_img);
            }


             Picasso.get()
                    .load(Constant_keys.imagebaseurl + chatModels.get(position).getSender_image())
                    .error(R.drawable.guest_user)
                     .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                    .into(holder.image_sender);
        }
        else  {
            holder.relative_sender.setVisibility(View.GONE);
            holder.linear_receiver.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(Constant_keys.imagebaseurl + reciever_image)
                    .error(R.drawable.loading_new)
                    .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                    .into(holder.image_receiver);
           // Glide.with(context).load(Constants.imagebaseurl + receiverimage).into(holder.image_receiver);
            holder.text_receivertime.setText(chatModels.get(position).getDate());
            if (st_msg_type.equalsIgnoreCase("text")) {
                holder.receiver_img_msg.setVisibility(View.GONE);
                holder.text_receivermessage.setVisibility(View.VISIBLE);
                holder.text_receivermessage.setText(chatModels.get(position).getMessage());
               // holder.receiver_txt_msg.setText(chatHistoryModel.getMessage());
            }
            else if (st_msg_type.equalsIgnoreCase("image") || st_msg_type.equalsIgnoreCase("0")) {
                holder.text_receivermessage.setVisibility(View.GONE);
                holder.receiver_img_msg.setVisibility(View.VISIBLE);
                Picasso.get()
                        .load(Constant_keys.imagebaseurl + chatModels.get(position).getImage())
                        .error(R.drawable.loading_new)
                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                        .into(holder.receiver_img_msg);
               // helperClass.loadImage(true, context, arrayList.get(position).getMessage(), holder.receiver_img);
            }





        }

        holder.sender_img_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog_show_img(position);

            }
        });

        holder.receiver_img_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog_show_img(position);

            }
        });


    }

    private void dialog_show_img(int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //  dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        // Include dialog.xml file
        dialog.setContentView(R.layout.zoomimage);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();

//                    TouchImageView img_zoom = (TouchImageView) dialog.findViewById(R.id.img_zoom);
        ImageView img_close = (ImageView) dialog.findViewById(R.id.img_close);
        ImageView img_chat = dialog.findViewById(R.id.img_chat);

        // img_chat.setImageResource();
        Picasso.get()
                .load(Constant_keys.imagebaseurl + chatModels.get(position).getImage())
                .error(R.drawable.loading_new)
                .into(img_chat);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }



    @Override
    public int getItemCount() {
        return chatModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_receivermessage, text_receivertime, text_sendermessage, text_sendertime;
        CircleImageView image_receiver, image_sender;
        LinearLayout linear_receiver;
        RelativeLayout relative_sender;
        RoundedImageView sender_img_msg, receiver_img_msg;

        public ViewHolder(View itemView) {
            super(itemView);
            text_receivermessage = itemView.findViewById(R.id.text_receiver_message);
            text_receivertime = itemView.findViewById(R.id.text_receiver_time);
            text_sendermessage = itemView.findViewById(R.id.text_sendermessage);
            text_sendertime = itemView.findViewById(R.id.text_sendertime);
            image_receiver = itemView.findViewById(R.id.image_receiver);
            image_sender = itemView.findViewById(R.id.image_sender);
            linear_receiver = itemView.findViewById(R.id.linear_receiver);
            relative_sender = itemView.findViewById(R.id.relative_sender);
            sender_img_msg = itemView.findViewById(R.id.sender_img);
            receiver_img_msg = itemView.findViewById(R.id.receiver_img);
        }
    }
}
