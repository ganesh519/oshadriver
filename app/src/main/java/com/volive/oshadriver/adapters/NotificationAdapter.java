package com.volive.oshadriver.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.volive.oshadriver.R;
import com.volive.oshadriver.models.Notification_model;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.Notifi_Holder> {

    Context context;
    ArrayList<Notification_model> notify_list;

    public NotificationAdapter(Context context, ArrayList<Notification_model> notify_list_array) {
        this.context = context;
        this.notify_list = notify_list_array;
    }

    @NonNull
    @Override
    public Notifi_Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notify_item_view, parent, false);

        return new Notifi_Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Notifi_Holder holder, int position) {
       Notification_model notification_model = notify_list.get(position);

        holder.text_message.setText(notify_list.get(position).getNotification_notification_title());
        holder.txt_time.setText(notify_list.get(position).getTime());


    }

    @Override
    public int getItemCount() {
        return notify_list.size();
    }

    public class Notifi_Holder extends RecyclerView.ViewHolder {
        TextView text_message, txt_time;

        public Notifi_Holder(View itemView) {
            super(itemView);
            text_message = itemView.findViewById(R.id.text_message);
            txt_time = itemView.findViewById(R.id.time);
        }
    }
}