package com.volive.oshadriver.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.models.Payment_model;

import java.util.ArrayList;


public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.ViewHolder> {

    Context context;
    ArrayList<Payment_model> payment_array;

    public PaymentAdapter(Context context, ArrayList<Payment_model> payment_array) {
        this.context = context;
        this.payment_array = payment_array;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_list, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Payment_model payment_model =  new Payment_model();
        payment_model = payment_array.get(position);



        holder.txt_order_number.setText("#" +" "+payment_array.get(position).getPay_order_id());
        holder.txt_date_pay.setText(payment_array.get(position).getPay_date());
        holder.txt_order_amount.setText(payment_array.get(position).getPay_offer_price());
        holder.txt_admit_amount.setText(payment_array.get(position).getPay_additional_amount());
        holder.txt_provider_payment.setText(payment_array.get(position).getPay_provider_amount());
        holder.txt_provider_mode.setText(payment_array.get(position).getPay_payment_method());

        Picasso.get().load(Constant_keys.imagebaseurl + payment_array.get(position).getPay_profile_pic())
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.guest_user)
                .into(holder.image_pay);


    }

    @Override
    public int getItemCount() {
        return payment_array.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_order_number, txt_date_pay, txt_order_amount, txt_admit_amount,txt_provider_payment,txt_provider_mode;
        ImageView image_pay;
        LinearLayout main_linear;

        public ViewHolder(View itemView) {
            super(itemView);

            image_pay = itemView.findViewById(R.id.image_pay);
            txt_order_number = itemView.findViewById(R.id.txt_order_number);
            txt_date_pay = itemView.findViewById(R.id.txt_date_pay);
            txt_admit_amount =  itemView.findViewById(R.id.txt_admit_amount);
            txt_order_amount = itemView.findViewById(R.id.txt_order_amount);
            txt_provider_payment = itemView.findViewById(R.id.txt_provider_payment);
            txt_provider_mode = itemView.findViewById(R.id.txt_provider_mode);




        }
    }
}
