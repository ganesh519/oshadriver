package com.volive.oshadriver.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.volive.oshadriver.R;
import com.volive.oshadriver.activities.SPTrackActivity;
import com.volive.oshadriver.models.ReqSPModel;


import java.util.ArrayList;


/**
 * Created by volive on 1/2/2019.
 */

public class ReqSpAdapter extends RecyclerView.Adapter<ReqSpAdapter.ViewHolder> {
    Context context;
    ArrayList<ReqSPModel> arrayList;

    public ReqSpAdapter(Context context, ArrayList<ReqSPModel> spModelArrayList) {
        this.context = context;
        this.arrayList = spModelArrayList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sp_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.sp_name_txt.setText(arrayList.get(position).getName());
        holder.location_txt.setText(arrayList.get(position).getLocation());
        holder.txt_currency.setText(arrayList.get(position).getCurrency());

        Glide.with(context).load(arrayList.get(position).getImage()).into(holder.profile_sp_img);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_layout);
                dialog.getWindow().setBackgroundDrawableResource(
                        R.drawable.bg_dialog);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();

//                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                ImageView dialog_cancel_img = dialog.findViewById(R.id.dialog_cancel_img);
                Button btn_accept = dialog.findViewById(R.id.btn_accept);
                dialog_cancel_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                btn_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent=new Intent(context, SPTrackActivity.class);

                        //intent.putExtra("rcv_img",recievr_image);


                       context.startActivity(intent);
                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sp_name_txt, location_txt, txt_currency;
        ImageView profile_sp_img;

        public ViewHolder(View itemView) {
            super(itemView);
            profile_sp_img = itemView.findViewById(R.id.profile_sp_img);
            sp_name_txt = itemView.findViewById(R.id.sp_name_txt);
            location_txt = itemView.findViewById(R.id.location_txt);
            txt_currency = itemView.findViewById(R.id.txt_currency);
        }
    }
}
