package com.volive.oshadriver.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volive.oshadriver.R;
import com.volive.oshadriver.activities.ChatActivity;
import com.volive.oshadriver.activities.SPRequestDetailsActivity;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.models.SpOrderListModel;
import com.volive.oshadriver.util.PreferenceUtils;

import java.util.ArrayList;

public class Sp_RequestAdapter extends RecyclerView.Adapter<Sp_RequestAdapter.ViewHolder> {
    Context context;
    ArrayList<SpOrderListModel> spOrderListModels;
    String status_req ,Req_Status;
    PreferenceUtils preferenceUtils;
    String work_status = " ";

    public Sp_RequestAdapter(Context context, ArrayList<SpOrderListModel> spOrderListModels, String s) {
        this.context = context;
        this.spOrderListModels = spOrderListModels;
        this.status_req = s;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sp_requestlist, parent, false);
        return new ViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        preferenceUtils = new PreferenceUtils(context);
       // holder.user_image.setImageResource(spOrderListModels.get(position).getImage());

        Picasso.get()
                .load(Constant_keys.imagebaseurl + spOrderListModels.get(position).getUser_photo())
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.guest_user)
                .into(holder.user_image);

        if (status_req.equalsIgnoreCase("1"))
        {
            holder.pending_image.setVisibility(View.GONE);
            Req_Status = "0";
//            holder.txt_reject_reason.setVisibility(View.GONE);
        }
        else if (status_req.equalsIgnoreCase("2"))
        {
            holder.pending_image.setVisibility(View.VISIBLE);
            Req_Status = "0";
            work_status =  spOrderListModels.get(position).getWork_status();
            if (work_status.equals("")|| work_status.isEmpty())
            {
                holder.bt_chat.setVisibility(View.GONE);
            }
            else if (work_status.equals("3")){
                holder.bt_chat.setVisibility(View.VISIBLE);
            }
//            holder.txt_reject_reason.setVisibility(View.GONE);   else if (status_req.equalsIgnoreCase("2") && work_status.equals("3")){
        } else if (status_req.equalsIgnoreCase("3"))
        {
            Req_Status = "1";
            holder.pending_image.setVisibility(View.VISIBLE);
            holder.pending_image.setImageDrawable(context.getDrawable(R.drawable.completed_list));
           // holder.bt_chat.setVisibility(View.VISIBLE);
//            holder.txt_reject_reason.setVisibility(View.GONE);
        } else if (status_req.equalsIgnoreCase("4"))
        {
            Req_Status = "3";
            holder.pending_image.setVisibility(View.VISIBLE);
            holder.pending_image.setImageDrawable(context.getDrawable(R.drawable.rejectimage));
//            holder.txt_reject_reason.setVisibility(View.VISIBLE);
        }

        holder.txt_order_id.setText("#"  + "" + spOrderListModels.get(position).getOrder_ID());
        holder.txt_pick_from.setText(spOrderListModels.get(position).getPick_loc());
        holder.txt_drop_up.setText(spOrderListModels.get(position).getDrop_loc());








       /* holder.chat_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  String sender_id =  spOrderListModels.get(position).getUserID();
                String reciever_id = spOrderListModels.get(position).getUserID();
                String recievr_image = Constant_keys.imagebaseurl + spOrderListModels.get(position).getUser_photo();
                String request_id = spOrderListModels.get(position).getReq_id();

               Intent intent = new Intent(context, ChatActivity.class);

                intent.putExtra("sender",preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""));
                intent.putExtra("rec_id",reciever_id);
                intent.putExtra("rcv_image",recievr_image);
                intent.putExtra("request",request_id);
                context.startActivity(intent);


            }
        });*/


        holder.bt_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String sender_id =  spOrderListModels.get(position).getUserID();
                String reciever_id = spOrderListModels.get(position).getUserID();
                String recievr_image =  spOrderListModels.get(position).getUser_photo();
                String request_id = spOrderListModels.get(position).getReq_id();

                Intent intent = new Intent(context, ChatActivity.class);

                intent.putExtra("sender",preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""));
                intent.putExtra("rec_id",reciever_id);
                intent.putExtra("rcv_image",recievr_image);
                intent.putExtra("request",request_id);
                context.startActivity(intent);


            }
        });



        holder.details.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v)
            {

                String req_id = spOrderListModels.get(position).getReq_id();
                String userId = spOrderListModels.get(position).getUserID();
                Log.e("aslm",req_id);
                Intent intent = new Intent(context, SPRequestDetailsActivity.class);
                intent.putExtra("status_req", status_req);
                intent.putExtra("main_sts",Req_Status);
                intent.putExtra("request", req_id);
                intent.putExtra("req_user",userId);
                intent.putExtra("order_id",spOrderListModels.get(position).getOrder_ID());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return spOrderListModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_order_id, txt_location, txt_chat,txt_pick_from,txt_drop_up;
        ImageView user_image, pending_image,chat_img;
        LinearLayout main_linear,ll_chat;
        Button btn_view_issues, details,bt_chat;

        public ViewHolder(View itemView) {
            super(itemView);
            user_image = (ImageView) itemView.findViewById(R.id.user_image);
            pending_image = (ImageView) itemView.findViewById(R.id.pending_image);
            txt_pick_from = itemView.findViewById(R.id.txt_pick_from);
            txt_drop_up = itemView.findViewById(R.id.txt_drop_up);
//            btn_view_issues = itemView.findViewById(R.id.btn_view_issues);
            txt_location = itemView.findViewById(R.id.txt_location);
            txt_order_id = itemView.findViewById(R.id.txt_order_id);
            details = itemView.findViewById(R.id.details_button);
           // chat_img = itemView.findViewById(R.id.chat_img);
            txt_chat = itemView.findViewById(R.id.txt_chat);
            bt_chat = itemView.findViewById(R.id.bt_chat);
        }
    }
}