package com.volive.oshadriver.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.volive.oshadriver.R;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.models.Vechicle_images_model;
import com.volive.oshadriver.util.PreferenceUtils;

import java.util.ArrayList;

import static com.volive.oshadriver.activities.ProfileActivity.selectable_id;


public class Vechicle_ImagesAdapter extends RecyclerView.Adapter<Vechicle_ImagesAdapter.ViewHolder> {

    private ArrayList<Vechicle_images_model> arrayList;
    Context context;
    private PreferenceUtils preferenceUtils;
    private HelperClass helperClass;

    public Vechicle_ImagesAdapter(Context context, ArrayList<Vechicle_images_model> arrayListIssues) {
        this.context = context;
        this.arrayList = arrayListIssues;
        this.helperClass = new HelperClass(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.vechi_images_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        preferenceUtils = new PreferenceUtils(context);

      //  helperClass.loadImage(true, context, Constant_keys.imagebaseurl + arrayList.get(position).getImage_pic(), holder.certification_img);
        Log.e("onBindViewHolder: ", arrayList.get(position).getImage_id());

        Picasso.get().load(arrayList.get(position).getImage_pic()).
                placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.car)
                .into(holder.img_car);

        if (arrayList.get(position).isVisible())
        {
            holder.checkbox.setVisibility(View.VISIBLE);
        } else {
            holder.checkbox.setVisibility(View.GONE);
        }

        if (selectable_id.contains(arrayList.get(position).getImage_id())) {
            System.out.println("Account found");
            holder.checkbox.setChecked(true);
        }

        final int new_position = holder.getAdapterPosition();
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("onCheckedChanged: ", "" + isChecked);

                if (isChecked) {
                    // holder.issuesCheck.setChecked(false);
                    if (selectable_id.contains(arrayList.get(position).getImage_id())) {
                        System.out.println("Account found");
                    } else {
                        System.out.println("Account not found");
                        selectable_id.add(arrayList.get(position).getImage_id());
                    }
                } else {
                    selectable_id.remove(arrayList.get(position).getImage_id());
                    removeItem(new_position);
                    notifyDataSetChanged();
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(Constant_keys.imagebaseurl + arrayList.get(position).getImage_pic());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            }
        });

    }

    public void removeItem(int position){
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkbox;
        RoundedImageView certification_img;
        ImageView img_car;

        public ViewHolder(View itemView) {
            super(itemView);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
            certification_img = (RoundedImageView) itemView.findViewById(R.id.certification_img);
            img_car = itemView.findViewById(R.id.img_car);
        }
    }
}
