package com.volive.oshadriver.fragments;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.activities.Sp_HomeActivity;
import com.volive.oshadriver.activities.Sp_Payment_activity;
import com.volive.oshadriver.adapters.PaymentAdapter;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.models.Payment_model;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.volive.oshadriver.activities.Sp_HomeActivity.rel_notification;
import static com.volive.oshadriver.activities.Sp_HomeActivity.toolbar_title;

/**
 * Created by volive on 7/23/2019.
 */

public class ContactUsFragment extends Fragment implements View.OnClickListener{

    View rootView;
    Button submit_details;
    PreferenceUtils preferenceUtils;
    String language = "", stUserID = " ";
    EditText  ed_add_name,ed_add_email,ed_add_phone,ed_add_messge;
    String stName = "",stEmail = " ",stPhone = " ",stMessage = " ";
    HelperClass helperClass;
    LinearLayout ll_contact;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.contact_us_fragment, container, false);
        preferenceUtils = new PreferenceUtils(getActivity());
        helperClass = new HelperClass(getActivity());
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        intializeUI();
        intializeValues();
        return rootView;
    }

    private void intializeUI() {
        submit_details=rootView.findViewById(R.id.submit_details);
        toolbar_title.setText("Contact Us");
        rel_notification.setVisibility(View.GONE);
        ed_add_messge = rootView.findViewById(R.id.ed_add_messge);
        ed_add_name = rootView.findViewById(R.id.ed_add_name);
        ed_add_email = rootView.findViewById(R.id.ed_add_email);
        ed_add_phone = rootView.findViewById(R.id.ed_add_phone);
        ll_contact = rootView.findViewById(R.id.ll_contact);
    }

    private void intializeValues() {
        submit_details .setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.submit_details:

                stName =  ed_add_name.getText().toString();
                stEmail = ed_add_email.getText().toString();
                stPhone = ed_add_phone.getText().toString();
                stMessage = ed_add_messge.getText().toString();

                if (stName.isEmpty())
                {
                    ed_add_name.setError(getString(R.string.enter_name));
                }
               else if (stEmail.isEmpty())
                {
                ed_add_email.setError(getString(R.string.enter_email));
                }
                else if (!HelperClass.validateEmail(ed_add_email.getText().toString().trim()))
                {
                helperClass.displaySnackbar(ll_contact, getString(R.string.please_enter_your_valid_email));
                }
                else if (stPhone.length() != 10)
                {
                    ed_add_phone.setError(getString(R.string.enter_mobile));
                }
                else if (stMessage.isEmpty())
                {
                    ed_add_messge.setError(getString(R.string.enter_messge));
                }
                else  if (helperClass.checkInternetConnection(getActivity())) {
                    submit_contact();
                } else {
                    helperClass.displaySnackbar(ll_contact,getResources().getString(R.string.check_internet));
                }
                break;



        }


    }

    private void submit_contact()
        {
            final API_Services services  = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> call_notification= null;
            call_notification = services.contact_us(Constant_keys.API_KEY,language,preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""),stName,stEmail,stPhone,stMessage);

            final ProgressDialog progressDoalog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
            progressDoalog.setCancelable(false);
            progressDoalog.setMessage("Please Wait....");
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDoalog.show();

            call_notification.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
                {
                    progressDoalog.dismiss();
                    if (response.isSuccessful())
                    {
                        Log.e("conatct",response.body().toString());
                        try {
                            JSONObject json_obejct =  new JSONObject(response.body().toString());
                            int status = json_obejct.getInt("status");
                            String   message = json_obejct.getString("message");
                            if (status == 1)
                            {
                              helperClass.displaySnackbar(ll_contact,message);
                              ed_add_email.setText("");
                              ed_add_name.setText("");
                              ed_add_messge.setText("");
                              ed_add_phone.setText("");
                                Intent a  = new Intent(getActivity(), Sp_HomeActivity.class);
                                startActivity(a);

                            }
                            else {
                                helperClass.displaySnackbar(ll_contact,message);
                            }

                        } catch (Exception e) {
                            progressDoalog.dismiss();
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    progressDoalog.dismiss();
                    Log.e("Erro:" ,call.toString());
                    Log.e("Error:",t.toString());

                }
            });

        }

}
