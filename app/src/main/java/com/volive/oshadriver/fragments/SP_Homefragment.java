package com.volive.oshadriver.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.volive.oshadriver.R;
import com.volive.oshadriver.activities.SP_completed;
import com.volive.oshadriver.activities.Sp_pending;
import com.volive.oshadriver.activities.Sp_reject;
import com.volive.oshadriver.activities.Sp_request;
import com.volive.oshadriver.activities.WalletActivity;
import com.volive.oshadriver.helperclasses.Constant_keys;
import com.volive.oshadriver.helperclasses.HelperClass;
import com.volive.oshadriver.util.API_Services;
import com.volive.oshadriver.util.Chating_screen_status;
import com.volive.oshadriver.util.PreferenceUtils;
import com.volive.oshadriver.util.Retrofit_fun;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.volive.oshadriver.activities.Sp_HomeActivity.rel_notification;

public class SP_Homefragment extends Fragment implements View.OnClickListener {
    View rootView;
    RelativeLayout rel_request, rel_pending, rel_completed, rel_reject, rel_wallet;
    CircleImageView profileimg;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    TextView txt_person_name, txt_user_email, txt_req_count, txt_pending, txt_completed, txt_reject, txt_wallet_amount, txt_referal_code;
    String stUserID, st_ReqType, language;
    private BroadcastReceiver mHandler = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            status_service();
        }
    };

    public SP_Homefragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_sp_homefragment, container, false);

        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        preferenceUtils = new PreferenceUtils(getActivity());
        helperClass = new HelperClass(getActivity());
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        st_ReqType = preferenceUtils.getStringFromPreference(PreferenceUtils.request_type, "");
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        intilaizeUI();
        intilizeValues();
        setValues();
//        status_service();
        if (getActivity() != null)
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mHandler, new IntentFilter("home"));


    }

    private void status_service() {

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_service;
        call_service = service.driversRidesStatus(Constant_keys.API_KEY, language, stUserID);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();


        call_service.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Log.e("jgadswik", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");

                        String user_details = jsonObject.getString("user_details");

                        if (status == 1) {
                            String st_request_count = jsonObject.getString("requests");
                            String pending_count = jsonObject.getString("pending");
                            String completed_count = jsonObject.getString("completed");
                            String reject_count = jsonObject.getString("reject");
                            String wallet_amount = jsonObject.getString("wallet_amount");


                            //request count
                            if (st_request_count.equals("")) {
                                txt_req_count.setText("0");
                            } else {
                                txt_req_count.setText(st_request_count);
                            }


                            //pending count
                            if (pending_count.equals("")) {
                                txt_pending.setText("0");
                            } else {
                                txt_pending.setText(pending_count);
                            }

                            //reject count

                            if (reject_count.equals("")) {
                                txt_reject.setText("0");
                            } else {
                                txt_reject.setText(reject_count);
                            }

                            //completed count
                            if (completed_count.equals("")) {
                                txt_completed.setText("0");
                            } else {
                                txt_completed.setText(completed_count);
                            }

                            //wallet amount
                            if (wallet_amount.equals("")) {
                                txt_wallet_amount.setText("0");
                            } else {
                                txt_wallet_amount.setText(wallet_amount);
                            }


                            /*if(st_request_count.equals("") && pending_count.equals("") && completed_count.equals("") && reject_count.equals(""))
                            {
                                txt_req_count.setText("0");
                                txt_reject .setText("0");
                                txt_completed.setText("0");
                                txt_pending.setText("0");
                                txt_wallet_amount.setText("0");
                            }
                            else {*/


                            //   }

                            if (jsonObject.has("referal_code")) ;
                            String referal_code = jsonObject.getString("referal_code");
                            preferenceUtils.saveString(PreferenceUtils.Referal_code, referal_code);

                        }


                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Eror", call.toString());
                Log.e("error", t.toString());

            }
        });


    }

    private void intilaizeUI() {
        rel_request = rootView.findViewById(R.id.rel_request);
        rel_pending = rootView.findViewById(R.id.rel_pending);
        rel_completed = rootView.findViewById(R.id.rel_completed);
        rel_reject = rootView.findViewById(R.id.rel_reject);
        rel_wallet = rootView.findViewById(R.id.rel_wallet);
        txt_person_name = rootView.findViewById(R.id.txt_person_name);
        profileimg = rootView.findViewById(R.id.profileimg);
        txt_user_email = rootView.findViewById(R.id.txt_user_email);
        txt_req_count = rootView.findViewById(R.id.txt_req_count);
        txt_pending = rootView.findViewById(R.id.txt_pending);
        txt_completed = rootView.findViewById(R.id.txt_completed);
        txt_reject = rootView.findViewById(R.id.txt_reject);
        txt_wallet_amount = rootView.findViewById(R.id.txt_wallet_amount);
        txt_referal_code = rootView.findViewById(R.id.txt_referal_code);


    }

    private void intilizeValues() {

        rel_wallet.setOnClickListener(this);
        rel_request.setOnClickListener(this);
        rel_pending.setOnClickListener(this);
        rel_completed.setOnClickListener(this);
        rel_reject.setOnClickListener(this);
    }

    private void setValues() {
        rel_notification.setVisibility(View.VISIBLE);
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "").isEmpty()) {
            profileimg.setImageResource(R.drawable.guest_user);

        } else {
            // validate.loadImage(true, ProfileActivity.this, ProfileActivity.image_url, profile_image_select);
            Glide.with(getActivity()).load(preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "")).into(profileimg);
        }

        txt_person_name.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.UserName, ""));
        txt_user_email.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Email, ""));


        txt_referal_code.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Referal_code, ""));


    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();

        switch (v.getId()) {
            case R.id.rel_request:


                i = new Intent(getActivity(), Sp_request.class);

                startActivity(i);
                break;
            case R.id.rel_completed:

                i = new Intent(getActivity(), SP_completed.class);
//                i.putExtra("Activity", Sp_HomeActivity.activityName);
                startActivity(i);
                break;
            case R.id.rel_reject:

                i = new Intent(getActivity(), Sp_reject.class);
//                i.putExtra("Activity", Sp_HomeActivity.activityName);
                startActivity(i);
                break;
            case R.id.rel_wallet:

                i = new Intent(getActivity(), WalletActivity.class);
                startActivity(i);
                break;

            case R.id.rel_pending:
                i = new Intent(getActivity(), Sp_pending.class);
//                i.putExtra("Activity", Sp_HomeActivity.activityName);
                startActivity(i);
                break;

        }

    }
// TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onPause() {
        super.onPause();
        Chating_screen_status.activityPaused();

        if (getActivity() != null)
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mHandler);

    }

    @Override
    public void onResume() {
        super.onResume();
        status_service();
        Chating_screen_status.activityResumed();

    }
}