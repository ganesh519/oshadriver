package com.volive.oshadriver.firebase;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.volive.oshadriver.R;
import com.volive.oshadriver.activities.ChatActivity;
import com.volive.oshadriver.activities.SPRequestDetailsActivity;
import com.volive.oshadriver.activities.Sp_HomeActivity;
import com.volive.oshadriver.activities.Sp_pending;
import com.volive.oshadriver.activities.Sp_reject;
import com.volive.oshadriver.activities.Sp_request;
import com.volive.oshadriver.activities.Splash_Screen;
import com.volive.oshadriver.util.Chating_screen_status;
import com.volive.oshadriver.util.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    int m = 0;
    PreferenceUtils preferenceUtils;
    String language = "", thread_id = "", type = "", title = "";
    JSONObject object;

    String provider_id, reciever_image, order_id, request_id,order_date, user_id, message, image, auth_level, sender_id, receiver_Id;

    Context ctx;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        preferenceUtils = new PreferenceUtils(this);
//        String data_msg = remoteMessage.getData().toString();
//        String notification_msg = remoteMessage.getNotification().getBody();
//        Log.e("onMessageReceived: ", data_msg);
//        Log.e("onMessage: ", notification_msg);

        try {
            if (remoteMessage != null) {
                preferenceUtils = new PreferenceUtils(this);
                Random random = new Random();
                m = random.nextInt(9999 - 1000) + 1000;
                if (remoteMessage.getData().size() > 0) {
                    String data = remoteMessage.getData().toString();
                    Log.e("data", data);
                    Map<String, String> params = remoteMessage.getData();
                    object = new JSONObject(params);
                    type = object.getString("notification_type");
                    title = object.getString("notification_title_en");
                   // message = object.getString("description_en");
                    // String body = object.getString("body");
                    Log.d("JSON_OBJECT", object.toString());
                    sendNotification(object.getString("notification_title_en"));
                }
                //notification data
                if (remoteMessage.getNotification() != null) {
                    Map<String, String> params = remoteMessage.getData();
                    object = new JSONObject(params);
                    Log.d("JSON_OBJECT", object.toString());
                    type = object.getString("notification_type");
                    title = object.getString("notification_title_en");
                   // message = object.getString("description_en");
                    // notification_handle_data();
                    notification_handle();
                }

            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("onMessageReceived: ", e.getMessage());
        }
    }

    private void notification_handle() {
            try {
                if (type.equalsIgnoreCase("new_request")) {
                    title = object.getString("notification_title_en");
                    Intent intent = new Intent(getApplicationContext(), SPRequestDetailsActivity.class);
                    intent.putExtra("status_req", "1");
                    intent.putExtra("main_sts","0");
                    intent.putExtra("request", object.getString("request_id"));
                    intent.putExtra("req_user",object.getString("sender_id"));
                    intent.putExtra("order_id",object.getString("order_id"));
                    handleNotification(intent, object.getString("description_en"));

                    if (Chating_screen_status.isActivityVisible()) {
                        intent = new Intent("home");
                        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                        localBroadcastManager.sendBroadcast(intent);
                    }

                }
                else if (type.equalsIgnoreCase("offer_accepted")) {
                    title = object.getString("notification_title_en");
                    Intent intent = new Intent(getApplicationContext(), Sp_pending.class);
                    handleNotification(intent, object.getString("description_en"));
                }
                else if (type.equalsIgnoreCase("cancel_request")) {
                    title = object.getString("notification_title_en");
                    Intent intent = new Intent(getApplicationContext(), Sp_reject.class);
                    handleNotification(intent, object.getString("description_en"));
                }
                else if (type.equalsIgnoreCase("new_offer")) {
                    title = object.getString("notification_title_en");
                    user_id = object.getString("sender_id");
                    provider_id = object.getString("receiver_id");
                    order_id = object.getString("order_id");
                    Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                    handleNotification(intent, object.getString("notification_title_en"));
                }
                else if (type.equalsIgnoreCase("work_started")) {
                    title = object.getString("notification_title_en");
                    Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                    handleNotification(intent, object.getString("notification_title_en"));
                }
                else if (type.equalsIgnoreCase("request_completed")) {
                    title = object.getString("notification_title_en");
                    Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                    handleNotification(intent, object.getString("notification_title_en"));
                }
                else if (object.getString("notification_type").equalsIgnoreCase("new_message")) {
                    title = object.getString("notification_title_en");
                    sender_id = object.getString("sender_id");
                    receiver_Id = object.getString("receiver_id");
                    request_id = object.getString("request_id");
                    if (Chating_screen_status.isActivityVisible()) {
                        Intent intent ;
                        intent = new Intent("FCM-MESSAGE");
                        intent.putExtra("sender", sender_id);
                        intent.putExtra("rec_id", receiver_Id);
                        intent.putExtra("request", request_id);
                        intent.putExtra("rcv_image",object.getString("image"));
                        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                        localBroadcastManager.sendBroadcast(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                        intent.putExtra("sender", receiver_Id);
                        intent.putExtra("rec_id", sender_id);
                        intent.putExtra("request", request_id);
                        intent.putExtra("rcv_image",object.getString("image"));
                        handleNotification(intent, object.getString("notification_title_en"));
                    }
                }

            }

            catch (JSONException e) {
                e.printStackTrace();
            }

    }

    private void handleNotification(Intent intent, String message) {
        if (isAppIsInBackground(this)) {
              createNotification(intent, message);
//            sendNotification(message);
        } else {
            createElseNotification(intent, message);
        }
    }

    private void createElseNotification(Intent intent, String message) {
        intent.putExtra("message", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);

        Notification notification;
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = getApplicationContext().getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationManager notificationManager =
                (NotificationManager)
                        getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker("").setWhen(0)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(inboxStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();


        notificationManager.notify(m, notification);

    }

    private void createNotification(Intent intent, String message) {
        intent.putExtra("message", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext());

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);

        Notification notification;
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = getApplicationContext().getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);

        }

        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker("").setWhen(0)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(inboxStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();

        notificationManager.notify(m, notification);

    }

    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            assert am != null;
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            assert am != null;
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, Splash_Screen.class);
        intent.putExtra("body", type);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext());
        Notification notification;
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);
        //String CHANNEL_ID = "my_channel_01";

        CharSequence name = getApplicationContext().getString(R.string.app_name);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        /*NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId).setTicker("").setWhen(0)
                        .setSmallIcon(R.mipmap.app_icon_final)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setStyle(inboxStyle)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .build();*/

        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker("").setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(inboxStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(messageBody)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();

        ;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(m, notification);
        // notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }




}








/*

{receiver_id=73, notification_type=new_request, seen_status=1,
        notification_title_ar=تم قبول عرضك, notification_title_en=You have new request,
        sender_id=62, request_id=494, description_ar=hello,
        description_en=hello, order_id=214131, request_type=2, created_at=2019-10-15 06:49:08}
*/



























